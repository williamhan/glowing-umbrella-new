<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportationinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sch_id');
            $table->string('shuttle_id');
            $table->string('shuttle_type');
            $table->string('shuttle_day_of_week');
            $table->string('shuttle_start_time');
            $table->string('shuttle_end_time');
            $table->string('shuttle_runs_after_hours');
            $table->string('shuttle_cutoff');
            $table->string('shuttle_after_cutoff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportationinfo');
    }
}
