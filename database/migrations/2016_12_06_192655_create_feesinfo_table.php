<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feesinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fee_id');
            $table->string('sch_id');
            $table->string('fee_state');
            $table->string('fee_type');
            $table->string('fee_amount');
            $table->string('fee_is_exp');
            $table->string('fee_is_sr');
            $table->string('fee_is_track_two');
            $table->string('fee_is_sc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feesinfo');
    }
}
