<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schoolinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sch_name');
            $table->string('sch_id');
            $table->string('sch_type');
            $table->string('sch_address');
            $table->string('sch_city');
            $table->string('sch_state');
            $table->string('sch_zip');
            $table->string('sch_phone');
            $table->string('sch_fax');
            $table->string('sch_duration_min');
            $table->string('sch_duration_max');
            $table->string('sch_transportation_id');
            $table->string('sch_schedule_id');
            $table->string('sch_docs_id');
            $table->string('sch_fees_id');
            $table->string('sch_housing_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schoolinfo');
    }
}
