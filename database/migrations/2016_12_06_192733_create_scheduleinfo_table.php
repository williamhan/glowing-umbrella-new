<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduleinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sch_id');
            $table->string('schedule_id');
            $table->string('schedule_sun');
            $table->string('schedule_mon');
            $table->string('schedule_tues');
            $table->string('schedule_wed');
            $table->string('schedule_thurs');
            $table->string('schedule_fri');
            $table->string('schedule_sat');
            $table->string('schedule_sun_start_time');
            $table->string('schedule_mon_start_time');
            $table->string('schedule_tues_start_time');
            $table->string('schedule_wed_start_time');
            $table->string('schedule_thurs_start_time');
            $table->string('schedule_fri_start_time');
            $table->string('schedule_sat_start_time');
            $table->string('schedule_sun_end_time');
            $table->string('schedule_mon_end_time');
            $table->string('schedule_tues_end_time');
            $table->string('schedule_wed_end_time');
            $table->string('schedule_thurs_end_time');
            $table->string('schedule_fri_end_time');
            $table->string('schedule_sat_end_time');
            $table->string('schedule_is_exp');
            $table->string('schedule_is_sr');
            $table->string('schedule_is_track_two');
            $table->string('schedule_is_sc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduleinfo');
    }
}
