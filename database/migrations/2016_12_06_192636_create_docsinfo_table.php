<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocsinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docsinfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sch_id');
            $table->string('doc_id');
            $table->string('is_alt');
            $table->string('doc_name');
            $table->string('doc_is_required');
            $table->string('doc_has_alt');
            $table->string('alt_doc_id_one');
            $table->string('alt_doc_id_two');
            $table->string('alt_doc_id_three');
            $table->string('doc_temp_ok');
            $table->string('doc_laminated_ok');
            $table->string('doc_receipt_ok');
            $table->string('doc_is_exp');
            $table->string('doc_is_sr');
            $table->string('doc_is_track_two');
            $table->string('doc_is_sc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docsinfo');
    }
}
