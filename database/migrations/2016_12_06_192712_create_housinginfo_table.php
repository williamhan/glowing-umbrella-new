<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHousinginfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('housinginfo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sch_id');
            $table->string('housing_provided');
            $table->string('housing_id');
            $table->string('housing_type');
            $table->string('housing_name');
            $table->string('housing_address');
            $table->string('housing_city');
            $table->string('housing_state');
            $table->string('housing_zip');
            $table->string('housing_phone');
            $table->string('housing_occupancy');
            $table->string('housing_has_shuttle');
            $table->string('housing_shuttle_id');
            $table->string('housing_is_exp');
            $table->string('housing_is_sr');
            $table->string('housing_is_track_two');
            $table->string('housing_is_sc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('housinginfo');
    }
}
