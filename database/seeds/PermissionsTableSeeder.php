<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        $permissions = array(
            array( // 1
                'name'         => 'global_admin',
                'display_name' => 'Global Admin',
                'description'  => 'do everything'
            ),
            array( // 2
                'name'         => 'approve_deny_namechanges',
                'display_name' => 'approve_deny_namechanges',
                'description'  => 'approve or deny namechanges'
            ),
            array( // 3
                'name'         => 'run_hireright',
                'display_name' => 'Run HireRight',
                'description'  => 'submit hireright requests'
            ),
            array( // 4
                'name'         => 'send_emails',
                'display_name' => 'send_emails',
                'description'  => 'send_emails'
            ),
            array( // 5
                'name'         => 'send_sms',
                'display_name' => 'Send SMS',
                'description'  => 'Send SMS'
            ),
            array( // 6
                'name'         => 'view_school_info',
                'display_name' => 'view_school_info',
                'description'  => 'view_school_info'
            ),
            array( // 7
                'name'         => 'edit_school_info',
                'display_name' => 'edit_school_info',
                'description'  => 'edit_school_info'
            ),
            array( // 8
                'name'         => 'request_namechange_recruiter',
                'display_name' => 'request_namechange_recruiter',
                'description'  => 'request_namechange_recruiter'
            ),
            array( // 9
                'name'         => 'request_namechange_permitspec',
                'display_name' => 'request_namechange_permitspec',
                'description'  => 'request_namechange_permitspec'
            ),
        );
        DB::table('permissions')->insert( $permissions );
        DB::table('permission_role')->delete();
        $role_id_admin = Role::where('name', '=', 'admin')->first()->id;
        $role_id_manager = Role::where('name', '=', 'manager')->first()->id;
        $role_id_agent = Role::where('name', '=', 'agent')->first()->id;
        $role_id_beta = Role::where('name', '=', 'beta')->first()->id;
        $permission_base = (int)DB::table('permissions')->first()->id - 1;
        $permissions = array(
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 1
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 2
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 3
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 4
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 5
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 6
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 7
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 8
            ),
            array(
                'role_id'       => $role_id_manager,
                'permission_id' => $permission_base + 9
            ),
            array(
                'role_id'       => $role_id_agent,
                'permission_id' => $permission_base + 3
            ),
            array(
                'role_id'       => $role_id_agent,
                'permission_id' => $permission_base + 4
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 5
            ),
            array(
                'role_id'       => $role_id_admin,
                'permission_id' => $permission_base + 6
            ),
            array(
                'role_id'       => $role_id_comment,
                'permission_id' => $permission_base + 6
            ),
        );
        DB::table('permission_role')->insert( $permissions );
    }
}
