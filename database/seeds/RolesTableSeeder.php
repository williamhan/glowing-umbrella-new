<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        $adminRole = new Role;
        $adminRole->name = 'admin';
        $adminRole->display_name = 'Project Owner'; // optional
		$adminRole->description  = 'User is the owner of this project'; // optional
        $adminRole->save();

        $managerRole = new Role;
        $managerRole->name = 'manager';
        $managerRole->display_name = 'Team Manager'; // optional
		$managerRole->description  = 'User is the manager of a team and can accept and deny namechanges'; // optional
        $managerRole->save();

        $agentRole = new Role;
        $agentRole->name = 'agent';
        $agentRole->display_name = 'Agent'; // optional
		$agentRole->description  = 'User is a regular agent and only has production permissions'; // optional
        $agentRole->save();

        $betaRole = new Role;
        $betaRole->name = 'beta';
        $betaRole->display_name = 'Beta Tester'; // optional
		$betaRole->description  = 'User is a regular agent with beta test access permissions'; // optional
        $betaRole->save();

        $user = User::where('role_id','=',5)->all();
        $user->attachRole( $betaRole );

        $user = User::where('role_id','=',4)->all();
        $user->attachRole( $adminRole );

        $user = User::where('role_id','=',3)->all();
        $user->attachRole( $managerRole );

        $user = User::where('role_id','=',2)->all();
        $user->attachRole( $agentRole );

        
    }
}
