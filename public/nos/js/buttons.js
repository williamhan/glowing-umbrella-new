/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#redbutton").click(function(){
        $(".form-header").removeClass("header-primary");
        $(".form-header").removeClass("header-blue");
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-green");
        $(".form-header").removeClass("header-yellow");
        $(".form-header").removeClass("header-purple");
        $(".form-header").addClass("header-red");
        $(".form-body").removeClass("theme-primary");
        $(".form-body").removeClass("theme-blue");
        $(".form-body").removeClass("theme-green");
        $(".form-body").removeClass("theme-yellow");
        $(".form-body").removeClass("theme-purple");
        $(".form-body").addClass("theme-red");
    });
    $("#defaultbutton").click(function(){
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-blue");
        $(".form-header").removeClass("header-green");
        $(".form-header").removeClass("header-yellow");
        $(".form-header").removeClass("header-purple");
        $(".form-header").addClass("header-primary");
        $(".form-body").removeClass("theme-red");
        $(".form-body").removeClass("theme-blue");
        $(".form-body").removeClass("theme-green");
        $(".form-body").removeClass("theme-yellow");
        $(".form-body").removeClass("theme-purple");
        $(".form-body").addClass("theme-primary");
    });
    $("#greenbutton").click(function(){
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-blue");
        $(".form-header").removeClass("header-primary");
        $(".form-header").removeClass("header-yellow");
        $(".form-header").removeClass("header-purple");
        $(".form-header").addClass("header-green");
        $(".form-body").removeClass("theme-primary");
        $(".form-body").removeClass("theme-blue");
        $(".form-body").removeClass("theme-red");
        $(".form-body").removeClass("theme-yellow");
        $(".form-body").removeClass("theme-purple");
        $(".form-body").addClass("theme-green");
    });
    $("#yellowbutton").click(function(){
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-blue");
        $(".form-header").removeClass("header-green");
        $(".form-header").removeClass("header-primary");
        $(".form-header").removeClass("header-purple");
        $(".form-header").addClass("header-yellow");
        $(".form-body").removeClass("theme-primary");
        $(".form-body").removeClass("theme-blue");
        $(".form-body").removeClass("theme-green");
        $(".form-body").removeClass("theme-red");
        $(".form-body").removeClass("theme-purple");
        $(".form-body").addClass("theme-yellow");
    });
    $("#purplebutton").click(function(){
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-blue");
        $(".form-header").removeClass("header-green");
        $(".form-header").removeClass("header-yellow");
        $(".form-header").removeClass("header-primary");
        $(".form-header").addClass("header-purple");
        $(".form-body").removeClass("theme-primary");
        $(".form-body").removeClass("theme-blue");
        $(".form-body").removeClass("theme-green");
        $(".form-body").removeClass("theme-yellow");
        $(".form-body").removeClass("theme-red");
        $(".form-body").addClass("theme-purple");
    });
    $("#bluebutton").click(function(){
        $(".form-header").removeClass("header-red");
        $(".form-header").removeClass("header-primary");
        $(".form-header").removeClass("header-green");
        $(".form-header").removeClass("header-yellow");
        $(".form-header").removeClass("header-purple");
        $(".form-header").addClass("header-blue");
        $(".form-body").removeClass("theme-primary");
        $(".form-body").removeClass("theme-red");
        $(".form-body").removeClass("theme-green");
        $(".form-body").removeClass("theme-yellow");
        $(".form-body").removeClass("theme-purple");
        $(".form-body").addClass("theme-blue");
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/welcomeemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                 alert("Welcome Packet Sent!");
            }
        });
    });
    $("#PastEmpButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/pastempemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("PEV Sent!");
            }
        });
    });
    $("#TravelEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/travelemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Travel Email Sent!");
            }
        });
    });
    $("#LeftMessageEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/leftmessageemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ConfirmationEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/confirmationemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ThreePApprovalEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/threepapprovalemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#WAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/wareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#VAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/vareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#NHReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/nhreleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */

$(document).ready(function(){
    $("#FindGhButton").click(function(){


        $('#ghmapmodal').modal('show');

    });
    $("#OrderGhButton").click(function(){
        alert("Not Yet Implemented");
    });

    var customIcons = {
        restaurant: {
            icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'
        },
        bar: {
            icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
        }
    };

    function mapload() {
        var map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng($('#address1_latitude').val(), $('#address1_longitude').val()),
            zoom: 10,
            mapTypeId: 'roadmap'
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: new google.maps.LatLng($('#address1_latitude').val(), $('#address1_longitude').val())
        });

        var infoWindow = new google.maps.InfoWindow;

        // Change this depending on the name of your PHP file
        downloadUrl("dblogintwo.php", function(data) {
            var xml = data.responseXML;

            //alert(data.responseXML);

            var markers = xml.documentElement.getElementsByTagName("marker");
            for (var i = 0; i < markers.length; i++) {
                var name = markers[i].getAttribute("name");
                var address = markers[i].getAttribute("address");
                var type = markers[i].getAttribute("type");
                var point = new google.maps.LatLng(
                    parseFloat(markers[i].getAttribute("lat")),
                    parseFloat(markers[i].getAttribute("lng")));
                var html = "<b>" + name + "</b> <br/>" + address;
                var icon = customIcons[type] || {};
                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    icon: icon.icon
                });
                bindInfoWindow(marker, map, infoWindow, html);
            }
        });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
    }

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.overrideMimeType('text/xml');
        request.send(null);
    }

    function doNothing() {};
});
/**
 * Created by williamhan on 11/18/16.
 */

$(document).ready(function(){
    $("#RunMvrButton").click(function(){
        alert("Not Yet Implemented");
    });
    $("#ReviewMvrButton").click(function(){
        alert("Not Yet Implemented");
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#NCRecruiter").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCPermitSpec").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/ps",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCNo").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny",
            type: "post",
            data: $("#allnames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJacob").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jacob",
            type: "post",
            data: $("#jacobnames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJessica").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jessica",
            type: "post",
            data: $("#jessicanames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoLisa").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/lisa",
            type: "post",
            data: $("#lisanames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoNick").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/nick",
            type: "post",
            data: $("#nicknames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoSepeti").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/sepeti",
            type: "post",
            data: $("#sepetinames").serialize(),
            success: function(){
                alert("Name Change Denied");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/welcomesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#PastEmpSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/pastempsms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#TravelSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/travelsms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#LeftMessageSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/leftmessagesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ConfirmationSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/confirmationsms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#LastChanceSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/lastchancesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#WAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/wareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#VAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/vareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#NHReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/nhreleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#DIYSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/diysms",
            type: "post",
            data: $("#mainform, #DIYSms").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#DDTwoFourteenButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/dd214",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ComLetterButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/commandersletter",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});
//# sourceMappingURL=buttons.js.map
