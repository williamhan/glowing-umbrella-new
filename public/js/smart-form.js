	$(function(){

			var rules = "true";
			var emailRules = "true";
			var numberRules = "true";
			var phoneRules = "true";
			var zipCodeRules = "true";
			var digitsRules = "true";
			var referencesRules = "false";
			var workHistoryRules = "false";


			$("#smart-form").steps({
				bodyTag: "fieldset",
				headerTag: "h2",
				bodyTag: "fieldset",
				transitionEffect: "slideLeft",
				enableFinishButton: true,
				titleTemplate: "#title#",
				labels: {
					finish: "Submit Application",
					next: "Continue",
					previous: "Go Back",
					loading: "Loading..."
				},
				onStepChanging: function (event, currentIndex, newIndex)
				{

				if(currentIndex == 1)
					{
					if(newIndex == 2)
			         {

					$("#signaturedata:empty").text(function(){
						$("#signature_data_error").text("This field is required.");
    				});


			        }
			       }

					if (currentIndex > newIndex){return true; }
					var form = $(this);
					if (currentIndex < newIndex){}
					return form.valid();
				},
				onStepChanged: function (event, currentIndex, priorIndex){
				},
				onFinishing: function (event, currentIndex){
					var form = $(this);
					form.validate().settings.ignore = ":disabled";
					return form.valid();
				},
				onFinished: function (event, currentIndex){
					var form = $(this);
					//var apptype = $("#app_type").val();
					$(form).ajaxSubmit({
				         beforeSend: function() {
							  $( "#header-left" ).replaceWith( "<div class='col-sm-7' id='header-left'><div class='logo'><img src='img/company3.png'></div><div class='verticle-line'><h1>Thank You For Applying!</h1></div><h2>We have received your job application.<br><br>Please call us to discuss next steps.<br><br>800-887-8619<br><br>Monday - Friday<br>7AM - 5PM (MST)</h2></div>" );

							var s = document.createElement("script");
							s.type = "text/javascript";
							s.src = "//conv.indeed.com/pagead/conversion.js";
							$("head").append(s);


							var _qevents = _qevents || [];

							(function() {
							var elem = document.createElement('script');
							elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
							elem.async = true;
							elem.type = "text/javascript";
							var scpt = document.getElementsByTagName('script')[0];
							scpt.parentNode.insertBefore(elem, scpt);
							})();

							_qevents.push(
							{qacct:"p-6p3pGQs92YrSx",labels:"_fp.event.Drive CRE Application Submit,_fp.customer.INSERT+CUSTOMER+TYPE",orderid:"INSERT+ORDER+ID", event:"refresh"}
							);




							var qc = document.createElement("noscript");
							var qci = document.createElement("img");
							qci.style = "display: none;";
							qci.height = "1";
							qci.width = "1";
							qci.alt = "Quantcast";
							qci.border = "0";
							qci.src = "//pixel.quantserve.com/pixel/p-6p3pGQs92YrSx.gif?labels=_fp.event.Drive+CRE+Application+Submit,_fp.customer.INSERT+CUSTOMER+TYPE&orderid=INSERT+ORDER+ID";
							$(qc).append(qci);
							$("head").append(qc);



							var cb = document.createElement("script");
							cb.type = "text/javascript";
							cb.src = "//click.appcast.io/pixels/generic3-2394.js?ent=98";
							$("head").append(cb);


							var baird = document.createElement("script");
							baird.type = "text/javascript";
							baird.src = "//click.appcast.io/pixels/bayard3-2967.js?ent=33";
							$("head").append(baird);


							var bairdorg = document.createElement("script");
							bairdorg.type = "text/javascript";
							bairdorg.src = "//click.appcast.io/pixels/bayard3-3001.js?ent=33";
							$("head").append(bairdorg);


							var ns = document.createElement("noscript");
							var nsi = document.createElement("img");
							nsi.height = "1";
							nsi.width = "1";
							nsi.border = "0";
							nsi.src = "//conv.indeed.com/pagead/conv/9175775594800170/?script=0";
							$(ns).append(nsi);
							$("head").append(ns);

							ga('send', 'pageview', '/non-cdl/thank-you');
							ga('send', 'event', 'PageLoad', 'ThankYou', 'Non-CDL 1.0');

							window._vis_opt_queue = window._vis_opt_queue || [];
							window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(201);});

							var howheard = $("#how_heard").val();
							var recordSource = $("#record_source").val();
							var geminiVars = ["3100", "3101", "3102", "3103", "3104", "3105", "3106", "3107"];
							var alltruckingvars = ["5085","5086"];

							var hearstvars = ["5088"];

							var upwardvars = ["5317"];

							if (upwardvars.indexOf(recordSource) === -1)
								{
									//Do Nothing
								}
							else
								{
									var upward = document.createElement("img");
									upward.height = "0";
									upward.width = "0";
									upward.src = "http://l5srv.net/AdServer/convert.ads?aid=MjEw";
									$("head").append(upward);
								}

							if (geminiVars.indexOf(recordSource) === -1)
								{
									//Do Nothing
								}
							else
								{
									var gmni = document.createElement("script");
									gmni.type = "text/javascript";
									gmni.src = "js/gemini.js";
									$("head").append(gmni);
								}

							if (alltruckingvars.indexOf(recordSource) === -1)
								{
									// Do Nothing
								}
							else
								{
									var alltrk = document.createElement("img");
									alltrk.height = "1";
									alltrk.width = "1";
									alltrk.border = "0";
									alltrk.src = "//www.clickmeter.com/conversion.aspx?id=55BA17FD8A554B768E0A1C80A3C7512E&val=0.00&param=empty&com=0.00&comperc=0.00";
									$("head").append(alltrk);

								}


							if (howheard.match("^GOOGLE"))
							{


							var googletwo = document.createElement("script");
									googletwo.type = "text/javascript";
									googletwo.src = "js/google.js";
									$("head").append(googletwo);



							var googlens = document.createElement("noscript");
							var googlediv = document.createElement("div");
							googlediv.style = "display:inline";
							var googlensi = document.createElement("img");
							googlensi.height = "1";
							googlensi.width = "1";
							googlensi.style = "border-style:none;";
							googlensi.alt = "border-style:none;";
							googlensi.src = "//www.googleadservices.com/pagead/conversion/1038203223/?label=CgZRCL3ZgwEQ1_KG7wM&amp;guid=ON&amp;script=0";
							$(googlens).append(googlediv);
							$(googlediv).append(googlensi);
							$("head").append(googlens);

									var google = document.createElement("script");
									google.type = "text/javascript";
									google.src = "//www.googleadservices.com/pagead/conversion.js";
									$("head").append(google);





							}


							if (howheard.match("^CDLJOBNOW"))
							{
							var cdljobnow = document.createElement("img");
									cdljobnow.height = "1";
									cdljobnow.width = "1";
									cdljobnow.border = "0";
									cdljobnow.src = "//sourcetracker.io/event/MrAfI";
									$("head").append(cdljobnow);
							}







							if (howheard.match("^RANDALLMEDIA"))
							{

							var randallns = document.createElement("noscript");
							var randallnsi = document.createElement("img");
							randallnsi.height = "0";
							randallnsi.width = "0";
							randallnsi.style = "display:none;visibility:hidden";
							randallnsi.src = "//www.googletagmanager.com/ns.html?id=GTM-WKF65B";
							$(randallns).append(randallnsi);
							$("head").append(randallns);

									var randall = document.createElement("script");
									randall.type = "text/javascript";
									randall.src = "js/randallrei.js";
									$("head").append(randall);
							}


							if (howheard.match("^BING"))
							{

							var bingns = document.createElement("noscript");
							var bingnsi = document.createElement("img");
							bingnsi.height = "0";
							bingnsi.width = "0";
							bingnsi.style = "display:none;visibility:hidden";
							bingnsi.src = "//bat.bing.com/action/0?ti=5014094&Ver=2";
							$(bingns).append(bingnsi);
							$("head").append(bingns);

									var bing = document.createElement("script");
									bing.type = "text/javascript";
									bing.src = "js/bing.js";
									$("head").append(bing);


/*
									var bingtwo = document.createElement("script");
									bingtwo.type = "text/javascript";
									bingtwo.src = "js/bingtwo.js";
									$("head").append(bingtwo);
*/
							}


							if (howheard.match("^ZIPALERTS"))
							{

							var zipns = document.createElement("noscript");
							var zipnsi = document.createElement("iframe");
							zipnsi.height = "0";
							zipnsi.width = "0";
							zipnsi.style = "display:none;visibility:hidden";
							zipnsi.src = "//www.googletagmanager.com/ns.html?id=GTM-P6P7Z4";
							$(zipns).append(zipnsi);
							$("head").append(zipns);

									var zipalerts = document.createElement("script");
									zipalerts.type = "text/javascript";
									zipalerts.src = "js/zipalerts.js";
									$("head").append(zipalerts);
							}

							$('#join-us-form').fadeOut( "slow", function() {
								    // Animation complete.
							});
							$("html, body").animate({ scrollTop: 0 }, "slow");
							return true;
						}
					});

					document.getElementById("smart-form").reset();

				}
			}).validate({
				errorClass: "state-error",
				validClass: "state-success",
				errorElement: "em",
				onkeyup: false,
				onclick: false,
				rules: {
					driver_fname:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_lname:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_email: {
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						email: true
					},
					work_phone: {
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						number: {
		                   depends: function(element) {
		                     if (numberRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (numberRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						phoneUS: {
		                   depends: function(element) {
		                     if (phoneRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (phoneRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					home_phone: {
						required: false,
						number: {
		                   depends: function(element) {
		                     if (numberRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (numberRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						phoneUS: {
		                   depends: function(element) {
		                     if (phoneRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (phoneRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_address:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_zip:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						zipcodeUS: {
		                   depends: function(element) {
		                     if (zipCodeRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (zipCodeRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_city:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driver_state:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					app_type:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					veteran_status:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					honorable_discharge:{
						required: "#veteran_status_y:checked"
					},
					cdl_holder:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					agree_to_terms:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					company_driver:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					company_driver_start:{
						required: "#company_driver_y:checked"
					},
					company_driver_end:{
						required: "#company_driver_y:checked"
					},
/*
					other_licenses:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'dlstate[0]':{
						required: "#other_licenses_y:checked"
					},
*/
					moving_violations:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'moving_violations_date[0]':{
						required: "#moving_violations_y:checked"
					},
					'violation_type[0]':{
						required: "#moving_violations_y:checked"
					},
					'violation_city[0]':{
						required: "#moving_violations_y:checked"
					},
					'violation_county[0]':{
						required: "#moving_violations_y:checked"
					},
					'violation_state[0]':{
						required: "#moving_violations_y:checked"
					},
					'violation_details[0]':{
						required: "#moving_violations_y:checked"
					},
					'fines_and_fees[0]':{
						required: "#moving_violations_y:checked"
					},
					accidents:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'accident_date[0]':{
						required: "#accidents_y:checked"
					},
					'accident_ticket[0]':{
						required: "#accidents_y:checked"
					},
					'accident_injuries[0]':{
						required: "#accidents_y:checked"
					},
					'accident_fatalities[0]':{
						required: "#accidents_y:checked"
					},
					'accident_preventable[0]':{
						required: "#accidents_y:checked"
					},
					'accident_fault[0]':{
						required: "#accidents_y:checked"
					},
					'accident_city[0]':{
						required: "#accidents_y:checked"
					},
					'accident_county[0]':{
						required: "#accidents_y:checked"
					},
					'accident_state[0]':{
						required: "#accidents_y:checked"
					},
					'accident_damage_amount[0]':{
						required: "#accidents_y:checked"
					},
					'accident_details[0]':{
						required: "#accidents_y:checked"
					},
					dui_dwi:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'dui_dwi_date[0]':{
						required: "#dui_dwi_y:checked"
					},
					'dui_dwi_city[0]':{
						required: "#dui_dwi_y:checked"
					},
					'dui_dwi_county[0]':{
						required: "#dui_dwi_y:checked"
					},
					'dui_dwi_state[0]':{
						required: "#dui_dwi_y:checked"
					},
					'dui_dwi_details[0]':{
						required: "#dui_dwi_y:checked"
					},
					suspendedrevoked:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'suspendedrevoked_start_date[0]':{
						required: "#suspendedrevoked_y:checked"
					},
					'suspendedrevoked_end_date[0]':{
						required: "#suspendedrevoked_y:checked"
					},
					'suspendedrevoked_city[0]':{
						required: "#suspendedrevoked_y:checked"
					},
					'suspendedrevoked_county[0]':{
						required: "#suspendedrevoked_y:checked"
					},
					'suspendedrevoked_state[0]':{
						required: "#suspendedrevoked_y:checked"
					},
					'license_reinstated[0]':{
						required: "#suspendedrevoked_y:checked"
					},
/*
					illegal_drugs:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					illegal_drugs_date:{
						required: "#illegal_drugs_y:checked"
					},
*/
					drugtest:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					drugtest_date:{
						required: "#drugtest_y:checked"
					},
					drugtest_details:{
						required: false
					},
					felony:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'felony_date[0]':{
						required: "#felony_y:checked"
					},
					'felony_convicted[0]':{
						required: "#felony_y:checked"
					},
					'felony_incarcerated[0]':{
						required: "#felony_y:checked"
					},
					'felony_city[0]':{
						required: "#felony_y:checked"
					},
					'felony_county[0]':{
						required: "#felony_y:checked"
					},
					'felony_state[0]':{
						required: "#felony_y:checked"
					},
					'felony_details[0]':{
						required: "#felony_y:checked"
					},
					misdemeanor:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'misdemeanor_date[0]':{
						required: "#misdemeanor_y:checked"
					},
					'misdemeanor_convicted[0]':{
						required: "#misdemeanor_y:checked"
					},
					'misdemeanor_city[0]':{
						required: "#misdemeanor_y:checked"
					},
					'misdemeanor_county[0]':{
						required: "#misdemeanor_y:checked"
					},
					'misdemeanor_state[0]':{
						required: "#misdemeanor_y:checked"
					},
					'misdemeanor_details[0]':{
						required: "#misdemeanor_y:checked"
					},
					schoolgrad:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					school_grad_name:{
						required: "#school_y:checked"
					},
					school_grad_date:{
						required: "#school_y:checked"
					},
					school_grad_city:{
						required: "#school_y:checked"
					},
					school_grad_state:{
						required: "#school_y:checked"
					},
					school_grad_zip:{
						required: "#school_y:checked",
						zipcodeUS: {
		                   depends: function(element) {
		                     if (zipCodeRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (zipCodeRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_start[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_end[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       if($("#workhiscurrent_y").prop('checked') == true)
		                        {
							    return false;
								}
								else
								{
								return true;
								}
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_company[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_title[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_address[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_city[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_state[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_zip[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                },
						zipcodeUS: {
		                   depends: function(element) {
		                     if (zipCodeRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (zipCodeRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_phone[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                },
						phoneUS: {
		                   depends: function(element) {
		                     if (phoneRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (phoneRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_reason[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					workhiscurrent_y:{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
			                     if ($("#workhisend").is(':empty'))
			                     {
			                       return true;
		                         }
			                     else
			                     {
			                       return false;
			                     }
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					'work_history_contact[0]':{
						required: {
		                   depends: function(element) {
		                     if ($("#app_version").val() == "Long")
		                     {
		                       return true;
	                         }
		                     else if ($("#app_version").val() == "Short")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					Disclosure_and_Everify_terms:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					FMCSA_terms:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					PSP_terms:{
						required:"#cdl_holder_y:checked"
					},
					signature_data:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					signature_data:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					disclosure_name:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					disclosure_ssn:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
						digits: {
		                   depends: function(element) {
		                     if (digitsRules == "true")
		                     {
		                       return true;
	                         }
		                     else if (digitsRules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                },
		                maxlength: 9,
		                minlength: 9
					},
					disclosure_dlNumber:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					driverlicense_state:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
		            },

					driver_birthdate:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					},
					disclosure_dlexp:{
						required: {
		                   depends: function(element) {
		                     if (rules == "true")
		                     {
		                       return true;
	                         }
		                     else if (rules == "false")
		                     {
		                       return false;
		                     }
		                   }
		                }
					}
				},
				messages: {
					driver_fname: {
						required: "Please enter First Name"
					},
					driver_lname: {
						required: "Please enter Last Name"
					},
					driver_email: {
						required: 'Please enter your email',
						email: 'You must enter a VALID email'
					},
					work_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},
					home_phone: {
						required: 'Please enter your telephone',
						number: 'Please enter numbers only'
					},
					driver_address: {
						required: "Please enter your address"
					},
					driver_zip:{
						required: 'Please enter your zip code'
					},
					driver_city:{
						required: 'Please enter your city'
					},
					driver_state:{
						required: 'Please select your state'
					},
					app_type:{
						required: 'Please select your driving experience'
					},
					veteran_status:{
						required: 'Please select veteran status'
					},
					agree_to_terms:{
						required: '    Please agree to terms and conditions'
					},
					Disclosure_and_Everify_terms:{
						required: '    Please agree to Disclosure and EVerify Terms'
					},
					FMCSA_terms:{
						required: '    Please agree to FMCSA Terms'
					},
					PSP_terms:{
						required: '    Please agree to PSP Terms'
					},
				},
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.field').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.field').removeClass(errorClass).addClass(validClass);
				},
				errorPlacement: function(error, element) {
					if (element.is(":radio") || element.is(":checkbox")) {
						element.closest('.option-group').after(error);
					} else {
						error.insertAfter(element.parent());
					}
				}

			});

			/* Reload Captcha
			----------------------------------------------- */
			function reloadCaptcha(){ $("#captchax").attr("src","php/captcha/captcha.php?r=" + Math.random()); }
			$('.captcode').click(function(e){
				e.preventDefault();
				reloadCaptcha();
			});

			/* Project datepicker range
			----------------------------------------------- */
			$("#disclosure_dlexp").datepicker({
				dateFormat: 'mm/dd/yy',
				maxDate: null,
				changeMonth: true,
				changeYear: true,
				yearRange: "-0:+50", // next fifty years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});


			$(".otherdlexp").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-50y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-50:+0", // next fifty years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#driver_birthdate").datepicker({
				dateFormat: "mm/dd/yy",
				maxDate: "-21y",
				changeMonth: true,
				changeYear: true,
				yearRange: '-100:-21', // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#driver_birthdate2").datepicker({
				dateFormat: 'mm/dd/yy',
			    minDate: new Date(1900,1-1,1), maxDate: '-18Y',
			    dateFormat: 'dd/mm/yy',
			    defaultDate: new Date(1970,1-1,1),
			    changeMonth: true,
			    changeYear: true,
			    yearRange: '-110:-18',
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#company_driver_start").datepicker({
				dateFormat: 'mm/dd/yy',
				maxDate: "-1",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( "#company_driver_end" ).datepicker( "option", "minDate", selectedDate );
					}
			});

			$("#company_driver_end").datepicker({
				dateFormat: 'mm/dd/yy',
				maxDate: "-1",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( "#company_driver_start" ).datepicker( "option", "maxDate", selectedDate );
					}
			});

			$(".moving_violations_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-5y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});


			$(".accidents_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-5y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".dui_dwi_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-21y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".suspendedrevoked_start_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-21y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
					}
			});

			$(".suspendedrevoked_end_date").datepicker({
				dateFormat: 'mm/dd/yy',
				//maxDate: "-21y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
					}
			});

			$("#illegal_drugs_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-1y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#drugtest_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-100y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".felony_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-100y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".incarceration_release_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-100y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".misdemeanor_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-100y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$("#school_grad_date").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-5y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			});

			$(".work_history_start").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-30y",
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
					}
			});


			$(".work_history_end").datepicker({
				dateFormat: 'mm/dd/yy',
				minDate: "-3y",
				changeMonth: true,
				changeYear: true,
				//showButtonPanel: true,
				currentText: "Current",
				yearRange: "-100:+0", // last hundred years
				numberOfMonths: 1,
				prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
				nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
					}
			});

	});


	$(document).ready(function(){
		$("input[id^='cdl_holder']").change(function(){
			   var cdlHold = $(this).val();
			    if(cdlHold == "N") {

				    $('#app_type').children().remove();
			        $("#app_type").append('<option value="SC"></option>');

/*
			    $("#app_type").val(function(i, v) { //index, current value
				  return v.replace(apptype,"SC");
				});
*/
			        $("#app_type").hide();
			        $("#PSP").hide();
		            $("#EVERIFY").show();
					$("#FMCSA").show();

			    }
		else if (cdlHold == "Y") {
// 			        $("#app_type").val("SR");


				    $('#app_type').children().remove();
				    $("#app_type").append('<option value="">select your CDL driving experience</option>');
			        $("#app_type").append('<option value="SR">I have less than 3 months experience</option>');
			        $("#app_type").append('<option value="T">I have more than 3 months but less than 6 months experience</option>');
			        $("#app_type").append('<option value="D">I have at least 6 months experience</option>');
			        //$("#app_type").append('<option value="D">I have at least 19 months experience in the last 36 months</option>');


			        $("#app_type").show();
			        $("#PSP").show();
		            $("#EVERIFY").show();
					$("#FMCSA").show();


			    }

			});
		});

	$(function(){
	  	$('.smartfm-ctrl').formShowHide();
	});

	$(function(){
	$('ul[role="tablist"]').hide();
	});

$(document).ready(function(){
	$("#driver_birthdate").mask('99/99/9999', {placeholder:'_'});
	$("#disclosure_dlexp").mask('99/99/9999', {placeholder:'_'});
});

	$(function() {

			/* Simple Cloning
			------------------------------------------------- */
			$('#simple-clone').cloneya({
				serializeID: true,
				serializeIndex: true
			});

			/* Group Cloning
			------------------------------------------------- */
			$('#clone-group-fields').cloneya({
				serializeID: true,
				serializeIndex: true
			});

			/* Group Cloning
			------------------------------------------------- */
			$('#clone-group-fields2').cloneya({
				serializeID: true,
				serializeIndex: true
			});
			/* Group Cloning
			------------------------------------------------- */
			$('#clone-group-fields3').cloneya({
				serializeID: true,
				serializeIndex: true
			});
			/* Group Cloning
			------------------------------------------------- */
			$('#clone-group-fields4').cloneya({
				serializeID: true,
				serializeIndex: true
			});


			/* MIN MAX Cloning
			------------------------------------------------- */
			$('#clone-min-max').cloneya({
				maximum: 3,
				minimum: 2
			});



			/* MOVING VIOLATIONS CLONING
			----------------------------------------------*/
			$('#clone-animate').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.moving_violations_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.moving_violations_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.moving_violations_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			});

			/* ACCIDENTS CLONING
			------------------------------------------------- */
			$('#clone-animate2').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.accidents_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			    toClone.find('.accdamageamount').each(function(){
			        $(this).maskMoney('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.accidents_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			    toClone.find('.accdamageamount').each(function(){
			        $(this).maskMoney({allowNegative: true, thousands:',', decimal:'.', affixesStay: false});
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.accidents_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
												        });
			    });
			    newclone.find('.accdamageamount').each(function(){
			        $(this).maskMoney({allowNegative: true, thousands:',', decimal:'.', affixesStay: false});
			    });
			});

			/* DUI CLONING
			------------------------------------------------- */
			$('#clone-animate3').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.dui_dwi_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.dui_dwi_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-50y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.dui_dwi_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-50y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			});

			/* SUSPENDED REVOKED CLONING
			------------------------------------------------- */
			$('#clone-animate4').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.suspendedrevoked_start_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			    toClone.find('.suspendedrevoked_end_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.suspendedrevoked_start_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    toClone.find('.suspendedrevoked_end_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.suspendedrevoked_start_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".suspendedrevoked_end_date" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    newclone.find('.suspendedrevoked_end_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-5y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".suspendedrevoked_start_date" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			});

			/* FELONY CLONING
			------------------------------------------------- */
			$('#clone-animate5').cloneya({
			    maximum: 50,
			    serializeIndex: true,
			    serializeID: true

			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.felony_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			    toClone.find('.smartfm-ctrl1').each(function(){
			        $(this).formShowHide('destroy');
			    });
			    toClone.find('.incarceration_release_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.felony_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".incarceration_release_date" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    toClone.find('.smartfm-ctrl1').each(function(){
			        $(this).formShowHide();
			    });
			    toClone.find('.incarceration_release_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".felony_date" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.felony_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".incarceration_release_date" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    newclone.find('.smartfm-ctrl1').each(function(){
			        $(this).formShowHide();
			    });
			    newclone.find('.incarceration_release_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".felony_date" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			});

			/* MISDEMEANOR CLONING
			------------------------------------------------- */
			$('#clone-animate6').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.misdemeanor_date').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.misdemeanor_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.misdemeanor_date').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-100y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			});

			/* OTHER DL CLONING
			------------------------------------------------- */
			$('#clone-animate8').cloneya({
			    maximum: 50,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.otherdlexp').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.otherdlexp').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-50y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-50:+0", // last fifty years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.otherdlexp').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-50y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-50:+0", // last fifty years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>'
			        });
			    });
			});


			/* Animated Cloning with custom events
			------------------------------------------------- */
			$('#clone-animate7').cloneya({
			    maximum: 50,
			    //preserveChildCount: true,
			    serializeID: true,
			    serializeIndex: true
			}).on('before_clone.cloneya', function(event, toClone){
			    toClone.find('.work_history_start').each(function(){
			        $(this).datepicker('destroy');
			    });
			    toClone.find('.work_history_end').each(function(){
			        $(this).datepicker('destroy');
			    });
			}).on('after_clone.cloneya', function (event, toClone, newclone) {
			    toClone.find('.work_history_start').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-30y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    toClone.find('.work_history_end').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-30y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			}).on('after_append.cloneya', function (event, toClone, newclone) {
			    newclone.find('.work_history_start').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-30y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
				onClose: function( selectedDate ) {
						$( ".work_history_end" ).datepicker( "option", "minDate", selectedDate );
					}
			        });
			    });
			    newclone.find('.work_history_end').each(function(){
			        $(this).datepicker({
						            dateFormat: 'mm/dd/yy',
									minDate: "-30y",
									changeMonth: true,
									changeYear: true,
									yearRange: "-100:+0", // last hundred years
									numberOfMonths: 1,
									prevText: '<span class="glyphicon glyphicon-chevron-left"></span>',
									nextText: '<span class="glyphicon glyphicon-chevron-right"></span>',
					onClose: function( selectedDate ) {
						$( ".work_history_start" ).datepicker( "option", "maxDate", selectedDate );
					}
			        });
			    });
			});
		});



$(document).ready(function(){
	var myw = $("#smart-form").width();
	$sigDiv = $("#signature").jSignature({width: myw, height: 88,'UndoButton':true});
});


$(document).ready(function(){
    $("#disclosure_name").focus(function(){
		$("#signaturedata:empty").text(function(){
			$("#signature_data_error").text("This field is required.");
		});
    });
});


$(document).ready(function(){
 	$("#signature").bind('change', function(e) {
    	var datapair = $("#signature").jSignature("getData", "image");
		$("#signaturedata").val(datapair[1]);
		$("#signature_data_error").hide();
	});
});

////////////////////// Begin Dynamic Gemini App Length ////////////////



$(document).ready(function() {
         
$("#frapdiv").hide();

         $('#driver_state').on('change', function(e){

    var cat_id = e.target.value;

       //ajax

       $.get('/api/category-dropdown?cat+id=' + cat_id, function(data){

           //success data
           $('#frap_school').empty();

           $('#frap_school').append('<option value="">Current School:</option>'
               );

           $.each(data, function(index, subcatObj){

               $('#frap_school').append('<option value="'+ subcatObj.schoolcode + '"> '
               + subcatObj.schoolprettyname + '</option>');


               


           });

           $("#frapdiv").show();

       });


   });
    });


$(document).ready(function() {
         


         $('#frap_school').on('change', function(e){

    var rec_cat_id = e.target.value;

       //ajax

       $.get('/api/rec-category-dropdown?rec+cat+id=' + rec_cat_id, function(data){

           //success data
           $('#frap_recruiter').empty();

           //$('#frap_recruiter').val( subcatObj.recruiter );

           $.each(data, function(index, subcatObj){

               $('#frap_recruiter').val( subcatObj.recruiter );
               //$('#frap_recruiter').append( 'value="'+ subcatObj.recruiter + '"' );
               // $('#frap_school').append('<option value="'+ subcatObj.schoolcode + '"> '
               // + subcatObj.schoolprettyname + '</option>');


               


           });

          

       });


   });
    });




//////////// Steps: 0=Landing page 1=Driving History 2=Criminal History 3=Work History 4=Disclaimer


$(document).ready(function()
	{
		//////////// Steps: 0=Landing page 1=Driving History 2=Criminal History 3=Work History 4=Disclaimer

				$("select[id^='driver_state']").change(function()
					{
						var residencestate = $(this).val();
						var statesarray = ['LA','MS','NJ','NY','CA','GA','IN','TX','UT'];
						if (statesarray.indexOf(residencestate) !== -1)
							{
								$("input[id^='cdl_holder']").change(function()
									{
										var cdlHold = $(this).val();
										if (cdlHold == "N")
											{
												$("#app_version").val("Long");
												$("#smart-form").steps("enableStep", 1);
												$("#smart-form").steps("enableStep", 2);
												$("#smart-form").steps("enableStep", 3);

											}
											else if (cdlHold == "Y")
											{
												$("#app_version").val("Long");
												$("#smart-form").steps("enableStep", 1);
												$("#smart-form").steps("enableStep", 2);
												$("#smart-form").steps("enableStep", 3);

											}
									}
								);
							}
						else if (statesarray.indexOf(residencestate) === -1)
							{
								$("input[id^='cdl_holder']").change(function()
									{
										var cdlHold = $(this).val();
										if (cdlHold == "N")
											{
												$("#app_version").val("Long");
												$("#smart-form").steps("enableStep", 1);
												$("#smart-form").steps("enableStep", 2);
												$("#smart-form").steps("enableStep", 3);

											}
											else if (cdlHold == "Y")
											{
												$("#app_version").val("Long");
												$("#smart-form").steps("enableStep", 1);
												$("#smart-form").steps("enableStep", 2);
												$("#smart-form").steps("enableStep", 3);

											}
									}
								);
							}
					}
				);

	}
);



////////////////////// End Dynamic Gemini App Length ////////////////


