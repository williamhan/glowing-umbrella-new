/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    
    $("#redbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-red", theme: "theme-red", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Red! Refresh to see changes.");
            }
        });
    });

    $("#defaultbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-primary", theme: "theme-primary", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Primary! Refresh to see changes.");
            }
        });
    });

    $("#greenbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-green", theme: "theme-green", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Green! Refresh to see changes.");
            }
        });
    });

    $("#yellowbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-yellow", theme: "theme-yellow", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Yellow! Refresh to see changes.");
            }
        });
    });

    $("#purplebutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-purple", theme: "theme-purple", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Purple! Refresh to see changes.");
            }
        });
    });

    $("#bluebutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-blue", theme: "theme-blue", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Blue! Refresh to see changes.");
            }
        });
    });

    // $("#redbutton").click(function(){
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-red");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-red");
    // });
    // $("#defaultbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-primary");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-primary");
    // });
    // $("#greenbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-green");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-green");
    // });
    // $("#yellowbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-yellow");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-yellow");
    // });
    // $("#purplebutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").addClass("header-purple");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").addClass("theme-purple");
    // });
    // $("#bluebutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-blue");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-blue");
    // });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "welcomeemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                 alert("Welcome Packet Sent!");
            }
        });
    });
    $("#PastEmpButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "pastempemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("PEV Sent!");
            }
        });
    });
    $("#TravelEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/travelemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Travel Email Sent!");
            }
        });
    });
    $("#LeftMessageEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/leftmessageemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ConfirmationEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/confirmationemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ThreePApprovalEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/threepapprovalemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#WAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/wareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#VAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/vareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#NHReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nhreleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */

$(document).ready(function(){
    $("#FindGhButton").click(function(){


        $('#ghmapmodal').modal('show');

    });
    $("#OrderGhButton").click(function(){
        alert("Not Yet Implemented");
    });

    var customIcons = {
        restaurant: {
            icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'
        },
        bar: {
            icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png'
        }
    };

    function mapload() {
        var map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng($('#address1_latitude').val(), $('#address1_longitude').val()),
            zoom: 10,
            mapTypeId: 'roadmap'
        });

        var marker = new google.maps.Marker({
            map: map,
            draggable: false,
            position: new google.maps.LatLng($('#address1_latitude').val(), $('#address1_longitude').val())
        });

        var infoWindow = new google.maps.InfoWindow;

        // Change this depending on the name of your PHP file
        downloadUrl("dblogintwo.php", function(data) {
            var xml = data.responseXML;

            //alert(data.responseXML);

            var markers = xml.documentElement.getElementsByTagName("marker");
            for (var i = 0; i < markers.length; i++) {
                var name = markers[i].getAttribute("name");
                var address = markers[i].getAttribute("address");
                var type = markers[i].getAttribute("type");
                var point = new google.maps.LatLng(
                    parseFloat(markers[i].getAttribute("lat")),
                    parseFloat(markers[i].getAttribute("lng")));
                var html = "<b>" + name + "</b> <br/>" + address;
                var icon = customIcons[type] || {};
                var marker = new google.maps.Marker({
                    map: map,
                    position: point,
                    icon: icon.icon
                });
                bindInfoWindow(marker, map, infoWindow, html);
            }
        });
    }

    function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        });
    }

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing;
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.overrideMimeType('text/xml');
        request.send(null);
    }

    function doNothing() {};
});
/**
 * Created by williamhan on 11/18/16.
 */

$(document).ready(function(){
    $("#RunMvrButton").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: '//drivecretest.com/nosdev/mvr.php',
           // url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderhireright').show();
            },
            complete: function(){
                $('#loaderhireright').hide();
            },
            success: function(data){
                $('#ReviewMvr').attr('href',data);
                $( '#ReviewMvr' ).show( "slow" );
                //alert("MVR Ordered!");
            }
        });
    });

    $("#RunEhfPackageButton").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: '//drivecretest.com/nosdev/ehf.php',
            // url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderhireright').show();
            },
            complete: function(){
                $('#loaderhireright').hide();
            },
            success: function(data){
                $('#ReviewEhfPackage').attr('href',data);
                $( '#ReviewEhfPackage' ).show( "slow" );
                //alert("MVR Ordered!");
            }
        });
    });


    $("#ReviewMvr").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: '//drivecretest.com/nosdev/mvrreview.php',
            type: "post",
            data: $("#mainform").serialize(),
            success: function(data){
                $('#ReviewMvr').attr('href',data);
                // $( '#ReviewEhfPackage' ).show( "fast" );
                //alert("MVR Ordered!");
            }
        });
    });


    $("#ReviewEhfPackage").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: '//drivecretest.com/nosdev/ehfreview.php',
            type: "post",
            data: $("#mainform").serialize(),
            success: function(data){
                $('#ReviewEhfPackage').attr('href',data);
                // $( '#ReviewEhfPackage' ).show( "fast" );
                //alert("MVR Ordered!");
            }
        });
    });

    $.ajax({
        url: '//drivecretest.com/nosdev/mvrreview.php',
        type: "post",
        data: $("#mainform").serialize(),
        success: function(data){
            $('#ReviewMvr').attr('href',data);
            $( '#ReviewMvr' ).show( "fast" );
            //alert("MVR Ordered!");
        }
    });

    $.ajax({
        url: '//drivecretest.com/nosdev/ehfreview.php',
        type: "post",
        data: $("#mainform").serialize(),
        success: function(data){
            $('#ReviewEhfPackage').attr('href',data);
            $( '#ReviewEhfPackage' ).show( "fast" );
            //alert("MVR Ordered!");
        }
    });
});

/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#NCRecruiter").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: 'namechange',
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCPermitSpec").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "namechangeps",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCNo").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny",
            type: "post",
            data: $("#allnames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJacob").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jacob",
            type: "post",
            data: $("#jacobnames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJessica").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jessica",
            type: "post",
            data: $("#jessicanames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoLisa").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/lisa",
            type: "post",
            data: $("#lisanames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoNick").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/nick",
            type: "post",
            data: $("#nicknames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoSepeti").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/sepeti",
            type: "post",
            data: $("#sepetinames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "welcomesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#PastEmpSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "pastempsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#TravelSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "travelsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#LeftMessageSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "leftmessagesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#ConfirmationSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "confirmationsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#LastChanceSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "lastchancesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#WAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "wareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#VAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "vareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#NHReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "nhreleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#DIYSmsButton").click(function(){
        var status = $("#mainform").serialize();
        var text = $("#Sms").val();
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "diysms",
            type: "post",
            data: $("#mainform").serialize() + '&Sms=' + text,
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
});
/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#DDTwoFourteenButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/dd214",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ComLetterButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/commandersletter",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});
//# sourceMappingURL=buttons.js.map
