<?php
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

use App\SchoolInfo;
use App\NameChange;
use App\User;
use Spatie\Sitemap\SitemapGenerator;
use Carbon\Carbon;
use App\BillTestModel;


/*
--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\AuthController@login');
// First Login Page

Route::post('/', 'Auth\AuthController@postLogin');
// Post First Login Page

Route::get('/auth/token/{token}', 'Auth\AuthController@authenticate');
// Login To Nos Link

Route::get('/logout', 'Auth\AuthController@logout');
// Logout Of Nos

Route::get('/dashboard', function () {
	
	$agent = Auth::user()->name;

	$sysuserid = Auth::user()->sysuserid;

	Session::put('agent', $agent);

	Session::put('sysuserid', $sysuserid);

	//$sessiondata = Session::all();

	//$appdata = BillTestModel::scribeapplist();

	$todaydate = Carbon::today()->subDays(30);

	//var_dump($sessiondata);
	
	 return view('examplenobuttons')
	 					->with('agent', $agent)
	 					//->with('appdata', BillTestModel::scribeapplist())
	 					->with('todaydate', $todaydate);
}
)->middleware('auth')->name('mainpage');
;

Route::get('/namechange/{guid}', 'NameChangeController@buildagain');
// bill

Route::post('/namechange/{guid}', 'NameChangeController@scribenamechange');
// bill

Route::get('/managers', function () {
	return view('login');
}
);

Route::post('/dashboard', 'NosController@index');

Route::post('/dashboardapplist', 'NosController@indexnoappidwithmodel');

Route::post('/colorchange', 'NosController@colorchange');

Route::post('/welcomeemail', 'EmailController@welcomeemail');

Route::post('/welcomesms', 'SmsController@welcomesms');

Route::post('/pastempemail', 'EmailController@pastempemail');

Route::post('/travelemail', 'EmailController@travelemail');

Route::post('/leftmessageemail', 'EmailController@leftmessageemail');

Route::post('/confirmationemail', 'EmailController@confirmationemail');

Route::post('/threepapprovalemail', 'EmailController@threepapprovalemail');

Route::post('/wareleaseemail', 'EmailController@wareleaseemail');

Route::post('/vareleaseemail', 'EmailController@vareleaseemail');

Route::post('/nhreleaseemail', 'EmailController@nhreleaseemail');

Route::post('/pastempsms', 'SmsController@pastempsms');

Route::post('/travelsms', 'SmsController@travelsms');

Route::post('/leftmessagesms', 'SmsController@leftmessagesms');

Route::post('/confirmationsms', 'SmsController@confirmationsms');

Route::post('/lastchancesms', 'SmsController@lastchancesms');

Route::post('/wareleasesms', 'SmsController@wareleasesms');

Route::post('/vareleasesms', 'SmsController@vareleasesms');

Route::post('/nhreleasesms', 'SmsController@nhreleasesms');

Route::post('/diysms', 'SmsController@diysms');

Route::post('/dd214', 'VeteransController@dd214email');

Route::post('/commandersletter', 'VeteransController@commandersletteremail');

Route::post('/namechange', 'NameChangeController@namechange');

Route::post('/namechangeps', 'NameChangeController@namechangeps');

Route::post('/namechange/deny', 'NameChangeController@deny');
// bill

Route::post('/namechange/deny/jacob', 'NameChangeController@deny');
// jessica

Route::post('/namechange/deny/jessica', 'NameChangeController@deny');
// jacob

Route::post('/namechange/deny/lisa', 'NameChangeController@deny');
// lisa

Route::post('/namechange/deny/nick', 'NameChangeController@deny');
// nick

Route::post('/namechange/deny/sepeti', 'NameChangeController@deny');
// sepeti

Route::post('/ghfinder', function() {
	$data = Request::all();
	return view('modals.ghmodals.ghfinder', $data);
}
);


Route::get('/schoolinfo/edit', array('as' => 'schoolinfo.edit', function()
{
	// 	return our view and Nerd information
	    return View::make('table') // 	pulls app/views/nerd-edit.blade.php
	    ->with('schoolinfo', SchoolInfo::where('sch_id', '>', 0)
	        ->get());
}
));

Route::resource('/phoneapp', 'PhoneAppController');

Route::resource('/schools', 'NerdController');

Route::resource('/schoolinfo', 'NerdInfoController');

Route::resource('/transportationinfo', 'TransportationController');

Route::resource('/scheduleinfo', 'ScheduleInfoController');

Route::resource('/docsinfo', 'DocsInfoController');

Route::resource('/feesinfo', 'FeesInfoController');

Route::resource('/housinginfo', 'HousingInfoController');

Route::resource('/userinfo', 'UserController');


