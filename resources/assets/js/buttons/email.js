/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "welcomeemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                 alert("Welcome Packet Sent!");
            }
        });
    });
    $("#PastEmpButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "pastempemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("PEV Sent!");
            }
        });
    });
    $("#TravelEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/travelemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Travel Email Sent!");
            }
        });
    });
    $("#LeftMessageEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/leftmessageemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ConfirmationEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/confirmationemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ThreePApprovalEmailButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/threepapprovalemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#WAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/wareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#VAReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/vareleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#NHReleaseButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nhreleaseemail",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loaderemail').show();
            },
            complete: function(){
                $('#loaderemail').hide();
            },
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});