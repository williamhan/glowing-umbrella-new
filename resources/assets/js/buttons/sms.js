/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#WelcomePacketSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "welcomesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#PastEmpSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "pastempsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#TravelSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "travelsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#LeftMessageSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "leftmessagesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#ConfirmationSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "confirmationsms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#LastChanceSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "lastchancesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#WAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "wareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#VAReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "vareleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#NHReleaseSmsButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "nhreleasesms",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
    $("#DIYSmsButton").click(function(){
        var status = $("#mainform").serialize();
        var text = $("#Sms").val();
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "diysms",
            type: "post",
            data: $("#mainform").serialize() + '&Sms=' + text,
            beforeSend: function() {
                $('#loadersms').show();
            },
            complete: function(){
                $('#loadersms').hide();
            },
            success: function(){
                alert("Text Sent Successfully!");
            }
        });
    });
});