/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    
    $("#redbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-red", theme: "theme-red", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Red! Refresh to see changes.");
            }
        });
    });

    $("#defaultbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-primary", theme: "theme-primary", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Primary! Refresh to see changes.");
            }
        });
    });

    $("#greenbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-green", theme: "theme-green", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Green! Refresh to see changes.");
            }
        });
    });

    $("#yellowbutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-yellow", theme: "theme-yellow", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Yellow! Refresh to see changes.");
            }
        });
    });

    $("#purplebutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-purple", theme: "theme-purple", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Purple! Refresh to see changes.");
            }
        });
    });

    $("#bluebutton").click(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        var userid = $("#agent_id").val();
        var userfname = $("#agent_fname").val();

        $.ajax({
            url: "colorchange",
            //url: 'https://699d1fphyyjd.runscope.net',
            type: "post",
            data: { header: "header-blue", theme: "theme-blue", username: userid, userfname: userfname },
            success: function(){
                 alert("Color Set To Blue! Refresh to see changes.");
            }
        });
    });

    // $("#redbutton").click(function(){
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-red");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-red");
    // });
    // $("#defaultbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-primary");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-primary");
    // });
    // $("#greenbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-green");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-green");
    // });
    // $("#yellowbutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-yellow");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-yellow");
    // });
    // $("#purplebutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-blue");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").addClass("header-purple");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-blue");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").addClass("theme-purple");
    // });
    // $("#bluebutton").click(function(){
    //     $(".form-header").removeClass("header-red");
    //     $(".form-header").removeClass("header-primary");
    //     $(".form-header").removeClass("header-green");
    //     $(".form-header").removeClass("header-yellow");
    //     $(".form-header").removeClass("header-purple");
    //     $(".form-header").addClass("header-blue");
    //     $(".form-body").removeClass("theme-primary");
    //     $(".form-body").removeClass("theme-red");
    //     $(".form-body").removeClass("theme-green");
    //     $(".form-body").removeClass("theme-yellow");
    //     $(".form-body").removeClass("theme-purple");
    //     $(".form-body").addClass("theme-blue");
    // });
});