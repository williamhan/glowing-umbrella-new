/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#NCRecruiter").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: 'namechange',
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCPermitSpec").click(function(){
        // $.ajaxSetup({
        //     headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        // });
        $.ajax({
            url: "namechangeps",
            type: "post",
            data: $("#mainform").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Requested");
            }
        });
    });

    $("#NCNo").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny",
            type: "post",
            data: $("#allnames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJacob").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jacob",
            type: "post",
            data: $("#jacobnames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoJessica").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/jessica",
            type: "post",
            data: $("#jessicanames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoLisa").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/lisa",
            type: "post",
            data: $("#lisanames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoNick").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/nick",
            type: "post",
            data: $("#nicknames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });

    $("#NCNoSepeti").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/nos/namechange/deny/sepeti",
            type: "post",
            data: $("#sepetinames").serialize(),
            beforeSend: function() {
                $('#loader').show();
            },
            complete: function(){
                $('#loader').hide();
            },
            success: function(){
                alert("Name Change Denied");
            }
        });
    });
});