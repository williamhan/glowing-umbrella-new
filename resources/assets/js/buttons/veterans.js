/**
 * Created by williamhan on 11/18/16.
 */
$(document).ready(function(){
    $("#DDTwoFourteenButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/dd214",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
    $("#ComLetterButton").click(function(){
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        $.ajax({
            url: "/commandersletter",
            type: "post",
            data: $("#mainform").serialize(),
            success: function(){
                alert("Sent To URL!");
            }
        });
    });
});