/**
 * Created by williamhan on 12/22/16.
 */
$(document).ready(function(){
    function autoSave()
    {
        var driver_guid = $('#driver_guid').val();
        var driver_email = $('#driver_email').val();
        var driver_fname = $('#driver_fname').val();
        var driver_lname = $('#driver_lname').val();
        var record_source = $('#record_source').val();
        var how_heard = $('#how_heard').val();
        var app_version = $('#app_version').val();
        var var_track = $('#var_track').val();
        var work_phone = $('#work_phone').val();
        var home_phone = $('#home_phone').val();
        var driver_address = $('#driver_address').val();
        var driver_zip = $('#driver_zip').val();
        var driver_city = $('#driver_city').val();
        var driver_state = $('#driver_state').val();
        var cdl_holder = $('input:radio[name=cdl_holder]:checked').val();
        var app_type = $('#app_type').val();
        var veteran_status = $('input:radio[name=veteran_status]:checked').val();
        var honorable_discharge = $('#honorable_discharge').val();
        var pass_drug_test = $( 'input:radio[name=pass_drug_test]:checked').val();
        var agree_to_terms = $( 'input:checkbox[name=agree_to_terms]:checked').val();
        var companyDriver = $('input:radio[name=company_driver]:checked').val();
        var company_driver_start = $('#company_driver_start').val();
        var company_driver_end = $('#company_driver_end').val();
        var moving_violations = $('input:radio[name=moving_violations]:checked').val();
        var accidents = $('input:radio[name=accidents]:checked').val();
        var dui_dwi = $('input:radio[name=dui_dwi]:checked').val();
        var suspendedrevoked = $('input:radio[name=suspendedrevoked]:checked').val();
        var drugtest = $('input:radio[name=drugtest]:checked').val();
        var drugtest_date = $('#drugtest_date').val();
        var drugtest_details = $('#drugtest_details').val();
        var felony = $('input:radio[name=felony]:checked').val();
        var misdemeanor = $('input:radio[name=misdemeanor]:checked').val();
        var schoolgrad = $('input:radio[name=schoolgrad]:checked').val();
        var school_grad_name = $('#school_grad_name').val();
        var school_grad_date = $('#school_grad_date').val();
        var school_grad_city = $('#school_grad_city').val();
        var school_grad_state = $('#school_grad_state').val();
        var school_grad_zip = $('#school_grad_zip').val();
        var referencename1 = $('#referencename1').val();
        var referencerelation1 = $('#referencerelation1').val();
        var referencephone1 = $('#referencephone1').val();
        var referencename2 = $('#referencename2').val();
        var referencerelation2 = $('#referencerelation2').val();
        var referencephone2 = $('#referencephone2').val();
        var referencename3 = $('#referencename3').val();
        var referencerelation3 = $('#referencerelation3').val();
        var referencephone3 = $('#referencephone3').val();
        var disclosure_and_everify_terms = $('input:checkbox[name=Disclosure_and_Everify_terms]:checked').val();
        var FMCSA_terms = $('input:checkbox[name=FMCSA_terms]:checked').val();
        var PSP_terms = $('input:checkbox[name=PSP_terms]:checked').val();
        var signaturedata = $('#signaturedata').val();
        var disclosure_name = $('#disclosure_name').val();
        var disclosure_ssn = $('#disclosure_ssn').val();
        var disclosure_dlNumber = $('#disclosure_dlNumber').val();
        var driver_birthdate = $('#driver_birthdate').val();
        var driverlicense_state = $('#driverlicense_state').val();
        var disclosure_dlexp = $('#disclosure_dlexp').val();
        var start_time = $('#start_time').val();
        //if(driver_guid !== '' && start_time !== '')
        //{
        $.ajax({
            url:"new_save_post.php",
            method:"POST",
            data:{driver_guid:driver_guid, driver_email:driver_email, driver_fname:driver_fname, driver_lname:driver_lname, record_source:record_source, how_heard:how_heard, app_version:app_version, var_track:var_track, work_phone:work_phone, home_phone:home_phone, driver_address:driver_address, driver_zip:driver_zip, driver_city:driver_city, driver_state:driver_state, cdl_holder:cdl_holder, app_type:app_type, veteran_status:veteran_status, honorable_discharge:honorable_discharge, pass_drug_test:pass_drug_test, agree_to_terms:agree_to_terms, companyDriver:companyDriver, company_driver_start:company_driver_start, company_driver_end:company_driver_end, moving_violations:moving_violations, accidents:accidents, dui_dwi:dui_dwi, suspendedrevoked:suspendedrevoked, drugtest:drugtest, drugtest_date:drugtest_date, drugtest_details:drugtest_details, felony:felony, misdemeanor:misdemeanor, schoolgrad:schoolgrad, school_grad_name:school_grad_name, school_grad_date:school_grad_date, school_grad_city:school_grad_city, school_grad_state:school_grad_state, school_grad_zip:school_grad_zip, referencename1:referencename1, referencerelation1:referencerelation1, referencephone1:referencephone1, referencename2:referencename2, referencerelation2:referencerelation2, referencephone2:referencephone2, referencename3:referencename3, referencerelation3:referencerelation3, referencephone3:referencephone3, disclosure_and_everify_terms:disclosure_and_everify_terms, FMCSA_terms:FMCSA_terms, PSP_terms:PSP_terms, signaturedata:signaturedata, disclosure_name:disclosure_name, disclosure_ssn:disclosure_ssn, disclosure_dlNumber:disclosure_dlNumber, driver_birthdate:driver_birthdate, driverlicense_state:driverlicense_state, disclosure_dlexp:disclosure_dlexp, start_time:start_time},
            dataType:"text"
        });
        //}
    }
    $('form').change(function(){
        autoSave();
    });
});