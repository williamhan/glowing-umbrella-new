<h2 id="contactInfo">Contact Info</h2>
<fieldset id="first">
    <div class="spacer-t20 spacer-b20"></div>
    <div class="section">

            {{ Form::hidden('start_time', date('Y-m-d H:i:s'), array_merge(['class' => 'gui-input'], ['id' => 'start_time'])) }}
            {{ Form::hidden('driver_guid', App\Driverapp::getGUID(), array_merge(['class' => 'gui-input'], ['id' => 'driver_guid'])) }}
            {{ Form::hidden('record_source', Request::get('rs'), array_merge(['class' => 'gui-input'], ['id' => 'record_source'])) }}
            {{ Form::hidden('how_heard', Request::get('id'), array_merge(['class' => 'gui-input'], ['id' => 'how_heard'])) }}
            {{ Form::hidden('app_version', URL::current(), array_merge(['class' => 'gui-input'], ['id' => 'app_version'])) }}
            {{ Form::hidden('var_track', Request::path(), array_merge(['class' => 'gui-input'], ['id' => 'var_track'])) }}
            {{ Form::customField('driver_email', 'driver_email', null, null, 'Email address', 'envelope', '12', 'email', 'gui-input') }}
    </div>
    <div class="align-center">
        <div class="frm-row">
            {{ Form::customField('driver_fname', 'driver_fname', null, null, 'First Name', 'user', '6', 'text', 'gui-input') }}
            {{ Form::customField('driver_lname', 'driver_lname', null, null, 'Last Name', 'user', '6', 'text', 'gui-input') }}
        </div>
    </div>
    <div class="align-center">
        <div class="frm-row">
            {{ Form::customField('work_phone', 'work_phone', null, null, 'Cell Phone', 'phone', '6', 'tel', 'gui-input') }}
            {{ Form::customField('home_phone', 'home_phone', null, null, 'Home Phone', 'phone-alt', '6', 'tel', 'gui-input') }}
        </div>
    </div>
    <div class="align-center">
        <div class="frm-row">
            {{ Form::customField('driver_address', 'driver_address', null, null, 'Street Address', 'road', '8', 'text', 'gui-input') }}
            {{ Form::customField('driver_zip', 'driver_zip', null, null, 'Zip', 'map-marker', '4', 'text', 'gui-input') }}
        </div>
    </div>
    <div class="align-center">
        <div class="frm-row">
            {{ Form::customField('driver_city', 'driver_city', null, null, 'City', 'home', '6', 'text', 'gui-input') }}
            <div class="section colm colm6">
                            <label class="field select prepend-icon">
                                <select id="driver_state" name="driver_state">
                                    <option value="">Choose State:</option>
                                        @foreach($stateslist as $state)
                                        <option value="{{ $state->state_abbr }}"> {{$state->state_name}}</option>
                                        @endforeach
                                </select>
                                <i class="arrow double"></i>
                            </label>
                        </div>
        </div>

    </div>
    <div class="align-center">
        <div class="frm-row">


            <div id="frapdiv" class="section colm colm12">
                <label class="field select prepend-icon">
                    <select id="frap_school" name="frap_school">
                        <option value="">Current School:</option>
                                        @foreach($schoollist as $school)
                                        <option value="{{ $school->schoolcode }}"> {{$school->schoolprettyname}}</option>
                                        @endforeach
                    </select>
                    <i class="arrow double"></i>
                </label>
            </div>
            {{ Form::hidden('frap_recruiter', '', array_merge(['class' => 'gui-input'], ['id' => 'frap_recruiter'])) }}
        </div>
    </div>
    {{-- @include('components.form.apptypebuttons') --}}
    <div class="section align-center">Do you have a CDL-A?
        <div id="cdl_holder" class="option-group field">
            {{ Form::radioButtons('cdl_holder', 'cdl_holder_y', 'Y', null, 'Yes', 'app_type_box', 'smartfm-ctrl') }}
            {{ Form::radioButtons('cdl_holder', 'cdl_holder_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
        @include('components.form.apptypebuttons')
    </div>
    @include('components.form.veteranbuttons')
    @include('components.form.drugtestbuttons')
    <div class="align-center">
        <div class="frm-row">
            {{ Form::dropdownList('accident_qty', 'accident_qty', ['0' => '0', '1' => '1', '2' => '2', '3' => '3', '4+' => '4+'], null, 'Number of accidents:', 'arrow double', '6') }}
            {{ Form::dropdownList('violation_qty', 'violation_qty', ['0' => '0', '1' => '1', '2' => '2', '3' => '3', '4+' => '4+'], null, 'Number of violations:', 'arrow double', '6') }}
            {{ Form::dropdownList('dui_qty', 'dui_qty', ['0' => '0', '1' => '1', '2' => '2', '3' => '3', '4+' => '4+'], null, 'Number of DUIs:', 'arrow double', '12') }}
        </div>
    </div>
    @include('components.form.agreetoterms')
    
</fieldset>

