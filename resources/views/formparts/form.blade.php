<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-0">

        <div id="formheader" class="form-header {{ auth()->user()->header_color }}">
            <h4><i class="fa fa-comments"></i>{{ auth()->user()->firstname }}'s New Orientation Sheet</h4>

        </div><!-- end .form-header section --> 
        <div class="form-body smart-steps stp-two">
            {{ Form::open(array('url' => '//drivecre.com/Scripts/newfilemakercopy/pdftestingfrap.php', 'name' => 'smart-form', 'id' => 'smart-form', 'enctype' => 'application/x-www-form-urlencoded')) }}
            {{-- {{ Form::open(array('url' => 'https://699d1fphyyjd.runscope.net', 'name' => 'smart-form', 'id' => 'smart-form', 'enctype' => 'application/x-www-form-urlencoded')) }} --}} 

            

            {{ Form::component('halfText', 'components.form.halftext', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon']) }}
            {{ Form::component('fullText', 'components.form.fulltext', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon']) }}
            {{ Form::component('fullEmail', 'components.form.fulltext', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon']) }}
            {{ Form::component('customField', 'components.form.customfield', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon', 'width', 'attributetype', 'secondclass' => null]) }}
            {{ Form::component('customReadOnly', 'components.form.customreadonly', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon', 'width', 'attributetype', 'secondclass' => null]) }}
            {{ Form::component('dropdownList', 'components.form.dropdownlist', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon', 'width']) }}
            {{ Form::component('radioButtons', 'components.form.radiobuttons', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'datashow', 'secondclass']) }}

                @include('formparts.pageone') 
                @include('formparts.condoffer')
                @include('formparts.pagetwo')
                @include('formparts.pagethree')
                @include('formparts.pagefour')
                @include('formparts.pagefive')
            {{ Form::close() }}    
       </div><!-- end .form-body section -->
    </div><!-- end .smart-forms section -->
</div><!-- end .smart-wrap section -->
<!--           </div> -->




