<h2 id="disclaimer"> Disclaimer </h2>
<fieldset  id="last">
    <div id="EVERIFY">
        <div class="section">
            <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>DISCLOSURE:</strong> By submitting this application I certify that I personally have completed this application and that all of the information is true and correct. I hereby authorize C.R. England, Inc. and its agents or contractors that receive this application to cause to be conducted, at any time, an investigation of my background for employment purposes, which may include, but is not limited to, any information relating to my character, general reputation, personal characteristics, mode of living, criminal history, driving record, credit history, past work experience, educational background, alcohol or drug test results, or failure to submit to an alcohol or drug test, or any other information about me which may reflect upon my potential for employment gathered from any individual, organization, entity, agency, or other source which may have knowledge concerning any such items of information. If hired or contracted this authorization for the above reports shall remain on file and shall serve as ongoing authorization for C.R. England, Inc to procure consumer reports at any time during my employment or contract period. I have completed this application of my own free will and hold C.R. England, Inc., its agents and contractors harmless for all liability for providing this application for my use. I consent to receiving text (SMS) messages from CR England and/or its affiliates regarding or relating to my application for employment. I have read and understand the  job description  and certify that I am able to meet the requirements of this position and may be contacted by, which may include, but is not limited to, email contact, phone contact, direct mail contact.
											<br>
											<br>
										<strong>EMPLOYMENT VERIFICATION:</strong> I hereby authorize C.R. England, Inc to do a complete background investigation, which includes contacting my past employers regarding my service, character and drug and alcohol test results as per Federal Motor Vehicle § 391.23 and 382.413. My past employers are released from any and all liability, which may result from furnishing such information. I authorize C.R. England to run a consumer report from USIS (DAC) services. These reports may include information concerning driving record, work experience, and motor vehicle reports.
                                    </span>
            </label>
        </div>
        <div class="align-center">
            <div class="frm-row">
                <div class="option-group field">
                    <div class="section colm colm12">
                        <label class="option block">
                            <input type="checkbox" name="Disclosure_and_Everify_terms" id="Disclosure_and_Everify_terms" value="Y">
                            <span class="checkbox"></span>
                            <span class="small-text fine-grey align-center">I have read and agree to the Disclosure and the Employment Verification release above.</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="spacer-b20"></div>
    </div>
    <div id="FMCSA">
        <div class="section">
            <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>DRIVER RIGHTS NOTIFICATION
                                        	<br>FMCSA NOTIFICATION OF DRIVER RIGHTS
                                        </strong> In compliance with 49 CFR Part 40 § 391.23 you have certain	rights regarding the performance history information that will be provided to prospective employers. I) You have the right to review	information provided by previous employers. II) You have the right to	have errors in the information corrected by the previous employer and	for that previous employer to re-send the corrected information to	prospective employers. III) You have the right to have a rebuttal	statement attached to the alleged erroneous information, if the	previous employer and the driver cannot agree on the accuracy of the	information. Drivers who have previous DOT regulated employment	history in the preceding three years and wish to review previous	employer-provided investigative information must submit a written	request to prospective employers. This may be done at any time,	including when applying, or as late as 30 days after being employed or	being notified of denial of employment. Prospective employers must	provide this information within five business days of receiving the	written request. If prospective employers have not yet received the requested information from the previous employer, then the five day	deadline will begin when the requested safety performance history	information is received. If you have not arranged to pick up or	receive the requested records within 30 days of prospective employers	making them available. Prospective employers may consider you to have	waived your request to review the record.
                                    </span>
            </label>
        </div>
        <div class="align-center">
            <div class="frm-row">
                <div class="option-group field">
                    <div class="section colm colm12">
                        <label class="option block">
                            <input type="checkbox" name="FMCSA_terms" id="FMCSA_terms" value="Y">
                            <span class="checkbox"></span>
                            <span class="small-text fine-grey align-center">I have read and agree to the FMCSA Notification of Driver Rights.</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PSP">
        <div class="section">
            <label class="field prepend-icon">
                                    <span class="input-hint small-text fine-grey align-center">
                                        <strong>MANDATORY USE FOR ALL ACCOUNT HOLDERS - IMPORTANT NOTICE - REGARDING BACKGROUND REPORTS FROM THE PSP ONLINE SERVICE</strong> 1. In connection with your application for employment with __C.R. England__ ("Prospective Employer"), Prospective Employer, its employees, agents or contractors may obtain one or more reports regarding your driving, and safety inspection history from the Federal Motor Carrier Safety Administration (FMCSA).
When the application for employment is submitted in person, if the Prospective Employer uses any information it obtains from FMCSA in a decision to not hire you or to make any other adverse employment decision regarding you, the Prospective Employer will provide you with a copy of the report upon which its decision was based and a written summary of your rights under the Fair Credit Reporting Act before taking any final adverse action. If any final adverse action is taken against you based upon your driving history or safety report, the Prospective Employer will notify you that the action has been taken and that the action was based in part or in whole on this report.
When the application for employment is submitted by mail, telephone, computer, or other similar means, if the Prospective Employer uses any information it obtains from FMCSA in a decision to not hire you or to make any other adverse employment decision regarding you, the Prospective Employer must provide you within three business days of taking adverse action oral, written or electronic notification: that adverse action has been taken based in whole or in part on information obtained from FMCSA; the name, address, and the toll free telephone number of FMCSA; that the FMCSA did not make the decision to take the adverse action and is unable to provide you the specific reasons why the adverse action was taken; and that you may, upon providing proper identification, request a free copy of the report and may dispute with the FMCSA the accuracy or completeness of any information or report. If you request a copy of a driver record from the Prospective Employer who procured the report, then, within 3 business days of receiving your request, together with proper identification, the Prospective Employer must send or provide to you a copy of your report and a summary of your rights under the Fair Credit Reporting Act.
The Prospective Employer cannot obtain background reports from FMCSA unless you consent in writing.
If you agree that the Prospective Employer may obtain such background reports, please read the following and sign below:
2. I authorize __C.R. England__ ("Prospective Employer") to access the FMCSA Pre-Employment Screening Program (PSP) system to seek information regarding my commercial driving safety record and information regarding my safety inspection history. I understand that I am consenting to the release of safety performance information including crash data from the previous five (5) years and inspection history from the previous three (3) years. I understand and acknowledge that this release of information may assist the Prospective Employer to make a determination regarding my suitability as an employee.
3. I further understand that neither the Prospective Employer nor the FMCSA contractor supplying the crash and safety information has the capability to correct any safety data that appears to be incorrect. I understand I may challenge the accuracy of the data by submitting a request to https://dataqs.fmcsa.dot.gov. If I am challenging crash or inspection information reported by a State, FMCSA cannot change or correct this data. I understand my request will be forwarded by the DataQs system to the appropriate State for adjudication.
4. Please note: Any crash or inspection in which you were involved will display on your PSP report. Since the PSP report does not report, or assign, or imply fault, it will include all Commercial Motor Vehicle (CMV) crashes where you were a driver or co-driver and where those crashes were reported to FMCSA, regardless of fault. Similarly, all inspections, with or without violations, appear on the PSP report. State citations associated with FMCSR violations that have been adjudicated by a court of law will also appear, and remain, on a PSP report.
I have read the above Notice Regarding Background Reports provided to me by Prospective Employer and I understand that if I sign this consent form, Prospective Employer may obtain a report of my crash and inspection history. I hereby authorize Prospective Employer and its employees, authorized agents, and/or affiliates to obtain the information authorized above.
                                    </span>
            </label>
        </div>
        <div class="align-center">
            <div class="frm-row">
                <div class="option-group field">
                    <div class="section colm colm12">
                        <label class="option block">
                            <input type="checkbox" name="PSP_terms" id="PSP_terms" class="gui-input" value="Y">
                            <span class="checkbox"></span>
                            <span class="small-text fine-grey align-center">I have read the above Notice Regarding Background Reports provided to me by Prospective Employer and I understand that if I sign this consent form, Prospective Employer may obtain a report of my crash and inspection history. I hereby authorize Prospective Employer and its employees, authorized agents, and/or affiliates to obtain the information authorized above.</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="frm-row">
        <div class="section colm colm12">
            <p class="small-text fine-grey align-center">By entering your name and identifying information below, you are signing this Disclosure, Consent, and Application electronically. You understand and agree that your electronic signature is the legal equivalent of your manual signature which may be upheld and used for identification and legal purposes. Your signature will be appended to the Past Employment Verification and the DOT D/A Disclosure and Authorization forms. You affirm that all the information contained in this application is correct and accurate to the best of your knowledge.<br></p>
        </div>

        <!-- 							<div class="spacer-b20"></div> -->

    </div>


    <div></div>Please Sign Below:
    <div id="signatureparent">
        <!-- 														<div class="field"> -->
        <div class="wrap-0" id="signature"></div>
        <input type="hidden" id="signaturedata" name="signature_data" class="gui-input">
    </div>

    <div class="frm-row">
        <div class="section align-center">
            <p id="signature_data_error" class="sig-error"></p>
        </div>


    </div>


    <div class="frm-row">
        <div class="section colm colm6">
            <label class="field prepend-icon">
                <input type="text" name="disclosure_name" id="disclosure_name" class="gui-input" placeholder="Legal Full Name">
                <span class="field-icon"><span class="glyphicon glyphicon-pencil"></span></span>

            </label>
        </div>

        <div class="section colm colm6">
            <label class="field prepend-icon">
                <input type="password" name="disclosure_ssn" id="disclosure_ssn" maxlength="9" class="gui-input" placeholder="Social Security Number">
                <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

            </label>
        </div>

    </div>

    <div class="frm-row">

        <div class="section colm colm6">
            <label class="field prepend-icon">
                <input type="password" name="disclosure_dlNumber" id="disclosure_dlNumber" class="gui-input" placeholder="Drivers License Number">
                <span class="field-icon"><span class="glyphicon glyphicon-certificate"></span></span>

            </label>
        </div>

        <div class="section colm colm6">
            <label class="field prepend-icon">
                <input type="text" name="driver_birthdate" id="driver_birthdate" class="gui-input" placeholder="Date Of Birth" readonly="readonly">
                <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

            </label>
        </div>

    </div>

    <div class="frm-row">

        <div class="section colm colm6">
            <label for="driverlicense_state" class="field select">
                <select id="driverlicense_state" name="driverlicense_state">
                    <option value="">Drivers License State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
                <i class="arrow double"></i>
            </label>
        </div>

        <div class="section colm colm6">
            <label class="field prepend-icon">
                <input type="text" name="disclosure_dlexp" id="disclosure_dlexp" class="gui-input" placeholder="Drivers License Expiration" readonly="readonly">
                <span class="field-icon"><span class="glyphicon glyphicon-calendar"></span></span>

            </label>
        </div>

    </div>
    <div id="result" class="result spacer-b10"></div>
</fieldset>

