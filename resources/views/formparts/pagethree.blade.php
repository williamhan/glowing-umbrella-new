<h2></h2>
<fieldset>
    <div class="section align-center">Have you ever failed or refused a pre-employment drug screen?
        <div id="drug_test" class="option-group field">
            {{ Form::radioButtons('drugtest', 'drugtest_y', 'Y', null, 'Yes', 'pre_emp_drugtest', 'smartfm-ctrl') }}
            {{ Form::radioButtons('drugtest', 'drugtest_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox" id="pre_emp_drugtest">
        <div class="frm-row">
            {{ Form::customReadOnly('drugtest_date', 'drugtest_date', null, null, 'When?', 'calendar', '6', 'text', 'gui-input') }}
            {{ Form::customField('drugtest_details', 'drugtest_details', null, null, 'Explain. (optional)', 'pencil', '6', 'text', 'gui-input') }}
        </div>
    </div>
    <div class="section align-center">Have you ever been charged with a felony?
        <div id="felony_charged" class="option-group field">
            {{ Form::radioButtons('felony', 'felony_y', 'Y', null, 'Yes', 'felony', 'smartfm-ctrl') }}
            {{ Form::radioButtons('felony', 'felony_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox" id="felony">
        <div id="clone-animate5">
            <div class="toclone" id="felonyclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('felony_date[0]', 'feldate', null, null, 'Date Charged', 'calendar', '12', 'text', 'gui-input felony_date') }}
                </div>
                <div class="frm-row">
                    <div class="section colm colm6">Were You Convicted?
                        <div class="option-group field">
                            {{ Form::radioButtons('felony_convicted[0]', 'felconvicted_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('felony_convicted[0]', 'felconvicted_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                    <div class="section colm colm6">Were You Incarcerated?
                        <div class="option-group field">
                            {{ Form::radioButtons('felony_incarcerated[0]', 'felincarcerated_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('felony_incarcerated[0]', 'felincarcerated_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                </div>
                <div class="frm-row">
                    {{ Form::customField('felony_city[0]', 'felcity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('felony_county[0]', 'felcounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('felony_state[0]', 'felstate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('felony_details[0]', 'feldetails', null, null, 'In your own words, describe the felony...', 'pencil', '12', 'text', 'gui-input') }}
                </div>
                @include('components.form.toclonebuttons')
            </div>
        </div> <!-- end #clone-group-fields5 -->
    </div>
    <div class="section align-center">Have you ever been charged with a misdemeanor?
        <div id="misdemeanor_charged" class="option-group field">
            {{ Form::radioButtons('misdemeanor', 'misdemeanor_y', 'Y', null, 'Yes', 'misdemeanor', 'smartfm-ctrl') }}
            {{ Form::radioButtons('misdemeanor', 'misdemeanor_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox" id="misdemeanor">
        <div id="clone-animate6">
            <div class="toclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('misdemeanor_date[0]', 'misdate', null, null, 'Date Charged', 'calendar', '12', 'text', 'gui-input misdemeanor_date') }}
                </div>
                <div class="section colm colm12">Were You Convicted?
                    <div class="option-group field">
                        {{ Form::radioButtons('misdemeanor_convicted[0]', 'misconvicted_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                        {{ Form::radioButtons('misdemeanor_convicted[0]', 'misconvicted_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                    </div>
                </div>
                <div class="frm-row">
                    {{ Form::customField('misdemeanor_city[0]', 'miscity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('misdemeanor_county[0]', 'miscounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('misdemeanor_state[0]', 'misstate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('misdemeanor_details[0]', 'misdetails', null, null, 'In your own words, describe the misdemeanor...', 'pencil', '12', 'text', 'gui-input') }}
                </div>
                @include('components.form.toclonebuttons')
            </div>
        </div> <!-- end #clone-group-fields6 -->
    </div>
    {{-- <div class="section align-center">Have you been to any other truck driving school in the last five years?
        <div id="school_grad" class="option-group field">
            {{ Form::radioButtons('schoolgrad', 'school_y', 'Y', null, 'Yes', 'other_school1', 'smartfm-ctrl') }}
            {{ Form::radioButtons('schoolgrad', 'school_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox" id="other_school1">
        <div class="frm-row">
            {{ Form::customField('school_grad_name', 'school_grad_name', null, null, 'School Name', 'book', '6', 'text', 'gui-input') }}
            {{ Form::customReadOnly('school_grad_date', 'school_grad_date', null, null, 'Graduation Date', 'calendar', '6', 'text', 'gui-input') }}
        </div>
        <div class="frm-row">
            {{ Form::customField('school_grad_city', 'school_grad_city', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
            {{ Form::dropdownList('school_grad_state', 'school_grad_state', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
            {{ Form::customField('school_grad_zip', 'school_grad_zip', null, null, 'Zip', 'map-marker', '4', 'text', 'gui-input') }}
        </div>
    </div> --}}
</fieldset>

