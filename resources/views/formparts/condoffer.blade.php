<h2>Offer</h2>
                        <fieldset>
                            <div id="conditional_offer" class="section align-center">
                                <h1>Congratulations!</h1>
                                <br>
                                <br>
                                <h3 class="align-center">
                                    We are pleased to make you a conditional offer of employment with C.R. England, Inc. conditioned upon successful completion of:
                                </h3>
                                <br>
                                <br>
                                <ul>
                                    <li>Drug Screen</li>
                                    <li>Criminal Background Check</li>
									<li>CDL School & Licensing</li>
                                    <li>CRE Road Test</li>
                                    <li>MVR & DAC Review</li>
                                    <li>Reference Checks</li>
                                    <li>Pre-Hire Interview</li>
                                    <li>New Hire Orientation</li>
                                    <li>Signed Driver Employment Contract</li>
                                </ul>

                            </div>
                        </fieldset>