<h2></h2>
<fieldset>
    <div class="section align-center">Have you ever been employed as a company driver <br>or independent contractor for C.R. England?
        <div id="companyDriver" class="option-group field">
            {{ Form::radioButtons('company_driver', 'company_driver_y', 'Y', null, 'Yes', 'company_driver_dates', 'smartfm-ctrl') }}
            {{ Form::radioButtons('company_driver', 'company_driver_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox formShowHide_reset" id="company_driver_dates">
        <div class="frm-row">Please Enter Your Start And Ending Dates Of Employment
            {{ Form::customReadOnly('company_driver_start', 'company_driver_start', null, null, 'Start Date', 'calendar', '6', 'text', 'gui-input') }}  
            {{ Form::customReadOnly('company_driver_end', 'company_driver_end', null, null, 'End Date', 'calendar', '6', 'text', 'gui-input') }}   
        </div>
    </div>
    <div class="section align-center">Have you had any moving violations in the last five years?
        <div id="movingViolations" class="option-group field">
            {{ Form::radioButtons('moving_violations', 'moving_violations_y', 'Y', null, 'Yes', 'moving_violations_info', 'smartfm-ctrl') }}
            {{ Form::radioButtons('moving_violations', 'moving_violations_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
    </div>
    <div class="section align-center hiddenbox formShowHide_reset" id="moving_violations_info">
        <div id="clone-animate">
            <div class="toclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('moving_violations_date[0]', 'vdate', null, null, 'Citation Date', 'calendar', '6', 'text', 'gui-input moving_violations_date') }}
                    {{ Form::dropdownList('violation_type[0]', 'vtype', ['Speeding Less Than 15 Over' => 'Speeding: Less than 15 MPH over the posted speed limit', 'Speeding Greater Than 15 Over' => 'Speeding: Greater than 15 MPH over the posted speed limit', 'Reckless' => 'Reckless Driving', 'Careless' => 'Careless Driving', 'Other' => 'Other. Describe Below'], null, 'Violation Type:', 'arrow double', '6') }}
                </div>
                <div class="frm-row align-center">
                    {{ Form::customField('violation_city[0]', 'vcity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('violation_county[0]', 'vcounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('violation_state[0]', 'vstate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row align-center">
                    {{ Form::customField('violation_details[0]', 'vdetails', null, null, 'In your own words, describe the citation...', 'pencil', '12', 'text', 'gui-input') }}
                </div>
                <div class="section align-center">Are all fines and fees associated with this citation paid?
                    <div class="option-group field">
                        {{ Form::radioButtons('fines_and_fees[0]', 'fines_and_fees_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                        {{ Form::radioButtons('fines_and_fees[0]', 'fines_and_fees_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                    </div>
                </div>
                @include('components.form.toclonebuttons')
            </div> <!-- end .toclone -->
        </div> <!-- end #clone-group-fields -->
    </div>
    <div class="section align-center">Have you had any traffic accidents in the last five years?
        <div id="carAccidents" class="option-group field">
            {{ Form::radioButtons('accidents', 'accidents_y', 'Y', null, 'Yes', 'accidents_info', 'smartfm-ctrl') }}
            {{ Form::radioButtons('accidents', 'accidents_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox formShowHide_reset" id="accidents_info">
        <div id="clone-animate2">
            <div class="toclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('accident_date[0]', 'accdate', null, null, 'Accident Date', 'calendar', '12', 'text', 'gui-input accidents_date') }}  
                </div>
                <div class="frm-row">
                    <div class="section align-center colm colm6">Were you issued a ticket?
                        <div class="option-group field">
                            {{ Form::radioButtons('accident_ticket[0]', 'accticy', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_ticket[0]', 'accticn', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                    <div class="section align-center colm colm6">Were there any injuries?
                        <div class="option-group field">
                            {{ Form::radioButtons('accident_injuries[0]', 'accidents_injuries_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_injuries[0]', 'accidents_injuries_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                </div>
                <div class="frm-row">
                    <div class="section align-center colm colm6">Were there any fatalities?
                        <div class="option-group field">
                            {{ Form::radioButtons('accident_fatalities[0]', 'accidents_fatalities_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_fatalities[0]', 'accidents_fatalities_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                    <div class="section align-center colm colm6">Was the accident preventable?
                        <div class="option-group field">
                            {{ Form::radioButtons('accident_preventable[0]', 'accidents_preventable_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_preventable[0]', 'accidents_preventable_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                </div>
                <div class="frm-row">
                    <div class="section align-center colm colm12">Who was listed as being at fault in the accident?
                        <div class="option-group field">
                            {{ Form::radioButtons('accident_fault[0]', 'accidents_fault_self', 'S', null, 'Me', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_fault[0]', 'accidents_fault_other', 'O', null, 'The other driver', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('accident_fault[0]', 'accidents_fault_both', 'B', null, 'Both Of Us', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                </div>
                <div class="frm-row">
                    {{ Form::customField('accident_city[0]', 'acccity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('accident_county[0]', 'acccounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('accident_state[0]', 'accstate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('accident_damage_amount[0]', 'accdamageamount', null, null, 'Total Damage Amount:', 'usd', '12', 'text', 'gui-input accdamageamount') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('accident_details[0]', 'accdetails', null, null, 'In your own words, describe the accident...', 'pencil', '12', 'text', 'gui-input') }}
                </div>
                @include('components.form.toclonebuttons')
            </div>
        </div><!-- end #clone-group-fields2 -->
    </div>
    <div class="section align-center">Have you ever been cited with DUI or DWI?
        <div id="duiDwi" class="option-group field">
            {{ Form::radioButtons('dui_dwi', 'dui_dwi_y', 'Y', null, 'Yes', 'dui', 'smartfm-ctrl') }}
            {{ Form::radioButtons('dui_dwi', 'dui_dwi_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox formShowHide_reset" id="dui">
        <div id="clone-animate3">
            <div class="toclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('dui_dwi_date[0]', 'duidate', null, null, 'Date Of DUI/DWI', 'calendar', '12', 'text', 'gui-input dui_dwi_date') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('dui_dwi_city[0]', 'duicity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('dui_dwi_county[0]', 'duicounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('dui_dwi_state[0]', 'duistate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('dui_dwi_details[0]', 'duidetails', null, null, 'In your own words, describe the DUI / DWI...', 'pencil', '12', 'text', 'gui-input') }}
                </div>
                @include('components.form.toclonebuttons')
            </div>
        </div> <!-- end #clone-group-fields3 -->
    </div>
    <div class="section align-center">Have you ever had your license suspended or revoked?
        <div id="suspendedRevoked" class="option-group field">
            {{ Form::radioButtons('suspendedrevoked', 'suspendedrevoked_y', 'Y', null, 'Yes', 'suspended_revoked', 'smartfm-ctrl') }}
            {{ Form::radioButtons('suspendedrevoked', 'suspendedrevoked_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
        </div>
        @include('components.form.spacer30')
    </div>
    <div class="section align-center hiddenbox" id="suspended_revoked">
        <div id="clone-animate4"> <!--  -->
            <div class="toclone">
                <div class="frm-row">
                    {{ Form::customReadOnly('suspendedrevoked_start_date[0]', 'suspendedstartdate', null, null, 'Start Date', 'calendar', '6', 'text', 'gui-input suspendedrevoked_start_date') }}  
                    {{ Form::customReadOnly('suspendedrevoked_end_date[0]', 'suspendedenddate', null, null, 'End Date', 'calendar', '6', 'text', 'gui-input suspendedrevoked_end_date') }}
                </div>
                <div class="frm-row">
                    {{ Form::customField('suspendedrevoked_city[0]', 'suspendedcity', null, null, 'City', 'globe', '4', 'text', 'gui-input') }}
                    {{ Form::customField('suspendedrevoked_county[0]', 'suspendedcounty', null, null, 'County', 'map-marker', '4', 'text', 'gui-input') }}
                    {{ Form::dropdownList('suspendedrevoked_state[0]', 'suspendedstate', App\States::pluck('state_name', 'state_abbr'), null, 'State:', 'arrow double', '4') }}
                </div>
                <div class="frm-row">
                    <div class="section align-center colm colm12">Has Your License Been Reinstated?
                        <div class="option-group field">
                            {{ Form::radioButtons('license_reinstated[0]', 'license_reinstated_y', 'Y', null, 'Yes', null, 'smartfm-ctrl') }}
                            {{ Form::radioButtons('license_reinstated[0]', 'license_reinstated_n', 'N', null, 'No', null, 'smartfm-ctrl') }}
                        </div>
                    </div>
                </div>
                @include('components.form.toclonebuttons')
            </div>
        </div> <!-- end #clone-group-fields4 -->
    </div>
</fieldset>

