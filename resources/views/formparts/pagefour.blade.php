<h2></h2>
<fieldset>
    <div class="section align-center"> Please provide the last three years of work history.
        <div class="section align-center hiddenbox" id="work_history">
            <div id="clone-animate7">
                <div class="toclone">
                    <!--
                                                        <div class="inner-wrap">
                                                            <div class="clone-inner">
                    -->
                    <div class="frm-row">
                        <div class="section colm colm6">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_company[0]" id="workhiscompany" class="gui-input" placeholder="Company Name">
                                <span class="field-icon"><i class="glyphicon glyphicon-briefcase"></i></span>
                            </label>
                        </div>
                        <div class="section colm colm6">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_title[0]" id="workhistitle" class="gui-input" placeholder="Job Title">
                                <span class="field-icon"><i class="glyphicon glyphicon-bookmark"></i></span>
                            </label>
                        </div>
                    </div>

                    <div class="frm-row">
                        <div class="section colm colm12">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_start[0]" id="workhisstart" class="gui-input work_history_start" placeholder="Start Date" readonly="readonly">
                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </label>
                        </div>
                    </div>

                    <div class="frm-row">
                        <div class="section colm colm6">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_end[0]" id="workhisend" class="gui-input work_history_end" placeholder="End Date" readonly="readonly">
                                <span class="field-icon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </label>
                        </div>

                        <!--
<div class="section colm colm6">Is This Your Current Employer?
                            <div class="option-group field">
                                <label class="option">
                                    <input type="radio" id="workhiscurrent_y" name="work_history_current[0]" class="smartfm-ctrl" value="Y" data-show-id="">
                                    <span class="radio"></span> Yes
                                </label>
                                <label class="option">
                                    <input type="radio" id="workhiscurrent_n" name="work_history_current[0]" class="smartfm-ctrl" value="N" data-show-id="noncurrent_work_history">
                                    <span class="radio"></span> No
                                </label>
                            </div>
                        </div>
-->
                        <!-- </div> -->

                        <!-- <div class="frm-row"> -->
                        <div class="section colm colm6">
                            <div class="option-group field">

                                <label class="option block">
                                    <input type="checkbox" name="work_history_current[0]" id="workhiscurrent_y" value="Y">
                                    <span class="checkbox"></span>
                                    <span class="small-text fine-grey align-center">Current Employer.</span>
                                </label>

                                <!--
<label class="option">
                                    <input type="radio" id="workhiscurrent_y" name="work_history_current[0]" class="smartfm-ctrl" value="Y" data-show-id="">
                                    <span class="radio"></span> Yes
                                </label>
                                <label class="option">
                                    <input type="radio" id="workhiscurrent_n" name="work_history_current[0]" class="smartfm-ctrl" value="N" data-show-id="noncurrent_work_history">
                                    <span class="radio"></span> No
                                </label>
-->
                            </div>
                        </div>
                    </div>




                    <div class="frm-row">
                        <div class="section colm colm12">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_address[0]" id="workhisaddress" class="gui-input" placeholder="Address">
                                <span class="field-icon"><i class="glyphicon glyphicon-road"></i></span>
                            </label>
                        </div>
                    </div>
                    <div class="frm-row">
                        <div class="section colm colm4">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_city[0]" id="workhiscity" class="gui-input" placeholder="City">
                                <span class="field-icon"><i class="glyphicon glyphicon-globe"></i></span>
                            </label>
                        </div>
                        <div class="section colm colm4">
                            <label class="field select prepend-icon">
                                <select id="workhisstate" name="work_history_state[0]">
                                    <option value="">State:</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                                <i class="arrow double"></i>
                            </label>
                        </div>
                        <div class="section colm colm4">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_zip[0]" id="workhiszip" class="gui-input" placeholder="Zip">
                                <span class="field-icon"><i class="glyphicon glyphicon-map-marker"></i></span>
                            </label>
                        </div>
                    </div>
                    <div class="frm-row">
                        <div class="section colm colm12">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_phone[0]" id="workhisphone" class="gui-input" placeholder="Phone Number">
                                <span class="field-icon"><i class="glyphicon glyphicon-phone-alt"></i></span>
                            </label>
                        </div>
                    </div>
                    <div class="frm-row hiddenbox" id="leave_reason">
                        <div class="section colm colm12">
                            <label class="field prepend-icon">
                                <input type="text" name="work_history_reason[0]" id="workhisreason" class="gui-input" placeholder="Reason For Leaving?">
                                <span class="field-icon"><i class="glyphicon glyphicon-plane"></i></span>
                            </label>
                        </div>
                    </div>
                    <div class="frm-row">

                        <div class="section colm colm12">May We Contact This Employer?
                            <div class="option-group field">
                                <label class="option">
                                    <input type="radio" id="workhiscontact_y" name="work_history_contact[0]" class="smartfm-ctrl" value="Y">
                                    <span class="radio"></span> Yes
                                </label>
                                <label class="option">
                                    <input type="radio" id="workhiscontact_n" name="work_history_contact[0]" class="smartfm-ctrl" value="N">
                                    <span class="radio"></span> No
                                </label>
                            </div>
                        </div>
                    </div>
                    <!--
                                                            </div>
                                                        </div>
                    -->
                    <a href="#" class="clone button btn-primary"><i class="glyphicon glyphicon-plus"></i></a>
                    <a href="#" class="delete button"><i class="glyphicon glyphicon-minus"></i></a>
                </div>
            </div> <!-- end #clone-group-fields7 -->
        </div>
    </div>
</fieldset>

