<div id="contact-modal-form-sms" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>SMS</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->
        <div id="loadersms" style="display: none">
            <img src="images/homerloading.gif" alt="Wait" />
        </div>

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="php/smartprocess.php" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">

                                    <label class="option">
                                        <a id="WelcomePacketSmsButton" class="button btn-black-third btn-primary btn-rounded">Send Welcome Packet SMS</a>
                                        <b class="tooltip"><em> "Thank you for speaking with me today and once again welcome to the team! Please check your email for your Welcome Documentation and travel information. - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                    {{--</div>--}}
                                    {{--<div class="section colm colm6">--}}
                                    <label class="option">
                                        <a id="LeftMessageSmsButton" class="button btn-black-third btn-primary btn-rounded">Left Message SMS</a>
                                        <b class="tooltip"><em> "Good afternoon. I am trying to reach you about the application you submitted for the truck driving job with C.R. England. Please call me back as soon as you can. Thanks! - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>


                                </div>


                                <div class="section">
                                    <label class="option">
                                        <a id="TravelSmsButton" class="button btn-black-third btn-primary btn-rounded">Send Travel SMS</a>
                                        <b class="tooltip"><em> "Thank you for speaking with me today. Please check your email for your Travel Information. Remember you need to call me once you have your bus ticket printed to confirm your start date. - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                    <label class="option">
                                        <a id="LastChanceSmsButton" class="button btn-black-third btn-primary btn-rounded">Last Chance SMS</a>
                                        <b class="tooltip"><em> "Are you still planning on starting Monday?  If you are no longer interested please let me know so that I can cancel your reservations so we don't get charged. Your response is necessary to keep your application active. Please call immediately. Thanks! - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                    <label class="option">
                                        <a id="ConfirmationSmsButton" class="button btn-black-third btn-primary btn-rounded">Confirmation SMS</a>
                                        <b class="tooltip"><em> "Good afternoon. I just wanted to follow up and make sure that you are good to go for school this upcoming Monday. Will you please give me a call and confirm that you will be attending. Thanks! - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                </div>


                                <div class="section">
                                    <label class="option">
                                        <a id="WAReleaseSmsButton" class="button btn-black-third btn-primary btn-rounded">WA Release SMS</a>
                                        <b class="tooltip"><em> "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                    <label class="option">
                                        <a id="VAReleaseSmsButton" class="button btn-black-third btn-primary btn-rounded">VA Release SMS</a>
                                        <b class="tooltip"><em> "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                    <label class="option">
                                        <a id="NHReleaseSmsButton" class="button btn-black-third btn-primary btn-rounded">NH Release SMS</a>
                                        <b class="tooltip"><em> "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - {{ auth()->user()->firstname }} - {{ auth()->user()->deskphone }}" </em></b>
                                    </label>
                                </div>

                                <div class="section">
                                    <form method="post" id="DIYSms">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <textarea class="gui-textarea" id="Sms" name="Sms" placeholder="Type Your Own SMS Message Here"></textarea>
                                                        <span class="field-icon"><i class="fa fa-comments"></i></span>
                                                        <span class="input-hint"> <strong>Hint:</strong> Please enter between 10 - 160 characters.</span>
                                                    </label>
                                                </div>
                                                <div class="section colm colm12">
                                                    <a id="DIYSmsButton" class="button btn-black-third btn-primary btn-rounded">Send</a>
                                                </div><!-- end section -->

                                            </div><!-- end .frm-row section -->
                                        </div><!-- end .form-body section -->
                                    </form>
                                </div>



                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->

<div id="contact-modal-form-free-text-sms" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>DIY SMS</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="php/smartprocess.php" id="smart-form">
                            <div class="colm colm12">



                                <div class="section">
                                    <form method="post" id="allnames">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <div class="frm-row">
                                                <div class="section colm colm12">
                                                    <label class="field prepend-icon">
                                                        <textarea class="gui-textarea" id="comment" name="comment" placeholder="Type Your SMS Message Here"></textarea>
                                                        <span class="field-icon"><i class="fa fa-comments"></i></span>
                                                        <span class="input-hint"> <strong>Hint:</strong> Please enter between 10 - 160 characters.</span>
                                                    </label>
                                                </div>
                                                <div class="section colm colm12">
                                                    <button type="submit" class="button btn-black-third btn-primary btn-rounded">Send</button>
                                                </div><!-- end section -->

                                            </div><!-- end .frm-row section -->
                                        </div><!-- end .form-body section -->
                                    </form>
                                </div>

                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->