

<div id="ghmapmodal" class="smartforms-modal" role="alert" onload="mapload()">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>HireRight</h3>

            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->

        <div class="smartforms-modal-body">

            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="php/smartprocess.php" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">
                                    <div style="height: 400px; width: 400px;">{!! Mapper::render() !!}</div>

                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>


                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->