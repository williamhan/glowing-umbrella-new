<div id="contact-modal-form-schools" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3><i class="fa fa-tachometer"></i> School Info</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->
        <div id="loader" style="display: none">
            <img src="build/images/homerloading.gif" alt="Wait" />
        </div>

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="php/smartprocess.php" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">
                                    <a href="#" data-smart-modal="#contact-modal-form-schools-SLC" class="button btn-primary btn-rounded">SLC</a>
                                    <a href="#" data-smart-modal="#contact-modal-form-schools-IND" class="button btn-black-third btn-primary btn-rounded">IND</a>
                                    <a href="#" data-smart-modal="#contact-modal-form-schools-DAL" class="button btn-black-third btn-primary btn-rounded">DAL</a>
                                    <a href="#" data-smart-modal="#contact-modal-form-schools-ML" class="button btn-black-third btn-primary btn-rounded">ML</a>
                                </div>

                                <div class="spacer spacer-t20 spacer-b20"></div>

                                <div class="section">
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">AAA</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">160 Driving Academy</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">CDL College</a>
                                </div>

                                <div class="section">
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Affordable</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Alliance</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">American</a>
                                </div>

                                <div class="section">
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Drive Train</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">NTDS</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">USTDTS</a>
                                </div>

                                <div class="section">
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Pine Bluff</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">VATA</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">SWTDTS</a>
                                </div>

                                <div class="section">
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Trucking Advantage</a>
                                    <a href="javascript:placeHolderFunc();" class="button btn-black-third btn-primary btn-rounded">Western Pacific</a>
                                </div>


                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->