<div id="contact-modal-form-greyhound" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>Greyhound</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->
        <div id="loader" style="display: none">
            <img src="build/images/homerloading.gif" alt="Wait" />
        </div>

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="{{ url('/ghfinder') }}" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">
                                    <div class="map-container">
                                        <div style="height: 400px; width: 400px;">{!! Mapper::render() !!}</div>
                                    </div>
                                    <a id="OrderGhButton" class="button btn-black-third btn-primary btn-rounded">Order Greyhound</a>
                                    <a id="FindGhButton" data-smart-modal="#ghmapmodal" class="button btn-black-third btn-primary btn-rounded">Find Nearest Greyhound</a>
                                    <input class="button btn-black-third btn-primary btn-rounded" type="submit" value="Update" />
                                </div>

                            </div>





                            <div class="smartforms-modal-footer">
                                <a id="OrderGhButton" class="button btn-black-third btn-primary btn-rounded">Order Greyhound</a>
                                <a id="FindGhButton" data-smart-modal="#ghmapmodal" class="button btn-black-third btn-primary btn-rounded">Find Nearest Greyhound</a>
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->