<div id="contact-modal-form-veterans" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>Veterans Emails</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->
        <div id="loader" style="display: none">
            <img src="build/images/homerloading.gif" alt="Wait" />
        </div>

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="php/smartprocess.php" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">
                                    <a id="DDTwoFourteenButton" class="button btn-black-third btn-primary btn-rounded">Request DD-214</a>
                                    <a id="ComLetterButton" class="button btn-black-third btn-primary btn-rounded">Commanders Letter</a>
                                </div>

                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->