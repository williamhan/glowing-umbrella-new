<div id="contact-modal-form-schools-SLC" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3><i class="fa fa-tachometer"></i> School Info - SLC </h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="/" id="quote">
                            <div class="form-body">

                                <div class="frm-row">

                                    <div class="colm colm4">

                                        <div class="section">
                                            <label for="fullname" class="field-label">School Info:</label>
                                            <label class="field">
                                                Replace This With School Info For SLC
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="company" class="field-label">School Schedule:</label>
                                            <label class="field">
                                                Replace This With Schedule
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="email" class="field-label">Fees:</label>
                                            <label class="field">
                                                Replace This With Fees Info
                                            </label>
                                        </div><!-- end section -->

                                    </div><!-- end .colm section -->

                                    <div class="colm colm4">

                                        <div class="section">
                                            <label for="projectname" class="field-label">Lodging Info:</label>
                                            <label class="field">
                                                Replace This With Lodging Info
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="website" class="field-label">Amenities:</label>
                                            <label class="field">
                                                Replace This With Amenities Info
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="likewebsite" class="field-label">Documents Needed:</label>
                                            <label class="field">
                                                Replace This With Docs Info
                                            </label>
                                        </div><!-- end section -->

                                    </div><!-- end .colm section -->

                                    <div class="colm colm4">

                                        <div class="section">
                                            <label for="budget" class="field-label">Shuttle Info</label>
                                            <label class="field">
                                                Replace This With Shuttle Info
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="timeframe" class="field-label">Room Capacity:</label>
                                            <label class="field">
                                                Replace This With Capacity Info
                                            </label>
                                        </div><!-- end section -->

                                        <div class="section">
                                            <label for="services" class="field-label">Length of Enrollment:</label>
                                            <label class="field">
                                                Replace This With Enrollment Info
                                            </label>
                                        </div><!-- end section -->
                                    </div><!-- end .colm section -->

                                </div><!-- end .frm-row section -->

                                <div class="frm-row">
                                    <div class="colm colm8">

                                        <div class="section">
                                            <label for="details" class="field-label">Other Notes:</label>
                                            <label class="field">
                                    <textarea class="gui-textarea" id="details" name="details">
                                    Replace This With Other Notes
                                    </textarea>
                                                <span class="input-hint"> <strong>Hint:</strong> A hint goes here.</span>
                                            </label>
                                        </div><!-- end section -->

                                    </div><!-- end .colm section -->
                                    <div class="colm colm4">

                                        <div class="section">
                                            <label for="details" class="field-label">Undecided Column:</label>
                                            <label class="field">
                                                Replace This With School Info
                                            </label>
                                        </div><!-- end section -->

                                    </div><!-- end .colm section -->
                                </div><!-- end .frm-row section -->

                            </div><!-- end .form-body section -->
                            <div class="form-footer">
                                <!--                 	<button type="submit" class="button btn-primary">Receive Quote</button> -->
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->