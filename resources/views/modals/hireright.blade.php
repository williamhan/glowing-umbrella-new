<div id="contact-modal-form-hireright" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>HireRight</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->
        <div id="loaderhireright" style="display: none">
            <img src="build/images/homerloading.gif" alt="Wait" />
        </div>

        

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="/hrtestreview" id="mvr-form">

                            {{ csrf_field() }}
                            <input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
                            <input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
                            <input type="hidden" name="agent_id" id="agent_id" value="{{ auth()->user()->sysuserid }}">
                            <input type="hidden" name="manager" id="manager" value="{{ auth()->user()->parentuseridname }}">
                            @if (!empty($app_id_guid))
                                <input type="hidden" name="app_id_guid" id="app_id_guid" value="{{ $app_id_guid }}">
                            @else
                                <input type="hidden" name="app_id_guid" id="app_id_guid" value=" ">
                            @endif

                            @if (!empty($cre_appid))
                                <input type="hidden" name="app_id" id="app_id" value="{{ $cre_appid }}">
                            @else
                                <input type="hidden" name="app_id" id="app_id" value=" ">
                            @endif

                            @if (!empty($permit_spec_guid))
                                <input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value="{{ $permit_spec_guid }}">
                            @else
                                <input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value=" ">
                            @endif

                            @if (!empty($permit_spec_name))
                                <input type="hidden" name="permit_spec_name" id="permit_spec_name" value="{{ $permit_spec_name }}">
                            @else
                                <input type="hidden" name="permit_spec_name" id="permit_spec_name" value=" ">
                            @endif

                            @if (!empty($cre_applicantidname))
                                <input type="hidden" name="full_name" id="full_name" value="{{ $cre_applicantidname }}">
                            @else
                                <input type="hidden" name="full_name" id="full_name" value=" ">
                            @endif

                            @if (!empty($firstname))
                                <input type="hidden" name="firstname" id="firstname" value="{{ $firstname }}">
                            @else
                                <input type="hidden" name="firstname" id="firstname" value=" ">
                            @endif

                            @if (!empty($middlename))
                                <input type="hidden" name="middlename" id="middlename" value="{{ $middlename }}">
                            @else
                                <input type="hidden" name="middlename" id="middlename" value=" ">
                            @endif

                            @if (!empty($lastname))
                                <input type="hidden" name="lastname" id="lastname" value="{{ $lastname }}">
                            @else
                                <input type="hidden" name="lastname" id="lastname" value=" ">
                            @endif

                            @if (!empty($cre_bookingdate))
                                <input type="hidden" name="booking_date" id="booking_date" value="{{ date('m-d-Y', strtotime(substr($cre_bookingdate, 0, 10))) }}">
                            @else
                                <input type="hidden" name="booking_date" id="booking_date" value=" ">
                            @endif

                            @if (!empty($cre_bookingschool))
                                <input type="hidden" name="school" id="school" value="{{ $cre_bookingschool }}">
                            @else
                                <input type="hidden" name="school" id="school" value=" ">
                            @endif

                            @if (!empty($cre_apptype))
                                <input type="hidden" name="app_type" id="app_type" value="{{ $cre_apptype }}">
                            @else
                                <input type="hidden" name="app_type" id="app_type" value=" ">
                            @endif

                            @if (!empty($cre_recruiter))
                                <input type="hidden" name="recruiter" id="recruiter" value="{{ $cre_recruiter }}">
                            @else
                                <input type="hidden" name="recruiter" id="recruiter" value=" ">
                            @endif

                            @if (!empty($cre_track))
                                <input type="hidden" name="track" id="track" value="{{ $cre_track }}">
                            @else
                                <input type="hidden" name="track" id="track" value=" ">
                            @endif

                            @if (!empty($email))
                                <input type="hidden" name="email" id="email" value="{{ $email }}">
                            @else
                                <input type="hidden" name="email" id="email" value=" ">
                            @endif

                            @if (!empty($mobilephone))
                                <input type="hidden" name="mobilephone" id="mobilephone" value="{{ $mobilephone }}">
                            @else
                                <input type="hidden" name="mobilephone" id="mobilephone" value=" ">
                            @endif

                            @if (!empty($cre_bookingdate))
                                <input type="hidden" name="booking_status" id="booking_status" value="Booked">
                            @else
                                <input type="hidden" name="booking_status" id="booking_status" value=" ">
                            @endif

                            @if (!empty($address1_line1))
                                <input type="hidden" name="address1_line1" id="address1_line1" value="{{ $address1_line1 }}">
                            @else
                                <input type="hidden" name="address1_line1" id="address1_line1" value=" ">
                            @endif

                            @if (!empty($address1_city))
                                <input type="hidden" name="address1_city" id="address1_city" value="{{ $address1_city }}">
                            @else
                                <input type="hidden" name="address1_city" id="address1_city" value=" ">
                            @endif

                            @if (!empty($address1_stateorprovince))
                                <input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value="{{ $address1_stateorprovince }}">
                            @else
                                <input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value=" ">
                            @endif

                            @if (!empty($address1_postalcode))
                                <input type="hidden" name="address1_postalcode" id="address1_postalcode" value="{{ $address1_postalcode }}">
                            @else
                                <input type="hidden" name="address1_postalcode" id="address1_postalcode" value=" ">
                            @endif

                            @if (!empty($address1_latitude))
                                <input type="hidden" name="address1_latitude" id="address1_latitude" value="{{ $address1_latitude }}">
                            @else
                                <input type="hidden" name="address1_latitude" id="address1_latitude" value=" ">
                            @endif

                            @if (!empty($address1_longitude))
                                <input type="hidden" name="address1_longitude" id="address1_longitude" value="{{ $address1_longitude }}">
                            @else
                                <input type="hidden" name="address1_longitude" id="address1_longitude" value=" ">
                            @endif

                            @if (!empty($gh_conf))
                                <input type="hidden" name="gh_conf" id="gh_conf" value="{{ $gh_conf }}">
                            @else
                                <input type="hidden" name="gh_conf" id="gh_conf" value=" ">
                            @endif

                            @if (!empty($dob))
                                <input type="hidden" name="DOB" id="DOB" value="{{ $dob }}">
                            @else
                                <input type="hidden" name="DOB" id="DOB" value=" ">
                            @endif

                            @if (!empty($cre_ssn))
                                <input type="hidden" name="ssn" id="ssn" value="{{ $cre_ssn }}">
                            @else
                                <input type="hidden" name="ssn" id="ssn" value=" ">
                            @endif

                            @if (!empty($dl_number))
                                <input type="hidden" name="dl_number" id="dl_number" value="{{ $dl_number }}">
                            @else
                                <input type="hidden" name="dl_number" id="dl_number" value=" ">
                            @endif

                            @if (!empty($dl_state))
                                <input type="hidden" name="dl_state" id="dl_state" value="{{ $dl_state }}">
                            @else
                                <input type="hidden" name="dl_state" id="dl_state" value=" ">
                            @endif

                            @if (!empty($dl_expire))
                                <input type="hidden" name="dl_expire" id="dl_expire" value="{{ $dl_expire }}">
                            @else
                                <input type="hidden" name="dl_expire" id="dl_expire" value=" ">
                            @endif

                            @if (!empty($departure_station))
                                <input type="hidden" name="departure_station" id="departure_station" value="{{ $departure_station }}">
                            @else
                                <input type="hidden" name="departure_station" id="departure_station" value=" ">
                            @endif



                            <div class="colm colm12">


                                <div class="section"> RUN:
                                    <a id="RunMvrButton" class="button btn-black-third btn-primary btn-rounded">MVR</a>
{{--                                     <a id="RunWidescreenButton" class="button btn-black-third btn-primary btn-rounded">20-20</a>
                                    <a id="RunDacButton" class="button btn-black-third btn-primary btn-rounded">DAC</a>
                                    <a id="RunCdlisButton" class="button btn-black-third btn-primary btn-rounded">CDLIS</a>
                                    <a id="RunEhfPackageButton" class="button btn-black-third btn-primary btn-rounded">EHF</a> --}}
                                    <a id="RunEhfPackageButton" class="button btn-black-third btn-primary btn-rounded">Everything Else</a>
                                </div>

                                <div class="spacer spacer-t20 spacer-b20"></div>

                                <div class="section"> REVIEW:
                                    <a id="ReviewMvr" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">MVR</a>
{{--                                     <a id="ReviewWidescreen" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">20-20</a>
                                    <a id="ReviewDac" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">DAC</a>
                                    <a id="ReviewCdlis" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">CDLIS</a>
                                    <a id="ReviewEhfPackage" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">EHF</a> --}}
                                    <a id="ReviewEhfPackage" style="display:none" href="#" target="_blank" class="button btn-black-third btn-primary btn-rounded">Everything Else</a>
                                </div>

                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->