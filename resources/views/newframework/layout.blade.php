<!DOCTYPE html>
<html>
<head>
    <title>Nos Schools Info</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" data-smart-modal="#contact-modal-form-hireright">HireRight</a>
            <a class="navbar-brand" href="{{ URL::to('docsinfo') }}">GreyHound</a>
            <a class="navbar-brand" href="{{ URL::to('feesinfo') }}">Email</a>
            <a class="navbar-brand" href="{{ URL::to('housinginfo') }}">SMS</a>
            <a class="navbar-brand" href="{{ URL::to('scheduleinfo') }}">School Info</a>
            <a class="navbar-brand" href="{{ URL::to('scheduleinfo') }}">School Info Tables</a>
            <a class="navbar-brand" href="{{ URL::to('transportationinfo') }}">Name Changes</a>
            <a class="navbar-brand" href="{{ URL::to('transportationinfo') }}">Name Change</a>
            <a class="navbar-brand" href="{{ URL::to('transportationinfo') }}">Logout</a>
        </div>

    </nav>

    @yield('body')

</div>
</body>
</html>