@extends('layouts.base')


{{--@section('head')--}}
{{--@parent--}}
{{--@include('scripts.cssfiles')--}}
{{--@include('scripts.jsfiles')--}}
{{--@include('scripts.topbuttons')--}}
{{--@include('scripts.colorbuttons')--}}

{{--@stop--}}

@section('navbar')
@if (auth()->user()->role_id == "2") {{--general users--}}
@include('headers.users')
@elseif(auth()->user()->role_id == "3") {{--managers--}}
@include('headers.managers')
@elseif(auth()->user()->role_id == "4") {{--admin--}}
@include('headers.admin')
@elseif(auth()->user()->role_id == "5") {{--beta users--}}
@include('headers.beta')
@elseif(auth()->user()->role_id == "6") {{--jamie users--}}
@include('headers.jamie')
@endif
@stop

@section('body')



    {{--@include('features.mainbuttons')--}}

    @include('features.smartwrapnoecho')
    @include('modals.schools')
    @include('modals.emails')
    @include('modals.sms')
    @include('modals.veterans')
    {{--@include('modals.greyhound')--}}
    @include('modals.hireright')
    @include('modals.namechange')
    @include('modals.schoolmodals.slc')
    @include('modals.schoolmodals.ind')
    @include('modals.schoolmodals.dal')
    @include('modals.schoolmodals.ml')
    @include('modals.ghmodals.ghmap')
    @include('modals.success')



@stop
