<!DOCTYPE html>
<html>
<head>
    <title>Nos Schools Info</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('schoolinfo') }}">Schools</a>
            <a class="navbar-brand" href="{{ URL::to('docsinfo') }}">Docs</a>
            <a class="navbar-brand" href="{{ URL::to('feesinfo') }}">Fees</a>
            <a class="navbar-brand" href="{{ URL::to('housinginfo') }}">Housing</a>
            <a class="navbar-brand" href="{{ URL::to('scheduleinfo') }}">Schedules</a>
            <a class="navbar-brand" href="{{ URL::to('transportationinfo') }}">Transportation</a>
            <a class="navbar-brand" href="{{ URL::to('userinfo') }}">Users</a>
            <a class="navbar-brand" href="{{ URL::to('dashboard') }}">Back To Nos</a>
        </div>

    </nav>

    @yield('body')

</div>
</body>
</html>