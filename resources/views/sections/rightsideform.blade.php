<div class="col-sm-5" id="join-us-form">
    <div id="header-right">
        @include('formparts.title')
        @include('formparts.form')
    </div>
</div>
