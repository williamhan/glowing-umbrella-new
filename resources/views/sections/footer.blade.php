<footer class="copyright-section">
    <div class="container">
        <div class="col-md-12 ">
            <P>© Copyright {{ date("Y") }} <a href="http://www.crengland.com"> C.R. England </a> | <a href="http://drivecre.com/privacy-policy/"> Privacy Policy </a></P>
        </div>
    </div>
</footer>
