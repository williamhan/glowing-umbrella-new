<div class="col-sm-7" id="header-left">
    <div class="logo"><img src="img/company3.png"></div>
    <div class="verticle-line">
        <h2>Congratulations on getting your CDL and<br> starting your career as a Professional Driver!</h2>
    </div>
    <h5>Why choose C.R. England?</h5>
    <h5>We take extreme pride in our drivers at C.R. England. They are the lifeblood of our business. Career opportunities, training, lucrative pay and stability are just a few of the staples we offer when you drive with us. Not to mention the average driver in the industry makes $40,260/year according to recent government statistics. C.R. England's average pay for a driver with one or more years' experience is <b>$53,100+/year</b>. That's over <b>$13,100 more</b> than the reported industry average.<br><br>

    
        {{-- At C.R. England, You Choose Your Lane, You Choose Your Position, and You Get ALL the Benefits!<br><br> --}}
        <h5>Great Pay and Benefits</h5>
        
        <ul>
            <li>Paid Time Off</li>
            <li>401k Match</li>
            <li>Medical, Dental, & Vision Coverage</li>
            <li>Life Insurance</li>
            <li>Legal Subscription</li>
        </ul>
        <h5>Safety and Training programs to ensure you feel confident behind the wheel</h5>

        <h5>Department of Labor and VA approved Apprenticeship Program "Operation Success"</h5>

        <h5>Tools To Help You Succeed:</h5>
        <ul>
            <li>CRE Toolbox mobile app</li>
            <li>CareerTrak</li>
            <li>Weigh my Truck</li>
            <li>Transflo Mobile+</li>
            <li>And more!</li>
        </ul>
        
    </h5>
    <h2>
        Let's put your CDL to good use &mdash; Apply Now.
    </h2>
</div>

