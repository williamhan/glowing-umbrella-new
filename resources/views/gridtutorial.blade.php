@extends('layouts.gridbase')

@section('container')
    <script type='text/javascript'>
        {{-- Your JavaScript code goes here! --}}
        function afterSubmitEvent(response, postdata)
        {
            //Parses the JSON response that comes from the server.
            result = JSON.parse(response.responseText);
            //result.success is a boolean value, if true the process continues,
            //if false an error message appear and all other processing is stopped,
            //result.message is ignored if result.success is true.
            return [result.success, result.message];
        }

    </script>



    Hello World!

    {{
       GridRender::setGridId("SchoolGrid")
         ->enablefilterToolbar()
         ->setGridOption('url',URL::to('/gridtest'))
         ->setGridOption('editurl',URL::to('/school-grid-crud'))
         ->setGridOption('rowNum', 10)
         ->setGridOption('rownumbers', true)
        // ->setGridOption('width', 750)
         ->setGridOption('height', 225)
         ->setGridOption('rowList',array(10,20,30))
         ->setGridOption('caption','School Info')
         ->setGridOption('viewrecords',true)
         ->setNavigatorOptions('navigator', array('add' => true, 'edit' => true, 'del' => true, 'view' => true, 'refresh' => false))
         ->setNavigatorOptions('add', array('closeAfterAdd' => true))
         ->setNavigatorEvent('add', 'afterSubmit', 'afterSubmitEvent')
         ->setNavigatorOptions('edit', array('closeAfterEdit' => true))
         ->setNavigatorEvent('edit', 'afterSubmit', 'afterSubmitEvent')
         ->setNavigatorEvent('del', 'afterSubmit', 'afterSubmitEvent')
         ->addColumn(array('index' => 'id', 'hidden' => true, 'editable' => true))
         ->addColumn(array('label' => 'SchoolId', 'index' => 'sch_id', 'editable' => true))
         ->addColumn(array('label' => 'Name', 'index' =>'sch_name', 'editable' => true))
         ->addColumn(array('label' => 'Type','index' => 'sch_type', 'align' => 'center', 'width' => 90, 'editable' => true, 'editoptions' => array('value' => 'P:Premier;3P:3P;T2:Track2;EC:EC'), 'edittype' => 'select', 'formatter' => 'select', 'editrules' => array('required' => true)))
         ->addColumn(array('label' => 'Address','index' => 'sch_address', 'editable' => true))
         ->addColumn(array('label' => 'City','index' => 'sch_city', 'editable' => true))
         ->addColumn(array('label' => 'State','index' => 'sch_state', 'editable' => true))
         ->addColumn(array('label' => 'Zip', 'index' => 'sch_zip', 'editable' => true))
         ->addColumn(array('label' => 'Phone', 'index' => 'sch_phone', 'editable' => true))
         ->addColumn(array('label' => 'Fax','index' => 'sch_fax', 'editable' => true))
         ->addColumn(array('label' => 'Duration Min','index' => 'sch_duration_min', 'editable' => true))
         ->addColumn(array('label' => 'Duration Max','index' => 'sch_duration_max', 'editable' => true))
         ->addColumn(array('label' => 'transportation_id','index' => 'sch_transportation_id', 'editable' => true))
         ->addColumn(array('label' => 'schedule_id','index' => 'sch_schedule_id', 'editable' => true))
         ->addColumn(array('label' => 'docs_id','index' => 'sch_docs_id', 'editable' => true))
         ->addColumn(array('label' => 'fees_id','index' => 'sch_fees_id', 'editable' => true))
         ->addColumn(array('label' => 'housing_id','index' => 'sch_housing_id', 'editable' => true))
         ->addGroupHeader(array('startColumnName' => 'name', 'numberOfColumns' => 2, 'titleText' => 'Book Information'))
         ->renderGrid()
       }}
@stop