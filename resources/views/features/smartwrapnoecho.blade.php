<div class="smart-wrap">
<div class="smart-forms smart-container wrap-90">
	<div id="formheader" class="form-header {{ auth()->user()->header_color }}">
		<h4><i class="fa fa-comments"></i>{{ auth()->user()->firstname }}'s New Orientation Sheet</h4>
	</div>
	<!-- end .form-header section --> 
	<div class="frm-row">
		<div class="section colm colm3">
			<form action="{{ url('/dashboardapplist') }}" method="POST" id="apprefresh">
				{{ csrf_field() }}
				<div class="form-body {{ auth()->user()->theme_color }}">
					<div class="frm-row">
						<!-- <div class="section colm colm4">
							</div> -->
						<div class="section colm colm12">
							<div class="smart-widget sm-left sml-80">
								<label class="field">
									<!-- <input type="text" name="appid" id="appid" class="gui-input" placeholder="Apps"> -->
									<input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
									<input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
									<input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
									@if (!empty(auth()->user()->deskphone))
									<input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
									@else
									<input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
									@endif
									@if (!empty(auth()->user()->user_ref_id))
									<input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
									@else
									<input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
									@endif
								</label> 
								<input style="width: 100%; font-size: 15px;" class="button" type="submit" value="Refresh My Apps" />
								{{--<a href="javascript:runAppId();" class="button"> Update </a>--}}
							</div>
							<!-- end .smart-widget section -->
						</div>
					</div>
				</div>
			</form>

			<div  style="height: 850px; overflow-y: scroll;">

			@if (!empty($appdata))
					
					@foreach($appdata as $app)

					{{--  $now = Carbon::now();	  --}}

						  @if (($app['statecode'] == 0))

							  {{--  @if (date('Y-m-d H:i:s', strtotime($app['createdon'])) >= $todaydate )  --}}
							  
							  
							 
							  






						  <form action="{{ url('/dashboard') }}" method="POST" id="sub123">
							{{ csrf_field() }}
							<div class="form-body-list {{ auth()->user()->theme_color }}">
								<div class="frm-row">
									<div class="section colm colm12">
										<div class="smart-widget sm-right">
											<label for="application_id">{{ $app['cre_applicantidname'] }}: </label>
											<input type="hidden" name="appid" id="appid" value="{{ $app['cre_appid'] }}">
											<input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
											<input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
											<input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
												@if (!empty(auth()->user()->deskphone))
													<input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
												@else
													<input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
												@endif
												@if (!empty(auth()->user()->user_ref_id))
													<input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
												@else
													<input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
												@endif
											{{--  <input class="button btn-blue btn-rounded" type="submit" value="2193661">  --}}
											<a href="javascript:void(0);" onclick="$(this).closest('form').submit()">{{ $app['cre_appid'] }}</a>
										</div>
									</div>
								</div>
							</div>
						</form>

						{{--  @else  --}}

						{{--  @endif  --}}





							{{--  <h5>{{ $app['cre_appid'] }}: {{ $app['cre_applicantidname'] }}: {{ $app['statecode'] }}: {{ $app['statecode_displayname'] }}: {{ $app['statuscode'] }}: {{ $app['statuscode_displayname'] }}</h5>  --}}

						@else

							{{--  <h5>No Active App Records 1</h5>  --}}
                                    
						@endif  
					
                
                	@endforeach

				@else

					<h5>No Active App Records</h5>
                                    
				@endif

					</div>

			
			
			
			
			
			
			
        </div>
			<div class="section colm colm6 vlft vrt">
				<form action="{{ url('/dashboard') }}" method="POST" id="sub1">
					{{ csrf_field() }}
					<div class="form-body {{ auth()->user()->theme_color }}">
						<div class="frm-row">
							<!-- <div class="section colm colm4">
								</div> -->
							<div class="section colm colm12 pad-r100 pad-l100">
								<!-- <div class="section colm colm3">
									</div> -->
								<!-- <div class="section colm colm6 pad-r30 pad-l30"> -->
								<div class="smart-widget sm-right smr-80">
									<label class="field">
									<input type="text" name="appid" id="appid" class="gui-input" placeholder="App ID">
									<input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
									<input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
									<input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
									@if (!empty(auth()->user()->deskphone))
									<input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
									@else
									<input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
									@endif
									@if (!empty(auth()->user()->user_ref_id))
									<input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
									@else
									<input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
									@endif
									</label>
									<input class="button" type="submit" value="Update" />
									{{--<a href="javascript:runAppId();" class="button"> Update </a>--}}
								</div>
								<!-- </div> -->
								<!-- <div class="section colm colm3">
									</div> -->
								<!-- end .smart-widget section -->
							</div>
							<!-- end section -->
							<!-- <div class="section colm colm4">
								</div> -->
							<!--                    </div> -->
						</div>
					</div>
				</form>
				<form method="post" action="#" id="mainform">
					{{ csrf_field() }}
					<div class="form-body {{ auth()->user()->theme_color }}">
						<div class="frm-row">
							<div class="colm colm12">
								<div class="colm colm12">
									<div class="section">
										<div class="price-box selected-box">
											@if (!empty($maindata['cre_bookingdate']))
											<div class="ribbon-large">
												<div class="ribbon-inner">BOOKED</div>
											</div>
											@else
											<div class="ribbon-large">
												<div class="ribbon-inner"></div>
											</div>
											@endif
											<div class="tagline">
												<span>Name:</span>
											</div>
											@if (!empty($maindata['fulllegalname']))
											<h5>{{ $maindata['fulllegalname'] }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>App ID:</span>
											</div>
											@if (!empty($maindata['cre_appid']))
											<h5>{{ $maindata['cre_appid'] }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>Start Date:</span>
											</div>
											@if (!empty($maindata['cre_bookingdate']))
											<h5>{{ date('m-d-Y', strtotime(substr($maindata['cre_bookingdate'], 0, 10))) }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>Employment Type:</span>
											</div>
											@if (!empty($maindata['cre_track']))
											@if ($maindata['cre_track'] == "Non-Native")
											@if($maindata['cre_apptype'] == "SC - School Student")
											<h5>Track 2</h5>
											@endif
											@elseif($maindata['cre_track'] == "Non-Native EC")
											<h5>E.C.</h5>
											@else
											@if (!empty($maindata['cre_apptype']))
											<h5>{{ $maindata['cre_apptype'] }}</h5>
											@else
											<h5></h5>
											@endif
											@endif
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>School Location:</span>
											</div>
											@if (!empty($maindata['cre_bookingschool']))
											<h5>{{ $maindata['cre_bookingschool'] }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>Recruiter:</span>
											</div>
											@if (!empty($maindata['cre_recruiter']))
											<h5>{{ $maindata['cre_recruiter'] }}</h5>
											@else
											<h5></h5>
											@endif
											@if (!empty($maindata['permit_spec_name']))
											<div class="tagline">
												<span>Permit Specialist:</span>
											</div>
											<h5>{{ $maindata['permit_spec_name'] }}</h5>
											@else
											@endif
											<div class="tagline">
												<span>Departure Zip Code:</span>
											</div>
											@if (!empty($maindata['address1_postalcode']))
											<h5>{{ $maindata['address1_postalcode'] }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>Email:</span>
											</div>
											@if (!empty($maindata['email']))
											<h5>{{ $maindata['email'] }}</h5>
											@else
											<h5></h5>
											@endif
											<div class="tagline">
												<span>Phone:</span>
											</div>
											@if (!empty($maindata['mobilephone']))
											<h5>{{ $maindata['mobilephone'] }}</h5>
											@else
											<h5></h5>
											@endif
											<input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
											<input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
											<input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
											@if (!empty(auth()->user()->deskphone))
											<input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
											@else
											<input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
											@endif
											@if (!empty(auth()->user()->user_ref_id))
											<input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
											@else
											<input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
											@endif
											<input type="hidden" name="agent_id" id="agent_id" value="{{ auth()->user()->sysuserid }}">
											<input type="hidden" name="manager" id="manager" value="{{ auth()->user()->parentuseridname }}">
											@if (!empty($maindata['app_id_guid']))
											<input type="hidden" name="app_id_guid" id="app_id_guid" value="{{ $maindata['app_id_guid'] }}">
											@else
											<input type="hidden" name="app_id_guid" id="app_id_guid" value=" ">
											@endif
											@if (!empty($maindata['cre_appid']))
											<input type="hidden" name="app_id" id="app_id" value="{{ $maindata['cre_appid'] }}">
											@else
											<input type="hidden" name="app_id" id="app_id" value=" ">
											@endif
											@if (!empty($maindata['permit_spec_guid']))
											<input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value="{{ $maindata['permit_spec_guid'] }}">
											@else
											<input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value=" ">
											@endif
											@if (!empty($maindata['permit_spec_name']))
											<input type="hidden" name="permit_spec_name" id="permit_spec_name" value="{{ $maindata['permit_spec_name'] }}">
											@else
											<input type="hidden" name="permit_spec_name" id="permit_spec_name" value=" ">
											@endif
											@if (!empty($maindata['cre_applicantidname']))
											<input type="hidden" name="full_name" id="full_name" value="{{ $maindata['cre_applicantidname'] }}">
											@else
											<input type="hidden" name="full_name" id="full_name" value=" ">
											@endif
											@if (!empty($maindata['firstname']))
											<input type="hidden" name="firstname" id="firstname" value="{{ $maindata['firstname'] }}">
											@else
											<input type="hidden" name="firstname" id="firstname" value=" ">
											@endif
											@if (!empty($maindata['middlename']))
											<input type="hidden" name="middlename" id="middlename" value="{{ $maindata['middlename'] }}">
											@else
											<input type="hidden" name="middlename" id="middlename" value=" ">
											@endif
											@if (!empty($maindata['suffix']))
											<input type="hidden" name="suffix" id="suffix" value="{{ $maindata['suffix'] }}">
											@else
											<input type="hidden" name="suffix" id="suffix" value=" ">
											@endif
											@if (!empty($maindata['lastname']))
											<input type="hidden" name="lastname" id="lastname" value="{{ $maindata['lastname'] }}">
											@else
											<input type="hidden" name="lastname" id="lastname" value=" ">
											@endif
											@if (!empty($maindata['cre_bookingdate']))
											<input type="hidden" name="booking_date" id="booking_date" value="{{ date('m-d-Y', strtotime(substr($maindata['cre_bookingdate'], 0, 10))) }}">
											@else
											<input type="hidden" name="booking_date" id="booking_date" value=" ">
											@endif
											@if (!empty($maindata['cre_bookingschool']))
											<input type="hidden" name="school" id="school" value="{{ $maindata['cre_bookingschool'] }}">
											@else
											<input type="hidden" name="school" id="school" value=" ">
											@endif
											@if (!empty($maindata['cre_apptype']))
											<input type="hidden" name="app_type" id="app_type" value="{{ $maindata['cre_apptype'] }}">
											@else
											<input type="hidden" name="app_type" id="app_type" value=" ">
											@endif
											@if (!empty($maindata['cre_recruiter']))
											<input type="hidden" name="recruiter" id="recruiter" value="{{ $maindata['cre_recruiter'] }}">
											@else
											<input type="hidden" name="recruiter" id="recruiter" value=" ">
											@endif
											@if (!empty($maindata['cre_track']))
											<input type="hidden" name="track" id="track" value="{{ $maindata['cre_track'] }}">
											@else
											<input type="hidden" name="track" id="track" value=" ">
											@endif
											@if (!empty($maindata['email']))
											<input type="hidden" name="email" id="email" value="{{ $maindata['email'] }}">
											@else
											<input type="hidden" name="email" id="email" value=" ">
											@endif
											@if (!empty($maindata['mobilephone']))
											<input type="hidden" name="mobilephone" id="mobilephone" value="{{ $maindata['mobilephone'] }}">
											@else
											<input type="hidden" name="mobilephone" id="mobilephone" value=" ">
											@endif
											@if (!empty($maindata['cre_bookingdate']))
											<input type="hidden" name="booking_status" id="booking_status" value="Booked">
											@else
											<input type="hidden" name="booking_status" id="booking_status" value=" ">
											@endif
											@if (!empty($maindata['address1_line1']))
											<input type="hidden" name="address1_line1" id="address1_line1" value="{{ $maindata['address1_line1'] }}">
											@else
											<input type="hidden" name="address1_line1" id="address1_line1" value=" ">
											@endif
											@if (!empty($maindata['address1_city']))
											<input type="hidden" name="address1_city" id="address1_city" value="{{ $maindata['address1_city'] }}">
											@else
											<input type="hidden" name="address1_city" id="address1_city" value=" ">
											@endif
											@if (!empty($maindata['address1_stateorprovince']))
											<input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value="{{ $maindata['address1_stateorprovince'] }}">
											@else
											<input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value=" ">
											@endif
											@if (!empty($maindata['address1_postalcode']))
											<input type="hidden" name="address1_postalcode" id="address1_postalcode" value="{{ $maindata['address1_postalcode'] }}">
											@else
											<input type="hidden" name="address1_postalcode" id="address1_postalcode" value=" ">
											@endif
											@if (!empty($maindata['address1_latitude']))
											<input type="hidden" name="address1_latitude" id="address1_latitude" value="{{ $maindata['address1_latitude'] }}">
											@else
											<input type="hidden" name="address1_latitude" id="address1_latitude" value=" ">
											@endif
											@if (!empty($maindata['address1_longitude']))
											<input type="hidden" name="address1_longitude" id="address1_longitude" value="{{ $maindata['address1_longitude'] }}">
											@else
											<input type="hidden" name="address1_longitude" id="address1_longitude" value=" ">
											@endif
											@if (!empty($maindata['gh_conf']))
											<input type="hidden" name="gh_conf" id="gh_conf" value="{{ $maindata['gh_conf'] }}">
											@else
											<input type="hidden" name="gh_conf" id="gh_conf" value=" ">
											@endif
											@if (!empty($maindata['dob']))
											<input type="hidden" name="DOB" id="DOB" value="{{ date('Y-m-d', strtotime(substr($maindata['dob'], 0, 10))) }}">
											@else
											<input type="hidden" name="DOB" id="DOB" value=" ">
											@endif
											@if (!empty($maindata['cre_ssn']))
											<input type="hidden" name="ssn" id="ssn" value="{{ $maindata['cre_ssn'] }}">
											@else
											<input type="hidden" name="ssn" id="ssn" value=" ">
											@endif
											@if (!empty($maindata['dl_number']))
											<input type="hidden" name="dl_number" id="dl_number" value="{{ preg_replace("/[^A-Za-z0-9]/", "", $maindata['dl_number']) }}">
											@else
											<input type="hidden" name="dl_number" id="dl_number" value=" ">
											@endif
											@if (!empty($maindata['dl_state']))
											<input type="hidden" name="dl_state" id="dl_state" value="{{ $maindata['dl_state'] }}">
											@else
											<input type="hidden" name="dl_state" id="dl_state" value=" ">
											@endif
											@if (!empty($maindata['dl_expire']))
											<input type="hidden" name="dl_expire" id="dl_expire" value="{{ date('Y-m-d', strtotime(substr($maindata['dl_expire'], 0, 10))) }}">
											@else
											<input type="hidden" name="dl_expire" id="dl_expire" value=" ">
											@endif
											@if (!empty($maindata['departure_station']))
											<input type="hidden" name="departure_station" id="departure_station" value="{{ $maindata['departure_station'] }}">
											@else
											<input type="hidden" name="departure_station" id="departure_station" value=" ">
											@endif
										</div>
										<!-- end .colm-box section -->
									</div>
								</div>
								<!-- end .section section -->    
							</div>
							<!-- end section -->
						</div>
						<!-- end frm-row section -->
					</div>
					<!-- end .form-body section -->
					<div class="spacer-t30">
						<div class="tagline"><span> Change NOS Theme: </span></div>
						<!-- .tagline -->
					</div>
					<div class="form-footer">
						<a id="defaultbutton" class="button btn-primary btn-rounded">Default</a>
						<a id="redbutton" class="button btn-red btn-rounded">Red</a>
						<a id="bluebutton" class="button btn-blue btn-rounded">Blue</a>
						<a id="yellowbutton" class="button btn-yellow btn-rounded">Yellow</a>
						<a id="purplebutton" class="button btn-purple btn-rounded">Purple</a>
						<a id="greenbutton" class="button btn-green btn-rounded">Green</a>
					</div>
					<!-- end .form-footer section -->
				</form>
			</div>
			<div class="section colm colm3">
				<div class="spacer-t30">
					<div class="tagline"><span> Sent / Received SMS: </span></div>
					<br>
					<br>
					<div  style="height: 850px; overflow-y: scroll;">
					<smssection>
				@if (!empty($allsmsdata))
					
					@foreach($allsmsdata as $smsg)

						@if ($smsg->direction == "outbound-api")


						<div class="from-me">
							<p> {{ $smsg->body }} </p>
						</div>
						<div class="smsclear"></div>

						@elseif ($smsg->direction == "inbound")


						<div class="from-them">
							<p> {{ $smsg->body }} </p>
						</div>
						<div class="smsclear"></div>


						@endif
					
                
                	@endforeach

				@else

					<h5>No SMS Records</h5>
                                    
				@endif

					</smssection>
				</div>

				</div>

			</div>





			<div class="smartforms-modal-footer">
			</div><!-- end .form-footer section -->
		</form>

				</div>
			</div>
		</div>
	</div>
	<!-- end .smart-forms section -->
</div>
<!-- end .smart-wrap section -->
<div id="contact-modal-form-greyhound" class="smartforms-modal" role="alert">
	<div class="smartforms-modal-container">
		<div class="smartforms-modal-header">
			<h3>Greyhound</h3>
			<a href="#" class="smartforms-modal-close">&times;</a>
		</div>
		<!-- .smartforms-modal-header -->
		<div class="smartforms-modal-body">
			<div class="smart-wrap">
				<div class="smart-forms smart-container wrap-full">
					<div class="form-body">
						<form method="post" action="{{ url('/ghfinder') }}" id="smart-form">
							<div class="colm colm12">
								<div class="section">
									<div class="map-container">
										<div style="height: 400px; width: 500px;">{!! Mapper::render() !!}</div>
									</div>
								</div>
							</div>
							<div class="smartforms-modal-footer">
							</div>
							<!-- end .form-footer section -->
						</form>
					</div>
					<!-- end .form-body section -->
				</div>
				<!-- end .smart-forms section -->
			</div>
			<!-- end .smart-wrap section -->
		</div>
		<!-- .smartforms-modal-body -->
	</div>
	<!-- .smartforms-modal-container -->
</div>
<!-- .smartforms-modal -->