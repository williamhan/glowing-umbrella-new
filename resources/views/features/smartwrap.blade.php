<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-0">

        <div id="formheader" class="form-header header-primary">
            <h4><i class="fa fa-comments"></i>New Orientation Sheet</h4>
        </div><!-- end .form-header section -->

        <form method="post" action="#" id="sub1">
            <div class="form-body theme-primary">
                <div class="frm-row">

                    <!-- 					<div class="colm colm6 pad-r30"> -->

                    <div class="section colm colm4">
                    </div>


                    <div class="section colm colm4">
                        <div class="smart-widget sm-right smr-120">
                            <label class="field">
                                <input type="text" name="appid" id="appid" class="gui-input" placeholder="Enter App ID">
                            </label>
                            <a href="javascript:runAppId();" class="button"> Update </a>
                        </div><!-- end .smart-widget section -->
                    </div><!-- end section -->

                    <div class="section colm colm4">
                    </div>

                    <!-- 					</div> -->
                </div>
            </div>
        </form>

        <form method="post" action="#" id="mainform">
            <div class="form-body theme-primary">

                <div class="frm-row">
                    <div class="colm colm12">
                        <div class="section">
                            <div class="price-box selected-box">
                                <div class="ribbon-large"> <div class="ribbon-inner">{{ $booking_status }}</div> </div>



                                <div class="tagline">
                                    <span>Name:</span>

                                </div>
                                <h5>{{ $full_name }}</h5>





                                <div class="tagline">
                                    <span>App ID:</span>

                                </div>
                                <h5>{{ htmlspecialchars($app_id) }}</h5>
                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->

                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->
                                <div class="tagline">
                                    <span>Start Date:</span>

                                </div>
                                <h5>{{ $newformat }}</h5>





                                <div class="tagline">
                                    <span>Employment Type:</span>

                                </div>
                                <h5>{{ htmlspecialchars($employmenttype) }}</h5>

                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->
                                <div class="tagline">
                                    <span>School Location:</span>

                                </div>
                                <h5>{{ $school }}</h5>
                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->

                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->
                                <div class="tagline">
                                    <span>Recruiter:</span>

                                </div>
                                <h5>{{ htmlspecialchars($recruiter) }}</h5>
                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->

                                <div class="tagline">
                                    <span>Departure Zip Code:</span>

                                </div>
                                <h5>{{ htmlspecialchars($zip) }}</h5>

                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->
                                <div class="tagline">
                                    <span>Email:</span>

                                </div>
                                <h5>{{ htmlspecialchars($email) }}</h5>
                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->
                                <div class="tagline">
                                    <span>Phone:</span>

                                </div>
                                <h5>{{ htmlspecialchars($mobilephone) }}</h5>
                                <!--                                     <div class="spacer spacer-t15 spacer-b15"></div> -->


                                <input type="hidden" name="app_id" id="app_id" value="{{ htmlspecialchars($app_id) }}">
                                <input type="hidden" name="full_name" id="full_name" value="{{ htmlspecialchars($full_name) }}">
                                <input type="hidden" name="firstname" id="full_name" value="{{ htmlspecialchars($firstname) }}">
                                <input type="hidden" name="lastname" id="full_name" value="{{ htmlspecialchars($lastname) }}">
                                <input type="hidden" name="booking_date" id="booking_date" value="{{ htmlspecialchars($booking_date) }}">
                                <input type="hidden" name="school" id="school" value="{{ htmlspecialchars($school) }}">
                                <input type="hidden" name="app_type" id="app_type" value="{{ htmlspecialchars($app_type) }}">
                                <input type="hidden" name="recruiter" id="recruiter" value="{{ htmlspecialchars($recruiter) }}">
                                <input type="hidden" name="track" id="track" value="{{ htmlspecialchars($track) }}">
                                <input type="hidden" name="email" id="email" value="{{ htmlspecialchars($email) }}">
                                <input type="hidden" name="mobilephone" id="mobilephone" value="{{ htmlspecialchars($mobilephone) }}">
                                <input type="hidden" name="phoneone" id="phoneone" value="{{ htmlspecialchars($phoneone) }}">
                                <input type="hidden" name="phonetwo" id="phonetwo" value="{{ htmlspecialchars($phonetwo) }}">
                                <input type="hidden" name="phonethree" id="phonethree" value="{{ htmlspecialchars($phonethree) }}">
                                <input type="hidden" name="booking_status" id="booking_status" value="{{ htmlspecialchars($booking_status) }}">
                                <input type="hidden" name="address1_line1" id="address1_line1" value="{{ htmlspecialchars($address1_line1) }}">
                                <input type="hidden" name="address1_city" id="address1_city" value="{{ htmlspecialchars($address1_city) }}">
                                <input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value="{{ htmlspecialchars($address1_stateorprovince) }}">
                                <input type="hidden" name="address1_postalcode" id="address1_postalcode" value="{{ htmlspecialchars($address1_postalcode) }}">
                                <input type="hidden" name="address1_latitude" id="address1_latitude" value="{{ $address1_latitude }}">
                                <input type="hidden" name="address1_longitude" id="address1_longitude" value="{{ $address1_longitude }}">



                            </div><!-- end .colm-box section -->
                        </div><!-- end .section section -->


                    </div><!-- end section -->




                </div><!-- end frm-row section -->


            </div><!-- end .form-body section -->
            <div class="form-footer">
                <a id="defaultbutton" href="#" class="button btn-primary btn-rounded">Default</a>
                <a id="redbutton" href="#" class="button btn-red btn-rounded">Red</a>
                <a id="bluebutton" href="#" class="button btn-blue btn-rounded">Blue</a>
                <a id="yellowbutton" href="#" class="button btn-yellow btn-rounded">Yellow</a>
                <a id="purplebutton" href="#" class="button btn-purple btn-rounded">Purple</a>
                <a id="greenbutton" href="#" class="button btn-green btn-rounded">Green</a>
            </div><!-- end .form-footer section -->
        </form>

    </div><!-- end .smart-forms section -->
</div><!-- end .smart-wrap section -->