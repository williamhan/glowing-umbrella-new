
<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-1">
        <div class="form-header header-primary">
        @if ($manager == "All")
            <h4><i class="fa fa-comments"></i>{{ $manager }} Name Changes</h4>
        @else
            <h4><i class="fa fa-comments"></i>{{ $manager }}'s Name Changes</h4>
        @endif
        </div>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="frm-row">
                    <div class="section colm colm2">
                        App Id
                    </div>
                    <div class="section colm colm3">
                        To
                    </div>
                    <div class="section colm colm3">
                        From
                    </div>
                    <div class="section colm colm3">
                        Approve?
                    </div>
                </div>
                @foreach($names[$manager] as $allnames)
                <form method="post" id="{{ $manager }}names">
                    {{ csrf_field() }}
                    <div class="form-body">
                        <div class="frm-row">
                            <div class="section colm colm2">
                                @if (!empty($allnames->appid))
                                    <a href="{{ $allnames->link }}" target="_blank">{{ $allnames->appid }}</a>
                                    <input type="hidden" name="app_id" id="app_id" value="{{ $allnames->appid }}">
                                    <input type="hidden" name="agent_email" id="agent_email" value="{{ $allnames->agent_email }}">
                                    <input type="hidden" name="is_permit_spec" id="is_permit_spec" value="{{ $allnames->is_permit_spec }}">
                                @else
                                    Your personal names
                                @endif
                            </div>
                            <div class="section colm colm3">
                                @if (!empty($allnames->to))
                                    {{ $allnames->to }}
                                    <input type="hidden" name="to" id="to" value="{{ $allnames->to }}">
                                    <input type="hidden" name="to_id" id="to_id" value="{{ $allnames->to_id }}">
                                @else
                                    To
                                @endif
                            </div>
                            <div class="section colm colm3">
                                @if (!empty($allnames->from))
                                    {{ $allnames->from }}
                                    <input type="hidden" name="from" id="from" value="{{ $allnames->from }}">
                                @else
                                    From
                                @endif
                            </div>
                            <div class="section colm colm1">
                                <button type="submit" class="button btn-black-third btn-primary btn-rounded">Yes</button>
                            </div><!-- end section -->
                            <div class="section colm colm1">
                            </div><!-- end section -->
                            <div class="section colm colm1">
                                <button id="NCNo{{ $manager }}" class="button btn-black-third btn-primary btn-rounded">No</button>
                            </div><!-- end section -->
                        </div><!-- end .frm-row section -->
                    </div><!-- end .form-body section -->
                </form>
                @endforeach
            </div>
        </div>
    </div><!-- end .smart-forms section -->
</div><!-- end .smart-wrap section -->

<div></div><!-- end section -->