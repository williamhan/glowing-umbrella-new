<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-0">

        <div id="formheader" class="form-header {{ auth()->user()->header_color }}">
            <h4><i class="fa fa-comments"></i>{{ auth()->user()->firstname }}'s New Orientation Sheet</h4>

        </div><!-- end .form-header section -->

        <form action="{{ url('/dashboard') }}" method="POST" id="sub1">

            {{ csrf_field() }}
            <div class="form-body {{ auth()->user()->theme_color }}">
                <div class="frm-row">
                    <div class="section colm colm7">
                    </div>
                    <div class="section colm colm4">
                        <div class="smart-widget sm-right smr-120">
                            <label class="field">
                                <input type="text" name="appid" id="appid" class="gui-input" placeholder="Enter App ID">
                                <input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
                                <input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
                                <input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
                                @if (!empty(auth()->user()->deskphone))
                                    <input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
                                @else
                                    <input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
                                @endif
                                @if (!empty(auth()->user()->user_ref_id))
                                    <input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
                                @else
                                    <input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
                                @endif
                            </label>

                            <input class="button" type="submit" value="Update" />
                            {{--<a href="javascript:runAppId();" class="button"> Update </a>--}}
                        </div><!-- end .smart-widget section -->
                    </div><!-- end section -->

                    <div class="section colm colm1">
                    </div>

                    <!--                    </div> -->
                </div>
            </div>
        </form>

         <div class="frm-row">
                    
            <div class="colm colm6">

                Test

            </div>

            <div class="colm colm6">

            

                <form method="post" action="#" id="mainform">
                    {{ csrf_field() }}
                    <div class="form-body {{ auth()->user()->theme_color }}">

                        <div class="frm-row">
                            <div class="colm colm12">
                                <div class="section">
                                    <div class="price-box selected-box">
                                        @if (!empty($cre_bookingdate))
                                        <div class="ribbon-large"> <div class="ribbon-inner">BOOKED</div> </div>
                                        @else
                                        <div class="ribbon-large"> <div class="ribbon-inner"></div> </div>
                                        @endif

                                        <div class="tagline">
                                            <span>Name:</span>

                                        </div>
                                            @if (!empty($cre_applicantidname))
                                                <h5>{{ $cre_applicantidname }}</h5>
                                            @else
                                                <h5></h5>
                                            @endif

                                        <div class="tagline">
                                            <span>App ID:</span>

                                        </div>
                                            @if (!empty($cre_appid))
                                                <h5>{{ $cre_appid }}</h5>
                                            @else
                                                <h5></h5>
                                            @endif

                                        <div class="tagline">
                                            <span>Start Date:</span>

                                        </div>
                                        @if (!empty($cre_bookingdate))
                                            <h5>{{ date('m-d-Y', strtotime(substr($cre_bookingdate, 0, 10))) }}</h5>
                                        @else
                                            <h5></h5>
                                        @endif

                                        <div class="tagline">
                                            <span>Employment Type:</span>

                                        </div>
                                            @if (!empty($cre_track))
                                                @if ($cre_track == "Non-Native")
                                                    @if($cre_apptype == "SC - School Student")
                                                        <h5>Track 2</h5>
                                                    @endif
                                                @elseif($cre_track == "Non-Native EC")
                                                    <h5>E.C.</h5>
                                                @else
                                                    @if (!empty($cre_apptype))
                                                        <h5>{{ $cre_apptype }}</h5>
                                                    @else
                                                        <h5></h5>
                                                    @endif
                                                @endif
                                            @else
                                                <h5></h5>
                                            @endif

                                        <div class="tagline">
                                            <span>School Location:</span>

                                        </div>
                                            @if (!empty($cre_bookingschool))
                                                <h5>{{ $cre_bookingschool }}</h5>
                                            @else
                                                <h5></h5>
                                        @endif

                                        <div class="tagline">
                                            <span>Recruiter:</span>

                                        </div>
                                            @if (!empty($cre_recruiter))
                                        <h5>{{ $cre_recruiter }}</h5>
                                            @else
                                                <h5></h5>
                                            @endif


                                            @if (!empty($permit_spec_name))
                                                <div class="tagline">
                                                    <span>Permit Specialist:</span>

                                                </div>
                                                <h5>{{ $permit_spec_name }}</h5>
                                            @else

                                            @endif

                                        <div class="tagline">
                                            <span>Departure Zip Code:</span>

                                        </div>
                                                @if (!empty($address1_postalcode))
                                        <h5>{{ $address1_postalcode }}</h5>
                                            @else
                                                <h5></h5>
                                            @endif

                                        <div class="tagline">
                                            <span>Email:</span>

                                        </div>
                                                    @if (!empty($email))
                                        <h5>{{ $email }}</h5>
                                            @else
                                                <h5></h5>
                                        @endif
                                        <div class="tagline">
                                            <span>Phone:</span>

                                        </div>
                                            @if (!empty($mobilephone))
                                        <h5>{{ $mobilephone }}</h5>
                                            @else
                                                <h5></h5>
                                        @endif
        										<input type="hidden" name="agent" id="agent" value="{{ auth()->user()->name }}">
                                            <input type="hidden" name="agent_email" id="agent_email" value="{{ auth()->user()->email }}">
                                        <input type="hidden" name="agent_fname" id="agent_fname" value="{{ auth()->user()->firstname }}">
                                            @if (!empty(auth()->user()->deskphone))
                                                <input type="hidden" name="agent_phone" id="agent_phone" value="{{ auth()->user()->deskphone }}">
                                            @else
                                                <input type="hidden" name="agent_phone" id="agent_phone" value="800-421-9004">
                                            @endif
                                            @if (!empty(auth()->user()->user_ref_id))
                                                <input type="hidden" name="user_ref_id" id="user_ref_id" value="{{ auth()->user()->user_ref_id }}">
                                            @else
                                                <input type="hidden" name="user_ref_id" id="user_ref_id" value="R236">
                                            @endif
        										<input type="hidden" name="agent_id" id="agent_id" value="{{ auth()->user()->sysuserid }}">
        										<input type="hidden" name="manager" id="manager" value="{{ auth()->user()->parentuseridname }}">
                                            @if (!empty($app_id_guid))
                                                <input type="hidden" name="app_id_guid" id="app_id_guid" value="{{ $app_id_guid }}">
                                            @else
                                                <input type="hidden" name="app_id_guid" id="app_id_guid" value=" ">
                                            @endif

                                            @if (!empty($cre_appid))
                                                <input type="hidden" name="app_id" id="app_id" value="{{ $cre_appid }}">
                                            @else
                                                <input type="hidden" name="app_id" id="app_id" value=" ">
                                            @endif

                                            @if (!empty($permit_spec_guid))
                                                <input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value="{{ $permit_spec_guid }}">
                                            @else
                                                <input type="hidden" name="permit_spec_guid" id="permit_spec_guid" value=" ">
                                            @endif

                                            @if (!empty($permit_spec_name))
                                                <input type="hidden" name="permit_spec_name" id="permit_spec_name" value="{{ $permit_spec_name }}">
                                            @else
                                                <input type="hidden" name="permit_spec_name" id="permit_spec_name" value=" ">
                                            @endif

                                            @if (!empty($cre_applicantidname))
                                                <input type="hidden" name="full_name" id="full_name" value="{{ $cre_applicantidname }}">
                                            @else
                                                <input type="hidden" name="full_name" id="full_name" value=" ">
                                            @endif

                                            @if (!empty($firstname))
                                                <input type="hidden" name="firstname" id="firstname" value="{{ $firstname }}">
                                            @else
                                                <input type="hidden" name="firstname" id="firstname" value=" ">
                                            @endif

                                            @if (!empty($middlename))
                                                <input type="hidden" name="middlename" id="middlename" value="{{ $middlename }}">
                                            @else
                                                <input type="hidden" name="middlename" id="middlename" value=" ">
                                            @endif

                                            @if (!empty($lastname))
                                                <input type="hidden" name="lastname" id="lastname" value="{{ $lastname }}">
                                            @else
                                                <input type="hidden" name="lastname" id="lastname" value=" ">
                                            @endif

                                            @if (!empty($cre_bookingdate))
                                                <input type="hidden" name="booking_date" id="booking_date" value="{{ date('m-d-Y', strtotime(substr($cre_bookingdate, 0, 10))) }}">
                                            @else
                                                <input type="hidden" name="booking_date" id="booking_date" value=" ">
                                            @endif

                                            @if (!empty($cre_bookingschool))
                                                <input type="hidden" name="school" id="school" value="{{ $cre_bookingschool }}">
                                            @else
                                                <input type="hidden" name="school" id="school" value=" ">
                                            @endif

                                            @if (!empty($cre_apptype))
                                                <input type="hidden" name="app_type" id="app_type" value="{{ $cre_apptype }}">
                                            @else
                                                <input type="hidden" name="app_type" id="app_type" value=" ">
                                            @endif

                                            @if (!empty($cre_recruiter))
                                                <input type="hidden" name="recruiter" id="recruiter" value="{{ $cre_recruiter }}">
                                            @else
                                                <input type="hidden" name="recruiter" id="recruiter" value=" ">
                                            @endif

                                            @if (!empty($cre_track))
                                                <input type="hidden" name="track" id="track" value="{{ $cre_track }}">
                                            @else
                                                <input type="hidden" name="track" id="track" value=" ">
                                            @endif

                                            @if (!empty($email))
                                                <input type="hidden" name="email" id="email" value="{{ $email }}">
                                            @else
                                                <input type="hidden" name="email" id="email" value=" ">
                                            @endif

                                            @if (!empty($mobilephone))
                                                <input type="hidden" name="mobilephone" id="mobilephone" value="{{ $mobilephone }}">
                                            @else
                                                <input type="hidden" name="mobilephone" id="mobilephone" value=" ">
                                            @endif

                                            @if (!empty($cre_bookingdate))
                                                <input type="hidden" name="booking_status" id="booking_status" value="Booked">
                                            @else
                                                <input type="hidden" name="booking_status" id="booking_status" value=" ">
                                            @endif

                                            @if (!empty($address1_line1))
                                                <input type="hidden" name="address1_line1" id="address1_line1" value="{{ $address1_line1 }}">
                                            @else
                                                <input type="hidden" name="address1_line1" id="address1_line1" value=" ">
                                            @endif

                                            @if (!empty($address1_city))
                                                <input type="hidden" name="address1_city" id="address1_city" value="{{ $address1_city }}">
                                            @else
                                                <input type="hidden" name="address1_city" id="address1_city" value=" ">
                                            @endif

                                            @if (!empty($address1_stateorprovince))
                                                <input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value="{{ $address1_stateorprovince }}">
                                            @else
                                                <input type="hidden" name="address1_stateorprovince" id="address1_stateorprovince" value=" ">
                                            @endif

                                            @if (!empty($address1_postalcode))
                                                <input type="hidden" name="address1_postalcode" id="address1_postalcode" value="{{ $address1_postalcode }}">
                                            @else
                                                <input type="hidden" name="address1_postalcode" id="address1_postalcode" value=" ">
                                            @endif

                                            @if (!empty($address1_latitude))
                                                <input type="hidden" name="address1_latitude" id="address1_latitude" value="{{ $address1_latitude }}">
                                            @else
                                                <input type="hidden" name="address1_latitude" id="address1_latitude" value=" ">
                                            @endif

                                            @if (!empty($address1_longitude))
                                                <input type="hidden" name="address1_longitude" id="address1_longitude" value="{{ $address1_longitude }}">
                                            @else
                                                <input type="hidden" name="address1_longitude" id="address1_longitude" value=" ">
                                            @endif

                                            @if (!empty($gh_conf))
                                                <input type="hidden" name="gh_conf" id="gh_conf" value="{{ $gh_conf }}">
                                            @else
                                                <input type="hidden" name="gh_conf" id="gh_conf" value=" ">
                                            @endif

                                            @if (!empty($dob))
                                                <input type="hidden" name="DOB" id="DOB" value="{{ date('Y-m-d', strtotime(substr($dob, 0, 10))) }}">
                                            @else
                                                <input type="hidden" name="DOB" id="DOB" value=" ">
                                            @endif

                                            @if (!empty($cre_ssn))
                                                <input type="hidden" name="ssn" id="ssn" value="{{ $cre_ssn }}">
                                            @else
                                                <input type="hidden" name="ssn" id="ssn" value=" ">
                                            @endif

                                            @if (!empty($dl_number))
                                                <input type="hidden" name="dl_number" id="dl_number" value="{{ preg_replace("/[^A-Za-z0-9]/", "", $dl_number) }}">
                                            @else
                                                <input type="hidden" name="dl_number" id="dl_number" value=" ">
                                            @endif

                                            @if (!empty($dl_state))
                                                <input type="hidden" name="dl_state" id="dl_state" value="{{ $dl_state }}">
                                            @else
                                                <input type="hidden" name="dl_state" id="dl_state" value=" ">
                                            @endif

                                            @if (!empty($dl_expire))
                                                <input type="hidden" name="dl_expire" id="dl_expire" value="{{ date('Y-m-d', strtotime(substr($dl_expire, 0, 10))) }}">
                                            @else
                                                <input type="hidden" name="dl_expire" id="dl_expire" value=" ">
                                            @endif

                                            @if (!empty($departure_station))
                                                <input type="hidden" name="departure_station" id="departure_station" value="{{ $departure_station }}">
                                            @else
                                                <input type="hidden" name="departure_station" id="departure_station" value=" ">
                                            @endif






                                    </div><!-- end .colm-box section -->
                                </div><!-- end .section section -->


                            </div><!-- end section -->

                            {{--<div class="colm colm6">--}}
                                {{--<div class="map-container">--}}
                                    {{--<div style="height: 400px; width: 400px;">{!! Mapper::render() !!}</div>--}}
                                {{--</div>--}}
                            {{--</div><!-- end .colm6 section -->--}}


                        </div><!-- end frm-row section -->


                    </div><!-- end .form-body section -->
                    <div class="form-footer">
                        <a id="defaultbutton" class="button btn-primary btn-rounded">Default</a>
                        <a id="redbutton" class="button btn-red btn-rounded">Red</a>
                        <a id="bluebutton" class="button btn-blue btn-rounded">Blue</a>
                        <a id="yellowbutton" class="button btn-yellow btn-rounded">Yellow</a>
                        <a id="purplebutton" class="button btn-purple btn-rounded">Purple</a>
                        <a id="greenbutton" class="button btn-green btn-rounded">Green</a>
                    </div><!-- end .form-footer section -->
                </form>

            </div>
        </div>



    </div><!-- end .smart-forms section -->
</div><!-- end .smart-wrap section -->

<div id="contact-modal-form-greyhound" class="smartforms-modal" role="alert">
    <div class="smartforms-modal-container">

        <div class="smartforms-modal-header">
            <h3>Greyhound</h3>
            <a href="#" class="smartforms-modal-close">&times;</a>
        </div><!-- .smartforms-modal-header -->

        <div class="smartforms-modal-body">
            <div class="smart-wrap">
                <div class="smart-forms smart-container wrap-full">
                    <div class="form-body">
                        <form method="post" action="{{ url('/ghfinder') }}" id="smart-form">
                            <div class="colm colm12">

                                <div class="section">
                                    <div class="map-container">
                                        <div style="height: 400px; width: 500px;">{!! Mapper::render() !!}</div>
                                    </div>

                                </div>

                            </div>





                            <div class="smartforms-modal-footer">
                            </div><!-- end .form-footer section -->
                        </form>
                    </div><!-- end .form-body section -->
                </div><!-- end .smart-forms section -->
            </div><!-- end .smart-wrap section -->
        </div><!-- .smartforms-modal-body -->
    </div><!-- .smartforms-modal-container -->
</div><!-- .smartforms-modal -->