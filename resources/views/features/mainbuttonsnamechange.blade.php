<div class="smartforms-px">
    @if (auth()->user()->role_id == "2") {{--general users--}}
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">HireRight</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Greyhound</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Email</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">SMS</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">School Info</a>
    @if (!empty($cre_appid))
        <a href="#" data-smart-modal="#contact-modal-form-namechange" class="smartforms-modal-trigger">Name Change</a>
    @else
        <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Name Change</a>
    @endif
    <a href="{{ URL::to('logout') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Logout</a>
    @elseif(auth()->user()->role_id == "3") {{--managers--}}
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">HireRight</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Greyhound</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Email</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">SMS</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">School Info</a>
    <a href="{{ URL::to('namechange/' . auth()->user()->firstname) }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Name Changes</a>
    <a href="{{ URL::to('dashboard') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Back To Nos</a>
    <a href="{{ URL::to('logout') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Logout</a>

    @elseif(auth()->user()->role_id == "4") {{--admin--}}
    <a href="#" data-smart-modal="#contact-modal-form-hireright" class="smartforms-modal-trigger">HireRight</a> 
    <a href="#" data-smart-modal="#contact-modal-form-greyhound" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Greyhound</a> 
    <a href="#" data-smart-modal="#contact-modal-form-emails" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Email</a> 
    <a href="#" data-smart-modal="#contact-modal-form-sms" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">SMS</a> 
    <a href="#" data-smart-modal="#contact-modal-form-schools" class="smartforms-modal-trigger">School Info</a>
    <a href="/schoolinfo" class="smartforms-modal-trigger">School Info Tables</a>
    <a href="{{ URL::to('dashboard') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Back To Nos</a>
    <a href="#" id="NCRecruiter" class="smartforms-modal-trigger">Name Change</a>
    <a href="#" id="NCRecruiter" data-smart-modal="#contact-modal-form-namechange" class="smartforms-modal-trigger">Name Change new</a>
    <a href="{{ URL::to('logout') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Logout</a>

    @elseif(auth()->user()->role_id == "5") {{--beta users--}}
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">HireRight</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Greyhound</a>
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Email</a>
    <a href="#" data-smart-modal="#contact-modal-form-sms" class="smartforms-modal-trigger">SMS</a> 
    <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">School Info</a>
    @if (!empty($cre_appid))
        <a href="#" data-smart-modal="#contact-modal-form-namechange" class="smartforms-modal-trigger">Name Change</a>
    @else
        <a href="#" class="smartforms-modal-trigger" style="opacity: 0.2;">Name Change</a>
    @endif
    <a href="{{ URL::to('logout') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Logout</a>
    @endif
</div>


