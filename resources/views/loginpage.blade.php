<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-3">

        <div class="form-header header-black">
            <h4><i class="fa fa-sign-in"></i>Login</h4>
        </div><!-- end .form-header section -->

        <form method="POST" id="contact">
            {{ csrf_field() }}
            <div class="form-body theme-black">



                <div class="spacer-b30">
                    <div class="tagline"><span> -Login- </span></div><!-- .tagline -->
                </div>

                <div class="section">
                    <label class="field prepend-icon">
                        <input type="email" name="email" id="inputEmail" class="gui-input" placeholder="Email Address" required autofocus>
                        <span class="field-icon"><i class="fa fa-user"></i></span>
                    </label>
                </div><!-- end section -->


            </div><!-- end .form-body section -->
            <div class="form-footer">
                <button type="submit" class="button btn-black">Sign In</button>
            </div><!-- end .form-footer section -->
        </form>

    </div><!-- end .smart-forms section -->
</div><!-- end .smart-wrap section -->

<div></div><!-- end section -->