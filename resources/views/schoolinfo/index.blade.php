@extends('tableedit.layout')

@section('body')

    <h1>Schools</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('schoolinfo/create') }}">Create a Record</a>
    </ul>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>School Type</td>
            <td>School Address</td>
            <td>School City</td>
            <td>School State</td>
            <td>School Zip</td>
            <td>School Phone</td>
            <td>School Fax</td>
            <td>School Duration Min</td>
            <td>School Duration Max</td>
            <td>Transportation</td>
            <td>Schedule</td>
            <td>Docs</td>
            <td>Fees</td>
            <td>Housing</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($schoolinfo as $key => $value)
            <tr>
                <td>{{ $value->sch_id }}</td>
                <td>{{ $value->sch_name }}</td>
                <td>{{ $value->sch_type }}</td>
                <td>{{ $value->sch_address }}</td>
                <td>{{ $value->sch_city }}</td>
                <td>{{ $value->sch_state }}</td>
                <td>{{ $value->sch_zip }}</td>
                <td>{{ $value->sch_phone }}</td>
                <td>{{ $value->sch_fax }}</td>
                <td>{{ $value->sch_duration_min }}</td>
                <td>{{ $value->sch_duration_max }}</td>
                <td><a href="{{ URL::to('transportationinfo/' . $value->sch_id) }}">View</a></td>
                <td><a href="{{ URL::to('scheduleinfo/' . $value->sch_id) }}">View</a></td>
                <td><a href="{{ URL::to('docsinfo/' . $value->sch_id) }}">View</a></td>
                <td><a href="{{ URL::to('feesinfo/' . $value->sch_id) }}">View</a></td>
                <td><a href="{{ URL::to('housinginfo/' . $value->sch_id) }}">View</a></td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'schoolinfo/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete this School', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}

                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('schoolinfo/' . $value->id) }}">Show this School</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('schoolinfo/' . $value->id . '/edit') }}">Edit this School</a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop