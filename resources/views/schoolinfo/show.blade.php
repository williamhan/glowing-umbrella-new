<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('schoolinfo') }}">Schools</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('schoolinfo') }}">View All Schools</a></li>
            <li><a href="{{ URL::to('schoolinfo/create') }}">Create a School</a>
        </ul>
    </nav>

    <h1>Showing {{ $schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $schoolinfo->sch_name }}</h2>
        <p>
            <strong>School Type:</strong> {{ $schoolinfo->sch_type }}<br>
            <strong>School Address:</strong> {{ $schoolinfo->sch_address }}<br>
            <strong>School City:</strong> {{ $schoolinfo->sch_city }}<br>
            <strong>School State:</strong> {{ $schoolinfo->sch_state }}<br>
            <strong>School Zip:</strong> {{ $schoolinfo->sch_zip }}<br>
            <strong>School Phone:</strong> {{ $schoolinfo->sch_phone }}<br>
            <strong>School Fax:</strong> {{ $schoolinfo->sch_fax }}<br>
            <strong>School Duration Min:</strong> {{ $schoolinfo->sch_duration_min }}<br>
            <strong>School Duration Max:</strong> {{ $schoolinfo->sch_duration_max }}<br>
            <strong>Transportation Id:</strong> {{ $schoolinfo->sch_transportation_id }}<br>
            <strong>Schedule Id:</strong> {{ $schoolinfo->sch_schedule_id }}<br>
            <strong>Docs Id:</strong> {{ $schoolinfo->sch_docs_id }}<br>
            <strong>Fees Id:</strong> {{ $schoolinfo->sch_fees_id }}<br>
            <strong>Housing Id:</strong> {{ $schoolinfo->sch_housing_id }}
        </p>
    </div>

</div>
</body>
</html>