<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('schoolinfo') }}">Schools</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('schoolinfo') }}">View All Schools</a></li>
            <li><a href="{{ URL::to('schoolinfo/create') }}">Create a School</a>
        </ul>
    </nav>

    <h1>Create a School</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'schoolinfo')) }}

    <div class="form-group">
        {{ Form::label('sch_name', 'Name') }}
        {{ Form::text('sch_name', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_type', 'School Type') }}
        {{ Form::select('sch_type', ['0' => 'Select a Type', 'P' => 'Premier', '3P' => 'Third Party', 'T2' => 'Track 2', 'EC' => 'Track 2 EC'], array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_address', 'Address') }}
        {{ Form::text('sch_address', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_city', 'City') }}
        {{ Form::text('sch_city', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_state', 'State') }}
        {{ Form::select('sch_state', $stateslist, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_zip', 'Zip') }}
        {{ Form::text('sch_zip', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_phone', 'Phone') }}
        {{ Form::text('sch_phone', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_fax', 'Fax') }}
        {{ Form::text('sch_fax', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_duration_min', 'Duration Min') }}
        {{ Form::text('sch_duration_min', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_duration_max', 'Duration Max') }}
        {{ Form::text('sch_duration_max', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_transportation_id', 'Transportation Id') }}
        {{ Form::text('sch_transportation_id', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_schedule_id', 'Schedule Id') }}
        {{ Form::text('sch_schedule_id', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_docs_id', 'Docs Id') }}
        {{ Form::text('sch_docs_id', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_fees_id', 'Fees Id') }}
        {{ Form::text('sch_fees_id', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_housing_id', 'Housing Id') }}
        {{ Form::text('sch_housing_id', null, ['class' => 'form-control']) }}
    </div>

    {{ Form::submit('Create the School!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
</html>