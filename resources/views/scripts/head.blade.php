<?php
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('America/Denver');
function getGUID()
{
    if (function_exists('com_create_guid')) {
        return com_create_guid();
    } else {
        mt_srand((double)microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid =             //chr(123) . // "{"
            substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
        //.chr(125);// "}"
        return $uuid;

    }
}

$start_time = date('Y-m-d H:i:s');
$driver_guid = getGUID();

$id = $_GET["id"];
$rs = $_GET["rs"];
$cctrack = $_GET["track_id"];
$email = $_GET["email"];
$fname = $_GET["fname"];
$lname = $_GET["lname"];
$phone = $_GET["phone"];
$address = $_GET["address"];
$zip = $_GET["zip"];
$city = $_GET["city"];

