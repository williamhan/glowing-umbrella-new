@extends('tableedit.layout')

@section('body')




    <h1>Transportation</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('transportationinfo/create') }}">Create a Record</a>
    </ul>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Transportation Id</td>
            <td>School</td>
            <td>Shuttle Type</td>
            <td>Shuttle Day Of Week</td>
            <td>Shuttle Start Time</td>
            <td>Shuttle End Time</td>
            <td>Shuttle Runs After Hours?</td>
            <td>Shuttle Cutoff Time</td>
            <td>What To Do After Cutoff Time</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($transportationinfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->schoolinfo->sch_name }}</td>
                <td>{{ $value->shuttle_type }}</td>
                <td>{{ $value->shuttle_day_of_week }}</td>
                <td>{{ $value->shuttle_start_time }}</td>
                <td>{{ $value->shuttle_end_time }}</td>
                <td>{{ $value->shuttle_runs_after_hours }}</td>
                <td>{{ $value->shuttle_cutoff }}</td>
                <td>{{ $value->shuttle_after_cutoff }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('transportationinfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('transportationinfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'transportationinfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop