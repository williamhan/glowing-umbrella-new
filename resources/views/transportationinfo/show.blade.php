@extends('tableedit.layout')

@section('body')
    @foreach($transportationinfo as $key => $value)
    <h1>Transportation For {{ $value->schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $value->sch_name }}</h2>
        <p>
            <strong>School:</strong> {{ $value->schoolinfo->sch_name }}<br>
            <strong>Shuttle Type:</strong> {{ $value->shuttle_type }}<br>
            <strong>Shuttle Day Of Week:</strong> {{ $value->shuttle_day_of_week }}<br>
            <strong>Shuttle Start Time:</strong> {{ $value->shuttle_start_time }}<br>
            <strong>Shuttle End Time:</strong> {{ $value->shuttle_end_time }}<br>
            <strong>Shuttle Runs After Hours?</strong> {{ $value->shuttle_runs_after_hours }}<br>
            <strong>Shuttle Cutoff Time:</strong> {{ $value->shuttle_cutoff }}<br>
            <strong>What To Do After Cutoff Time:</strong> {{ $value->shuttle_after_cutoff }}<br>
        </p>
    </div>
    @endforeach
@stop
