<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('schools') }}">Schools</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('schools') }}">View All Schools</a></li>
            <li><a href="{{ URL::to('schools/create') }}">Create a School</a>
        </ul>
    </nav>

    <h1>Showing {{ $school->school_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $school->school_name }}</h2>
        <p>
            <strong>GH Station:</strong> {{ $school->gh_station }}<br>
            <strong>School Type:</strong> {{ $school->sch_type }}
        </p>
    </div>

</div>
</body>
</html>