<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('schools') }}">Schools</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('schools') }}">View All Schools</a></li>
            <li><a href="{{ URL::to('schools/create') }}">Create a School</a>
        </ul>
    </nav>

    <h1>Edit {{ $school->school_name }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($school, array('route' => array('schools.update', $school->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('school_name', 'Name') }}
        {{ Form::text('school_name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('gh_station', 'Arrival Station (N/A if No Station)') }}
        {{ Form::text('gh_station', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('sch_type', 'School Type') }}
        {{ Form::select('sch_type', ['0' => 'Select a Type', 'P' => 'Premier', '3P' => 'Third Party', 'T2' => 'Track 2', 'EC' => 'Track 2 EC'], array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Edit the School!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>
</body>
</html>