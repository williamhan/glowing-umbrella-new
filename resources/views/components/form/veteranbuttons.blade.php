<div class="section align-center">Are you a Veteran?
        <div id="veteran_status" class="option-group field">
            <label class="option">
                <input type="radio" id="veteran_status_y" name="veteran_status" class="smartfm-ctrl" value="Y"  data-show-id="honorable_discharge_box">
                <span class="radio"></span> Yes
            </label>
            <label class="option">
                <input type="radio" id="veteran_status_n" name="veteran_status" class="smartfm-ctrl" value="N"  data-show-id="">
                <span class="radio"></span> No
            </label>
        </div><!-- end .option-group section -->

        <div class="spacer-b30"></div>

        <div id="honorable_discharge_box" class="hiddenbox">
            <div class="section">
                <label class="field select">
                    <select id="honorable_discharge" name="honorable_discharge">
                        <option value="">Veteran Status:</option>
                        <option value="Y">I received an honorable discharge</option>
                        <option value="N">I did not receive an honorable discharge</option>
                    </select>
                    <i class="arrow double"></i>
                </label>
            </div>
        </div>
    </div>