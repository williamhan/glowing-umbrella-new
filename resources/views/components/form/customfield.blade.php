<div class="section colm colm{{$width}}">
	<label class="field prepend-icon">
        {{ Form::$attributetype($name, $value, array_merge(['class' => $secondclass], ['id' => $id], ['placeholder' => $placeholder], [$attributes])) }}
        <span class="field-icon">
			<span class="glyphicon  glyphicon-{{$icon}}"></span>
		</span>
    </label>
</div>



{{-- {{ Form::component('customField', 'components.form.customfield', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon', 'width', 'attributetype', 'secondclass']) }} --}}