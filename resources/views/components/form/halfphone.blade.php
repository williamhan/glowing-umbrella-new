<div class="section colm colm6">
	<label class="field prepend-icon">
        {{ Form::tel($name, $value, array_merge(['class' => 'gui-input'], ['id' => $id], ['placeholder' => $placeholder])) }}
        <span class="field-icon">
			<span class="glyphicon {{$icon}}"></span>
		</span>
    </label>
</div>