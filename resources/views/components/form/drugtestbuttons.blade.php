<div class="align-center">
        <div class="frm-row">
            <div class="section colm colm12 align-center">Are you able to pass a drug test?
                <div id="pass_drug_test" class="option-group field">
                    <label class="option">
                        <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="Y">
                        <span class="radio"></span> Yes
                    </label>
                    <label class="option">
                        <input type="radio" name="pass_drug_test" class="smartfm-ctrl" value="N">
                        <span class="radio"></span> No
                    </label>
                </div>
            </div>
        </div>
    </div>