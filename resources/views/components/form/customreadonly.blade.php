<div class="section colm colm{{$width}}">
	<label class="field prepend-icon">
        {{ Form::$attributetype($name, $value, array_merge(['class' => $secondclass], ['id' => $id], ['placeholder' => $placeholder], ['readonly' => 'readonly'])) }}
        <span class="field-icon">
			<span class="glyphicon  glyphicon-{{$icon}}"></span>
		</span>
    </label>
</div>