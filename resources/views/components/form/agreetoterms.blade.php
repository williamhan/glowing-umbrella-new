<div class="align-center">
        <div class="frm-row">
            <div class="option-group field">
                <div class="section colm colm12">
                    <label class="option block">
                        <input type="checkbox" name="agree_to_terms" id="agree_to_terms" value="Yes">
                        <span class="checkbox"></span> I agree to the
                        <a href = "javascript:void(0)" style="color: #999; text-decoration:underline;" onclick = "document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">terms and conditions.</a>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div id="light" class="white_content">By providing the information above, I am providing an electronic signature expressly authorizing C.R. England, its affiliates, or designated third parties to contact me at any address, telephone number or email address I have provided so it can communicate with me regarding my application for employment. I agree that C.R. England may use SMS (text) messages, automatic telephone dialing systems or pre-recorded messages in connection with calls made to any telephone number I entered, even if the telephone number is assigned to a cellular telephone service or other service for which the called party is charged. I understand that C.R. England may record telephone conversations I have with its representatives for quality control and training purposes and I consent to the recording of those calls.
        <a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
    </div>

    <div id="fade" class="black_overlay"></div>

    <div class="spacer-b20"></div>