<div class="section colm colm{{$width}}">
    <label for="{{$name}}" class="field select prepend-icon">
		{{ Form::select($name, $value, null, array_merge(['placeholder' => $placeholder], ['id' => $id])) }}
        <i class="{{$icon}}"></i>
    </label>
</div>

{{-- {{ Form::component('dropdownList', 'components.form.dropdownlist', ['name', 'id', 'value', 'attributes' =>[], 'placeholder', 'icon', 'width']) }}
 --}}{{-- <div class="section colm colm{{$width}}">
                <label for="{{$name}}" class="field select prepend-icon">


                {{ Form::select($name, App\States::pluck('state_name', 'state_abbr'), null, ['placeholder' => 'Choose State'], ['id' => $id]) }}

                <i class="arrow double"></i>
                </label>

                
            </div> --}}

{{-- {{ Form::component('dropdownList', 'components.form.dropdownlist', ['name', 'id', 'placeholder', 'width']) }}
 --}}
{{-- {{ Form::component('dropdownList', 'components.form.dropdownlist', ['name', 'id', 'placeholder', 'width']) }} --}}