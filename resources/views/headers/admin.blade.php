    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-hireright">HireRight</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-greyhound">GreyHound</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-emails">Email</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-sms">SMS</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-schools">School Info</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('schoolinfo') }}">School Info Tables</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('userinfo') }}">User Info</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('namechange/' . auth()->user()->firstname) }}">Name Changes Tabs</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-namechange">Name Change</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('phoneapp') }}">PhoneApp</a>
            <a href="{{ URL::to('dashboard') }}" class="smartforms-modal-trigger" style="color: #FFF; background: #CC0000;">Back To Nos</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('logout') }}">Logout</a>
        </div>
    </nav>