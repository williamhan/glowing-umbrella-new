	<nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-hireright">HireRight</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" style="opacity: 0.2;" {{--data-smart-modal="#contact-modal-form-greyhound"--}}>GreyHound</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" style="opacity: 0.2;" {{--data-smart-modal="#contact-modal-form-emails"--}}>Email</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-sms">SMS</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" style="opacity: 0.2;" {{--data-smart-modal="#contact-modal-form-schools"--}}>School Info</a>
            <a class="navbar-brand smartforms-modal-trigger" href="#" data-smart-modal="#contact-modal-form-namechange">Name Change</a>
            <a class="navbar-brand smartforms-modal-trigger" href="{{ URL::to('logout') }}">Logout</a>
        </div>
	</nav>