@if ($manager == "Jessica Greenwald")
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#home">ALL</a></li>
        <li><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li class="active"><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@elseif ($manager == "Lisa Butler")
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#home">ALL</a></li>
        <li><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li class="active"><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@elseif ($manager == "Nick Moss")
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#home">ALL</a></li>
        <li><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li class="active"><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@elseif ($manager == "Jacob Bushman")
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#home">ALL</a></li>
        <li class="active"><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@elseif ($manager == "Sepeti Moala")
    <ul class="nav nav-tabs">
        <li><a data-toggle="tab" href="#home">ALL</a></li>
        <li><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li class="active"><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@else
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">ALL</a></li>
        <li><a data-toggle="tab" href="#menu1">Jacob</a></li>
        <li><a data-toggle="tab" href="#menu3">Lisa</a></li>
        <li><a data-toggle="tab" href="#menu4">Nick</a></li>
        <li><a data-toggle="tab" href="#menu5">Sepeti</a></li>
        <li><a data-toggle="tab" href="#menu2">Jessica Rec.</a></li>
        <li><a data-toggle="tab" href="#menu6">Jessica P.S.</a></li>
    </ul>
@endif
