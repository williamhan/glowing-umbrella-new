@extends('tableedit.layout')

@section('body')

    @foreach($scheduleinfo as $key => $value)

    <h1>Schedule For {{ $value->schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $value->schoolinfo->sch_name }}</h2>
        <p>
            <strong>School:</strong> {{ $value->schoolinfo->sch_name }}<br>
            <strong>Sunday Class:</strong> {{ $value->schedule_sun }} <strong>Start:</strong> {{ $value->schedule_sun_start_time }} <strong>End:</strong> {{ $value->schedule_sun_end_time }} <br>
            <strong>Monday Class:</strong> {{ $value->schedule_mon }} <strong>Start:</strong> {{ $value->schedule_mon_start_time }} <strong>End:</strong> {{ $value->schedule_mon_end_time }} <br>
            <strong>Tuesday Class:</strong> {{ $value->schedule_tues }} <strong>Start:</strong> {{ $value->schedule_tues_start_time }} <strong>End:</strong> {{ $value->schedule_tues_end_time }} <br>
            <strong>Wednesday Class:</strong> {{ $value->schedule_wed }} <strong>Start:</strong> {{ $value->schedule_wed_start_time }} <strong>End:</strong> {{ $value->schedule_wed_end_time }} <br>
            <strong>Thursday Class:</strong> {{ $value->schedule_thurs }} <strong>Start:</strong> {{ $value->schedule_thurs_start_time }} <strong>End:</strong> {{ $value->schedule_thurs_end_time }} <br>
            <strong>Friday Class:</strong> {{ $value->schedule_fri }} <strong>Start:</strong> {{ $value->schedule_fri_start_time }} <strong>End:</strong> {{ $value->schedule_fri_end_time }} <br>
            <strong>Saturday Class:</strong> {{ $value->schedule_sat }} <strong>Start:</strong> {{ $value->schedule_sat_start_time }} <strong>End:</strong> {{ $value->schedule_sat_end_time }} <br>
            <strong>Is Experienced:</strong> {{ $value->schedule_is_exp }}<br>
            <strong>Is SR:</strong> {{ $value->schedule_is_sr }}<br>
            <strong>Is Track Two:</strong> {{ $value->schedule_is_track_two }}<br>
            <strong>Is SC:</strong> {{ $value->schedule_is_sc }}<br>
        </p>
    </div>

    @endforeach
@stop
