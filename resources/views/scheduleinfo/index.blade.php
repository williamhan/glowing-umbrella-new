@extends('tableedit.layout')

@section('body')




    <h1>Schedules</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('scheduleinfo/create') }}">Create a Record</a>
    </ul>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Schedule Id</td>
            <td>School</td>
            <td>Schedule Sunday</td>
            <td>Schedule Monday</td>
            <td>Schedule Tuesday</td>
            <td>Schedule Wednesday</td>
            <td>Schedule Thursday</td>
            <td>Schedule Friday</td>
            <td>Schedule Saturday</td>
            <td>Schedule Is Exp</td>
            <td>Schedule Is SR</td>
            <td>Schedule Is Track2</td>
            <td>Schedule Is SC</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($scheduleinfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->schoolinfo->sch_name }}</td>
                <td>
                    @if ($value->schedule_sun == "Yes")
                    {{ $value->schedule_sun_start_time }} - {{ $value->schedule_sun_end_time }}
                    @else
                    Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_mon == "Yes")
                        {{ $value->schedule_mon_start_time }} - {{ $value->schedule_mon_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_tues == "Yes")
                        {{ $value->schedule_tues_start_time }} - {{ $value->schedule_tues_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_wed == "Yes")
                        {{ $value->schedule_wed_start_time }} - {{ $value->schedule_wed_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_thurs == "Yes")
                        {{ $value->schedule_thurs_start_time }} - {{ $value->schedule_thurs_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_fri == "Yes")
                        {{ $value->schedule_fri_start_time }} - {{ $value->schedule_fri_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>
                    @if ($value->schedule_sat == "Yes")
                        {{ $value->schedule_sat_start_time }} - {{ $value->schedule_sat_end_time }}
                    @else
                        Closed
                    @endif
                </td>
                <td>{{ $value->schedule_is_exp }}</td>
                <td>{{ $value->schedule_is_sr }}</td>
                <td>{{ $value->schedule_is_track_two }}</td>
                <td>{{ $value->schedule_is_sc }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('scheduleinfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('scheduleinfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'scheduleinfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop