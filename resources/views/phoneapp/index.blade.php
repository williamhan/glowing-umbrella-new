@extends('layouts.basenew')

@section('navbar')
@if (auth()->user()->role_id == "2") {{--general users--}}
@include('headers.users')
@elseif(auth()->user()->role_id == "3") {{--managers--}}
@include('headers.managers')
@elseif(auth()->user()->role_id == "4") {{--admin--}}
@include('headers.admin')
@elseif(auth()->user()->role_id == "5") {{--beta users--}}
@include('headers.beta')
@endif
@stop

@section('body')
    @include('formparts.form')
    {{-- @include('scripts.head') --}}
@stop
{{-- @section('preloader')
    @include('sections.preloader')
@stop
@section('jumbotron')
    @include('sections.jumbotron')
@stop
@section('footer')
    @include('sections.footer')
@stop --}}