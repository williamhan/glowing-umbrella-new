<!DOCTYPE html>
<html lang="en">
<head>

@yield('pretitle')
    <title>C.R. England</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ elixir('js/all.js') }}"></script>
    <script type="text/javascript" src="{{ elixir('js/buttons.js') }}"></script>
    <script type="text/javascript" src="js/signature/excanvas.js"></script>
    <script type="text/javascript" src="js/signature/jSignature.min.noconflict.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/signature/flashcanvas.js"></script>
    <![endif]-->








    <!—- JS Files For Analytics —->
    {{-- <script type="text/javascript" src="js/analytics.js"></script> --}}

        <style>
        .black_overlay{
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: black;
            z-index:1001;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=80);
        }
        .white_content {
            display: none;
            position: absolute;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            padding: 16px;
            border: 16px solid #243140;
            background-color: white;
            z-index:1002;
            overflow: auto;
        }
        #signatureparent {
            color:darkblue;
        }
        #signature {
            padding: 0 0 0 0;
            margin: 0 0 0 0;
            border: 2px solid #585858;
            background-color:#484848;
        }
        .jSignature {
            max-width:948px!important;
            position: relative;
            height: 84px;
            align-content: center;
        }
    </style>
</head>
<body style="overflow:auto; margin:0">


<!-- =========================
     Pre-loader
============================== -->
@yield('preloader')
<!-- =========================
     Section1 - Header
============================== -->
@yield('jumbotron')
<!-- =========================
     Section - Copyright
============================== -->
@yield('footer')





</body>
</html>
