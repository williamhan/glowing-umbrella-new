<?php


if (!empty($_POST['appid']))
{
	$appid = $_POST['appid'];
}

elseif (empty($_POST['appid']))
{
	$appid = "2193661";
}
//$appid = $_POST['appid'];

$url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/1678?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";

$jsonData = array(
        'appid' => $appid
);


$jsonDataEncoded = json_encode($jsonData);


// Initialize curl
$curl = curl_init();

// Configure curl options
$opts = array(
        CURLOPT_URL             => $url,
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_CUSTOMREQUEST   => 'POST',
        CURLOPT_POST            => 1,
        CURLOPT_POSTFIELDS      => $jsonDataEncoded
);

// Set curl options
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
curl_setopt_array($curl, $opts);

// Get the results
$result = curl_exec($curl);

// Close resource
curl_close($curl);

$response = json_decode($result, true);


$app_id = $response["appInfo"][0]["cre_appid"];


$full_name = ucwords($response["appInfo"][0]["cre_applicantidname"]);


$firstname = ucwords($response["appInfo"][0]["firstname"]);


$lastname = ucwords($response["appInfo"][0]["lastname"]);


$email = $response["appInfo"][0]["email"];


if (!empty($response["appInfo"][0]["cre_bookingdate"]))
{

    $booking_date = substr($response["appInfo"][0]["cre_bookingdate"], 0, 10);

    $time = strtotime($booking_date);

    $newformat = date('m-d-Y',$time);

    $booking_status = "Booked";
}
else
{
    $newformat = " ";

    $booking_status = "Not Booked";
}


$school = $response["appInfo"][0]["cre_bookingschool"];


$app_type = $response["appInfo"][0]["cre_apptype"];


$recruiter = $response["appInfo"][0]["cre_recruiter"];


$track = $response["appInfo"][0]["cre_track"];


$mobilephone = $response["appInfo"][0]["mobilephone"];


$phoneone = $response["appInfo"][0]["telephone1"];


$phonetwo = $response["appInfo"][0]["telephone2"];


$phonethree = $response["appInfo"][0]["telephone3"];


$address1_line1 = $response["appInfo"][0]["address1_line1"];


$address1_city = $response["appInfo"][0]["address1_city"];


$address1_stateorprovince = $response["appInfo"][0]["address1_stateorprovince"];


$zip = $response["appInfo"][0]["address1_postalcode"];


$address1_latitude = $response["appInfo"][0]["address1_latitude"];


$address1_longitude = $response["appInfo"][0]["address1_longitude"];



//$employmenttype =

if ($track == "Non-Native")
{
    if ($app_type == "SC - School Student")
    {
        $employmenttype = "Track 2";
    }
}

elseif ($track == "Non-Native EC")
{
    $employmenttype = "E.C.";
}

else
{
    $employmenttype = $app_type;
}








?>