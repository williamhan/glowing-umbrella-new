<!doctype html>
<html lang="en">
<head>
    <title> OCF Remastered </title>
    @section('head')
        <meta charset="utf-8">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ elixir('css/all.css') }}" />
        <script type="text/javascript" src="{{ elixir('js/all.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/buttons.js') }}"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    @show
</head>
<body class="woodbg">
<div class="smartforms-px">
<div class="container">
@yield('navbar')
</div>
</div>
@yield('body')
</body>
</html>