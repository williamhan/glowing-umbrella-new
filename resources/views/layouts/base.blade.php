<!doctype html>
<html lang="en">
<head>
    <title> OCF Remastered </title>
    @section('head')
    <meta charset="utf-8">
        <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="{{ elixir('css/all.css') }}" />
        <script type="text/javascript" src="{{ elixir('js/all.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('js/buttons.js') }}"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <style type="text/css">
            #map {
                width: 400px;
                height: 400px;
            }
            .vlft {
    border-left: 1px solid #CFCFCF;
    height: 100%;
}
.vrt {
    border-right: 1px solid #CFCFCF;
    height: 100%;
}
        </style>
    @show
</head>
<body class="woodbg">
<div class="smartforms-px">
<div class="container">
@yield('navbar')
</div>
</div>
@yield('body')
</body>
</html>

