@extends('tableedit.layout')

@section('body')

    <h1>Info For {{ $userinfo->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $userinfo->name }}</h2>
        <p>
            <strong>Name:</strong> {{ $userinfo->name }}<br>
            <strong>First Name:</strong> {{ $userinfo->firstname }}<br>
            <strong>Last Name:</strong> {{ $userinfo->lastname }}<br>
            <strong>Email:</strong> {{ $userinfo->email }}<br>
            <strong>Manager:</strong> {{ $userinfo->parentuseridname }}<br>
            <strong>Desk Phone:</strong> {{ $userinfo->deskphone }}<br>
        </p>
    </div>
@stop
