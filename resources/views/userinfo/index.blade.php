@extends('tableedit.layout')

@section('body')




    <h1>Users</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('userinfo/create') }}">Create a Record</a>
    </ul>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>User Id</td>
            <td>Name</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Email</td>
            <td>Manager</td>
            <td>Desk Phone</td>
            <td>Title</td>
            <td>HireRight Id</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($userinfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->firstname }}</td>
                <td>{{ $value->lastname }}</td>
                <td>{{ $value->email }}</td>
                <td>{{ $value->parentuseridname }}</td>
                <td>{{ $value->deskphone }}</td>
                <td>{{ $value->title }}</td>
                <td>{{ $value->user_ref_id }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('userinfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('userinfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'userinfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop