@extends('tableedit.layout')
@section('body')
    <h1>Housing</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('housinginfo/create') }}">Create a Record</a>
    </ul>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Housing Id</td>
            <td>School</td>
            <td>Housing Provided:</td>
            <td>Housing Type:</td>
            <td>Housing Name:</td>
            <td>Address:</td>
            <td>City, State Zip:</td>
            <td>Phone:</td>
            <td>Occupancy:</td>
            <td>Has Shuttle?</td>
            <td>Shuttle Id</td>
            <td>Is Exp?</td>
            <td>Is SR?</td>
            <td>Is Track 2?</td>
            <td>Is SC?</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($housinginfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->schoolinfo->sch_name }}</td>
                <td>{{ $value->housing_provided }}</td>
                <td>{{ $value->housing_type }}</td>
                <td>{{ $value->housing_name }}</td>
                <td>{{ $value->housing_address }}</td>
                <td>{{ $value->housing_city }}, {{ $value->housing_state }} {{ $value->housing_zip }}</td>
                <td>{{ $value->housing_phone }}</td>
                <td>{{ $value->housing_occupancy }}</td>
                <td>{{ $value->housing_has_shuttle }}</td>
                <td>{{ $value->housing_shuttle_id }}</td>
                <td>{{ $value->housing_is_exp }}</td>
                <td>{{ $value->housing_is_sr }}</td>
                <td>{{ $value->housing_is_track_two }}</td>
                <td>{{ $value->housing_is_sc }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('housinginfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('housinginfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'housinginfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop