@extends('tableedit.layout')

@section('body')
    @foreach($housinginfo as $key => $value)
    <h1>Housing For {{ $value->schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $value->schoolinfo->sch_name }}</h2>
        <p>
            <strong>School:</strong> {{ $value->schoolinfo->sch_name }}<br>
            <strong>Housing Provided:</strong> {{ $value->housing_provided }}<br>
            <strong>Housing Type:</strong> {{ $value->housing_type }}<br>
            <strong>Housing Name:</strong> {{ $value->housing_name }}<br>
            <strong>Address:</strong> {{ $value->housing_address }}<br>
            <strong>City, State Zip:</strong> {{ $value->housing_city }}, {{ $value->housing_state }} {{ $value->housing_zip }}<br>
            <strong>Phone:</strong> {{ $value->housing_phone }}<br>
            <strong>Occupancy:</strong> {{ $value->housing_occupancy }}<br>
            <strong>Has Shuttle?</strong> {{ $value->housing_has_shuttle }}<br>
            <strong>Shuttle ID:</strong> {{ $value->housing_shuttle_id }}<br>
            <strong>Is Exp?</strong> {{ $value->housing_is_exp }}<br>
            <strong>Is SR?</strong> {{ $value->housing_is_sr }}<br>
            <strong>Is Track 2?</strong> {{ $value->housing_is_track_two }}<br>
            <strong>Is SC?</strong> {{ $value->housing_is_sc }}<br>
        </p>
    </div>
    @endforeach
@stop
