@extends('tableedit.layout')

@section('body')
    <h1>Create a Record</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'housinginfo')) }}

    <div class="form-group">
        {{ Form::label('sch_id', 'School') }}
        {{ Form::select('sch_id', $schoollist, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_provided', 'Housing Provided:') }}
        {{ Form::select('housing_provided', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_type', 'Housing Type:') }}
        {{ Form::select('housing_type', ['1' => 'Hotel', '2' => 'On-Site', '3' => 'Other'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_name', 'Housing Name:') }}
        {{ Form::text('housing_name', null, ['class' => 'form-control']) }}
        {{--{{ Form::text('shuttle_start_time', null, ['class' => 'form-control']) }}--}}
    </div>

    <div class="form-group">
        {{ Form::label('housing_address', 'Address:') }}
        {{ Form::text('housing_address', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_city', 'City') }}
        {{ Form::text('housing_city', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_state', 'State') }}
        {{ Form::select('housing_state', $stateslist, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_zip', 'Zip') }}
        {{ Form::text('housing_zip', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_phone', 'Phone:') }}
        {{ Form::text('housing_phone', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_occupancy', 'Occupancy:') }}
        {{ Form::select('housing_occupancy', ['1' => 'Single', '2' => 'Double', '3' => 'Triple', '4' => 'Quadruple', '8' => '8'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_has_shuttle', 'Has Shuttle?') }}
        {{ Form::select('housing_has_shuttle', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_shuttle_id', 'Shuttle ID:') }}
        {{ Form::text('housing_shuttle_id', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_exp', 'Is Exp?') }}
        {{ Form::select('housing_is_exp', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_sr', 'Is SR?') }}
        {{ Form::select('housing_is_sr', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_track_two', 'Is Track 2?') }}
        {{ Form::select('housing_is_track_two', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_sc', 'Is SC?') }}
        {{ Form::select('housing_is_sc', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    {{ Form::submit('Create the Housing!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}


    @stop
