@extends('tableedit.layout')

@section('body')

    <h1>Edit Housing For {{ $housinginfo->schoolinfo->sch_name }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($housinginfo, array('route' => array('housinginfo.update', $housinginfo->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('sch_id', 'School') }}
        {{ Form::select('sch_id', $schoollist, $housinginfo->sch_id, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_provided', 'Housing Provided:') }}
        {{ Form::select('housing_provided', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_provided, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_type', 'Housing Type:') }}
        {{ Form::select('housing_type', ['1' => 'Hotel', '2' => 'On-Site', '3' => 'Other'], $housinginfo->housing_type, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_name', 'Housing Name:') }}
        {{ Form::text('housing_name', $housinginfo->housing_name, ['class' => 'form-control']) }}
        {{--{{ Form::text('shuttle_start_time', null, ['class' => 'form-control']) }}--}}
    </div>

    <div class="form-group">
        {{ Form::label('housing_address', 'Address:') }}
        {{ Form::text('housing_address', $housinginfo->housing_address, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_city', 'City') }}
        {{ Form::text('housing_city', $housinginfo->housing_city, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_state', 'State') }}
        {{ Form::select('housing_state', $stateslist, $housinginfo->housing_state, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_zip', 'Zip') }}
        {{ Form::text('housing_zip', $housinginfo->housing_zip, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_phone', 'Phone:') }}
        {{ Form::text('housing_phone', $housinginfo->housing_phone, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_occupancy', 'Occupancy:') }}
        {{ Form::select('housing_occupancy', ['1' => 'Single', '2' => 'Double', '3' => 'Triple', '4' => 'Quadruple', '8' => '8'], $housinginfo->housing_occupancy, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_has_shuttle', 'Has Shuttle?') }}
        {{ Form::select('housing_has_shuttle', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_has_shuttle, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_shuttle_id', 'Shuttle ID:') }}
        {{ Form::text('housing_shuttle_id', $housinginfo->housing_shuttle_id, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_exp', 'Is Exp?') }}
        {{ Form::select('housing_is_exp', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_is_exp, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_sr', 'Is SR?') }}
        {{ Form::select('housing_is_sr', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_is_sr, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_track_two', 'Is Track 2?') }}
        {{ Form::select('housing_is_track_two', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_is_track_two, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('housing_is_sc', 'Is SC?') }}
        {{ Form::select('housing_is_sc', ['Yes' => 'Yes', 'No' => 'No'], $housinginfo->housing_is_sc, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    {{ Form::submit('Edit the Housing!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@stop