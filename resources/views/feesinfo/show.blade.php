@extends('tableedit.layout')

@section('body')
    @foreach($feesinfo as $key => $value)
    <h1>Fees For {{ $value->schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $value->sch_name }}</h2>
        <p>
            <strong>School:</strong> {{ $value->schoolinfo->sch_name }}<br>
            <strong>fee_state:</strong> {{ $value->fee_state }}<br>
            <strong>fee_type:</strong> {{ $value->fee_type }}<br>
            <strong>fee_amount:</strong> {{ $value->fee_amount }}<br>
            <strong>fee_is_exp:</strong> {{ $value->fee_is_exp }}<br>
            <strong>fee_is_sr:</strong> {{ $value->fee_is_sr }}<br>
            <strong>fee_is_track_two:</strong> {{ $value->fee_is_track_two }}<br>
            <strong>fee_is_sc:</strong> {{ $value->fee_is_sc }}<br>
        </p>
    </div>
    @endforeach
@stop
