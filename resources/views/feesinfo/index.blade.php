@extends('tableedit.layout')

@section('body')




    <h1>Fees</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('feesinfo/create') }}">Create a Record</a>
    </ul>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Fee Id</td>
            <td>School</td>
            <td>Fee State</td>
            <td>Fee Type</td>
            <td>Fee Amount</td>
            <td>Is Exp</td> 
            <td>Is SR</td>
            <td>Is Track 2</td>
            <td>Is SC</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($feesinfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->schoolinfo->sch_name }}</td>
                <td>{{ $value->fee_state }}</td>
                <td>{{ $value->fee_type }}</td>
                <td>{{ $value->fee_amount }}</td>
                <td>{{ $value->fee_is_exp }}</td>
                <td>{{ $value->fee_is_sr }}</td>
                <td>{{ $value->fee_is_track_two }}</td>
                <td>{{ $value->fee_is_sc }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('feesinfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('feesinfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'feesinfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop