@extends('tableedit.layout')

@section('body')

    <h1>Edit Fees For {{ $feesinfo->schoolinfo->sch_name }}</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::model($feesinfo, array('route' => array('feesinfo.update', $feesinfo->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('sch_id', 'School') }}
        {{ Form::select('sch_id', $schoollist, $feesinfo->sch_id,array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_state', 'fee_state') }}
        {{ Form::select('fee_state', $stateslist, $feesinfo->fee_state, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_type', 'fee_type') }}
        {{ Form::text('fee_type', $feesinfo->fee_type, array('class' => 'form-control')) }} 
    </div>

    <div class="form-group">
        {{ Form::label('fee_amount', 'fee_amount') }}
        {{ Form::text('fee_amount', $feesinfo->fee_amount, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_exp', 'fee_is_exp') }}
        {{ Form::select('fee_is_exp', ['Yes' => 'Yes', 'No' => 'No'], $feesinfo->fee_is_exp, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_sr', 'fee_is_sr') }}
        {{ Form::select('fee_is_sr', ['Yes' => 'Yes', 'No' => 'No'], $feesinfo->fee_is_sr, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_track_two', 'fee_is_track_two') }}
        {{ Form::select('fee_is_track_two', ['Yes' => 'Yes', 'No' => 'No'], $feesinfo->fee_is_track_two, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_sc', 'fee_is_sc') }}
        {{ Form::select('fee_is_sc', ['Yes' => 'Yes', 'No' => 'No'], $feesinfo->fee_is_sc, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    {{ Form::submit('Edit the Fees!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

@stop
