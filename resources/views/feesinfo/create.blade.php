@extends('tableedit.layout')

@section('body')
    <h1>Create a Record</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'feesinfo')) }}

    <div class="form-group">
        {{ Form::label('sch_id', 'School') }}
        {{ Form::select('sch_id', $schoollist, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_state', 'fee_state') }}
        {{ Form::select('fee_state', $stateslist, array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_type', 'fee_type') }}
        {{ Form::text('fee_type', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_amount', 'fee_amount') }}
        {{ Form::text('fee_amount', null, array('class' => 'form-control')) }}
        {{--{{ Form::text('shuttle_start_time', null, ['class' => 'form-control']) }}--}}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_exp', 'fee_is_exp') }}
        {{ Form::select('fee_is_exp', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_sr', 'fee_is_sr') }}
        {{ Form::select('fee_is_sr', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_track_two', 'fee_is_track_two') }}
        {{ Form::select('fee_is_track_two', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('fee_is_sc', 'fee_is_sc') }}
        {{ Form::select('fee_is_sc', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    {{ Form::submit('Create the Fees!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}


    @stop
