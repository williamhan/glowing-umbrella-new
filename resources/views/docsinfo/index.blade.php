@extends('tableedit.layout')
@section('body')
    <h1>Docs</h1>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('docsinfo/create') }}">Create a Record</a>
    </ul>
    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-responsive table-bordered">
        <thead>
        <tr>
            <td>Doc Id</td>
            <td>School</td>
            <td>is alt</td>
            <td>doc name</td>
            <td>doc is required</td>
            <td>doc has alt</td>
            <td>alt doc id one</td>
            <td>alt doc id two</td>
            <td>alt doc id three</td>
            <td>doc temp ok</td>
            <td>doc laminated ok</td>
            <td>doc receipt ok</td>
            <td>doc is exp</td>
            <td>doc is sr</td>
            <td>doc is track_two</td>
            <td>doc is sc</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($docsinfo as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                <td>{{ $value->schoolinfo->sch_name }}</td>
                <td>{{ $value->is_alt }}</td>
                <td>{{ $value->doc_name }}</td>
                <td>{{ $value->doc_is_required }}</td>
                <td>{{ $value->doc_has_alt }}</td>
                <td>{{ $value->alt_doc_id_one }}</td>
                <td>{{ $value->alt_doc_id_two }}</td>
                <td>{{ $value->alt_doc_id_three }}</td>
                <td>{{ $value->doc_temp_ok }}</td>
                <td>{{ $value->doc_laminated_ok }}</td>
                <td>{{ $value->doc_receipt_ok }}</td>
                <td>{{ $value->doc_is_exp }}</td>
                <td>{{ $value->doc_is_sr }}</td>
                <td>{{ $value->doc_is_track_two }}</td>
                <td>{{ $value->doc_is_sc }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>



                    <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                    <a class="btn btn-small btn-success" href="{{ URL::to('docsinfo/' . $value->id) }}">Show this Record</a>

                    <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('docsinfo/' . $value->id . '/edit') }}">Edit this Record</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'docsinfo/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Record', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop