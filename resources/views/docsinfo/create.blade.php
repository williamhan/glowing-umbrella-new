@extends('tableedit.layout')

@section('body')
    <h1>Create a Record</h1>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    {{ Form::open(array('url' => 'docsinfo')) }}

    <div class="form-group">
        {{ Form::label('sch_id', 'School') }}
        {{ Form::select('sch_id', $schoollist, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('is_alt', 'Is Alternate?') }}
        {{ Form::select('is_alt', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_name', 'Name Of Document') }}
        {{ Form::text('doc_name', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_is_required', 'Is Document Required?') }}
        {{ Form::select('doc_is_required', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
        {{--{{ Form::text('shuttle_start_time', null, ['class' => 'form-control']) }}--}}
    </div>

    <div class="form-group">
        {{ Form::label('doc_has_alt', 'Does Document Have Alternate?') }}
        {{ Form::select('doc_has_alt', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('alt_doc_id_one', 'Alternate Document 1:') }}
        {{ Form::text('alt_doc_id_one', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('alt_doc_id_two', 'Alternate Document 2:') }}
        {{ Form::text('alt_doc_id_two', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('alt_doc_id_three', 'Alternate Document 3:') }}
        {{ Form::text('alt_doc_id_three', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_temp_ok', 'Is Temporary Ok?') }}
        {{ Form::select('doc_temp_ok', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_laminated_ok', 'Is Laminated Ok?') }}
        {{ Form::select('doc_laminated_ok', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_receipt_ok', 'Is A Receipt Ok?') }}
        {{ Form::select('doc_receipt_ok', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_is_exp', 'Is Exp?') }}
        {{ Form::select('doc_is_exp', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_is_sr', 'Is SR?') }}
        {{ Form::select('doc_is_sr', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_is_track_two', 'Is Track 2?') }}
        {{ Form::select('doc_is_track_two', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    <div class="form-group">
        {{ Form::label('doc_is_sc', 'Is SC?') }}
        {{ Form::select('doc_is_sc', ['Yes' => 'Yes', 'No' => 'No'], array('class' => 'form-control'), ['placeholder' => 'Select Option...']) }}
    </div>

    {{ Form::submit('Create the Docs!', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}


    @stop
