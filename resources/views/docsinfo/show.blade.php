@extends('tableedit.layout')

@section('body')

    @foreach($docsinfo as $key => $value)

    <h1>Docs For {{ $value->schoolinfo->sch_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $value->sch_name }}</h2>
        <p>
            <strong>School:</strong> {{ $value->schoolinfo->sch_name }}<br>
            <strong>Is Alternate?</strong> {{ $value->is_alt }}<br>
            <strong>Name Of Document:</strong> {{ $value->doc_name }}<br>
            <strong>Is Document Required?</strong> {{ $value->doc_is_required }}<br>
            <strong>Does Document Have Alternate?</strong> {{ $value->doc_has_alt }}<br>
            <strong>Alternate Document 1:</strong> {{ $value->alt_doc_id_one }}<br>
            <strong>Alternate Document 2:</strong> {{ $value->alt_doc_id_two }}<br>
            <strong>Alternate Document 3:</strong> {{ $value->alt_doc_id_three }}<br>
            <strong>Is Temporary Ok?</strong> {{ $value->doc_temp_ok }}<br>
            <strong>Is Laminated Ok?</strong> {{ $value->doc_laminated_ok }}<br>
            <strong>Is A Receipt Ok?</strong> {{ $value->doc_receipt_ok }}<br>
            <strong>Is Exp?</strong> {{ $value->doc_is_exp }}<br>
            <strong>Is SR?</strong> {{ $value->doc_is_sr }}<br>
            <strong>Is Track 2?</strong> {{ $value->doc_is_track_two }}<br>
            <strong>Is SC?</strong> {{ $value->doc_is_sc }}<br>
        </p>
    </div>
    @endforeach
@stop
