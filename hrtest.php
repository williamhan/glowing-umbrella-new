<?php
// INCLUDE SECURITY HEADER CLASS//
include ('WS-Security.php');
// DATA PRINT FOR TERMINAL RUN //
print_r("Calling HireRight Connect Web Service ..\r\n");
// SIMILAR TO SoapClient BUT ADDING HEADERS//
$hirerightclient = new WSSoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE, 'soap_version' => SOAP_1_1));
$hirerightclient->__setLocation('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); // IONA - Explicitly setting the location forced the POST headers to be set as needed
$hirerightclient->__setUsernameToken("wsapi_api", "wsapi_pswd");

$HRObject = new StdClass();
$tmpObj = new StdClass();
$srvcObj = new StdClass();
$flxfieldObj = new StdClass();
$payloadObj = new StdClass();
$bgcheckObj = new StdClass();
$bgsearchpkgObj = new StdClass();
$personaldataObj = new StdClass();
$personalObj = new StdClass();
$nameObj = new StdClass();
$addressObj = new StdClass();
$contactoneObj = new StdClass();
$phoneoneObj = new StdClass();
$phonetwoObj = new StdClass();
$contacttwoObj = new StdClass();
$demographicObj = new StdClass();
$addresslineObj = new StdClass();
$servicedata = '<ns1:Service id="MVR Standard"/>';
$flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
$flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
$flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';
$ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">111111111</GovernmentId>';
$supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';
$srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);
$flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);
$addresslineObj->AddressLine = new SoapVar("5151 California Avenue", XSD_STRING);
$phoneoneObj->FormattedNumber = new SoapVar("3721236789", XSD_STRING);
$phonetwoObj->FormattedNumber = new SoapVar("123456789", XSD_STRING);

$nameObj = array();
$nameObj[] = new SoapVar("Fname", XSD_STRING, null, null, 'GivenName');
$nameObj[] = new SoapVar("Mname", XSD_STRING, null, null, 'MiddleName');
$nameObj[] = new SoapVar("Lname", XSD_STRING, null, null, 'FamilyName');

$addressObj = array();
$addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
$addressObj[] = new SoapVar("90210", XSD_STRING, null, null, 'PostalCode');
$addressObj[] = new SoapVar("OH", XSD_STRING, null, null, 'Region');
$addressObj[] = new SoapVar("Municipality", XSD_STRING, null, null, 'Municipality');
$addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

$contactoneObj = array();
$contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
$contactoneObj[] = new SoapVar($phoneoneObj, SOAP_ENC_OBJECT, null, null, 'Telephone');
$contactoneObj[] = new SoapVar("test@hireright.com", XSD_STRING, null, null, 'InternetEmailAddress');

$contacttwoObj = array();
$contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
$contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

$demographicObj = array();
$demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
$demographicObj[] = new SoapVar("1938-12-31", XSD_STRING, null, null, 'DateOfBirth');
$demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
$demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
$demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
$demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
$demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
$demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
$demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
$demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
$demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

$persondata = array();
$persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
$persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
$persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
$persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
$persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

$contactmethod = array();

$personaldataObj = array();
$personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
$personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);

$bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

$bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

$payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

// The final parameter in this call forces the namespace of the data element to reference hireright namespace - until I put this in, the receiving service did not recognize this element
$tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->ClientApplicantId = new SoapVar('AppID_170109_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->ClientRequestId = new SoapVar('ReqID_170109_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

$HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

$CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

try {
    $ret = $hirerightclient->__SoapCall('Create', array($CreateParms));
} catch (Exception $e) {

    echo 'Request Headers:' . $hirerightclient->__getLastRequestHeaders() . "\n\n";

    echo "REQUEST:\n" . str_ireplace('><', ">\n<", $hirerightclient->__getLastRequest()) . "\n";
    echo "Response:\n" . str_ireplace('><', ">\n<", $hirerightclient->__getLastResponse()) . "\n";

    echo 'Exception: ' . print_r($e, TRUE);
}

//echo 'Request Headers:' . $hirerightclient->__getLastRequestHeaders() . "\n\n";

//echo "REQUEST:\n" . str_ireplace('><', ">\n<", $hirerightclient->__getLastRequest()) . "\n";
//echo "Response:\n" . str_ireplace('><', ">\n<", $hirerightclient->__getLastResponse()) . "\n";
print_r($ret);
?>