<?php

$passmvrexp='wsapi_pswd';
$createdmvrexp = gmdate('Y-m-d\TH:i:s\Z');
$noncemvrexp = mt_rand();
$passdigestmvrexp = base64_encode(pack('H*', sha1(pack('H*', $noncemvrexp) . pack('a*', $createdmvrexp) . pack('a*', $passmvrexp))));




$hirerightclientmvrexp = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));


$authmvrexp = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
$authmvrexp .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
$authmvrexp .= '				<wsse:Username>wsapi_api</wsse:Username>';
$authmvrexp .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrexp.'</wsse:Password>';
$authmvrexp .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrexp)).'</wsse:Nonce>';
$authmvrexp .= '				<wsu:Created>' . $createdmvrexp . '</wsu:Created>';
$authmvrexp .= '			</wsse:UsernameToken>';
$authmvrexp .= '		</wsse:Security>';
$auth_blockmvrexp = new SoapVar( $authmvrexp, XSD_ANYXML, NULL, NULL, NULL, NULL );


$headermvrexp = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrexp );
$hirerightclientmvrexp->__setSoapHeaders( $headermvrexp );
$hirerightclientmvrexp->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


/**
 * This function implements a WS-Security digest authentification for PHP.
 *
 * @access private
 * @param string $user
 * @param string $password
 * @return SoapHeader
 */




// INCLUDE SECURITY HEADER CLASS//
//include ('WSSE-Auth.php');
// DATA PRINT FOR TERMINAL RUN //
print_r("Calling HireRight Connect Web Service ..\r\n");
// SIMILAR TO SoapClient BUT ADDING HEADERS//



//$authheader = new WsseAuthHeader("wsapi_api", "wsapi_pswd");

//$hirerightclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE, 'soap_version' => SOAP_1_1));
//$hirerightclient->__setSoapHeaders(soapClientWSSecurityHeader('wsapi_api', 'wsapi_pswd'));


//$hirerightclient = new WSSoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE, 'soap_version' => SOAP_1_1));
//$hirerightclient->__setLocation('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2'); // IONA - Explicitly setting the location forced the POST headers to be set as needed

//$headerbody = array('Username'=>"wsapi_api",
//    'Password'=>"wsapi_pswd");
//$header = new SOAPHeader("urn:enterprise.soap.hireright.com/objs", 'Security', array('Username'=>"wsapi_api",
//    'Password'=>"wsapi_pswd"));
//
////set the Headers of Soap Client.
//$hirerightclient->__setSoapHeaders($header);

//$hirerightclient->__setUsernameToken("wsapi_api", "wsapi_pswd");
/* TREE STRUCTURE OF CALL:
                          ENVELOPE
                                |
                                |--> BODY
                                      |
                                      |-->hrobjs:CREATE
                                                      |
                                                      |-->hrobjs:HROBJECT
                                                                      |
                                                                      |-->$tmpObj
                                                                              |-CompanyLogin
                                                                              |-AccountId
                                                                              |-UserRefId
                                                                              |-ClientApplicantId
                                                                              |-ClientRequestId
                                                                              |-PackageId
                                                                              |-Services
                                                                                    |-->Service id="MVR Standard"
                                                                              |-FlexFields
                                                                                    |-->FlexField name="ReqNumber"
                                                                                    |-->FlexField name="ClientStatusReceiverURL"
                                                                              |-Payload
                                                                                    |-BackgroundCheck
                                                                                        |-BackgroundSearchPackage
                                                                                            |-PersonalData
                                                                                                |-PersonName
                                                                                                    |-GivenName
                                                                                                    |-MiddleName
                                                                                                    |-FamilyName
*/


$HRObject = new StdClass();

//$app = app();
$HRWebLink = new StdClass();
$urlObj = new StdClass();



$HRObject = new StdClass();

$tmpObj = new StdClass();

$srvcObj = new StdClass();

$flxfieldObj = new StdClass();

$payloadObj = new StdClass();

$bgcheckObj = new StdClass();

$phoneoneObj = new StdClass();

$phonetwoObj = new StdClass();

$addresslineObj = new StdClass();


$servicedata = '<ns1:Service id="CDLIS+"/>';



$flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
$flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
$flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

$ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

$supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

$licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>EXT</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

$srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

$flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

$addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

$phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

$phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

$nameObj = array();
$nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
$nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
$nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

$addressObj = array();
$addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
$addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
$addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
$addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
$addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

$contactoneObj = array();
$contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
$contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
$contactoneObj[] = new SoapVar('cdliserror@drivecretest.com', XSD_STRING, null, null, 'InternetEmailAddress');

$contacttwoObj = array();
$contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
$contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

$demographicObj = array();
$demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
$demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
$demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
$demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
$demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
$demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
$demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
$demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
$demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
$demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
$demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

$persondata = array();
$persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
$persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
$persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
$persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
$persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

$personaldataObj = array();
$personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
$personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
$personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

$bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

$bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

$payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

$tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
$tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

$HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

$CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);
//$CreateParms = new SoapVar($HRObject, SOAP_ENC_OBJECT);
//$hirerightclient->__setLocation(" https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

try {
    //$ret = $hirerightclient->__soapCall('Create', array($CreateParms));
    $ret = $hirerightclientmvrexp->__soapCall("Create", array($CreateParms));
    //$ret = $hirerightclient->Create($CreateParms);
} catch (Exception $e) {

    echo 'Request Headers:' . $hirerightclientmvrexp->__getLastRequestHeaders() . "\n\n";

    //echo 'Request:' . $hirerightclient->__getLastRequest() . '<br><br>';

    echo "REQUEST:\n" . str_ireplace('><', ">\n<", $hirerightclientmvrexp->__getLastRequest()) . "\n";
    echo "Response:\n" . str_ireplace('><', ">\n<", $hirerightclientmvrexp->__getLastResponse()) . "\n";

    //echo 'Response: ' . $hirerightclient->__getLastResponse() . '<br><br>';

    echo 'Exception: ' . print_r($e, TRUE);
}

//echo 'Click <a href="' . $ret->URL . '" target="_blank">here</a>';

echo 'Request Headers:' . $hirerightclientmvrexp->__getLastRequestHeaders() . "\n\n";

//echo 'Request:' . $hirerightclient->__getLastRequest() . '<br><br>';

echo "REQUEST:\n" . str_ireplace('><', ">\n<", $hirerightclientmvrexp->__getLastRequest()) . "\n";
echo "Response:\n" . str_ireplace('><', ">\n<", $hirerightclientmvrexp->__getLastResponse()) . "\n";

print_r($ret);

//var_dump($e);
?>