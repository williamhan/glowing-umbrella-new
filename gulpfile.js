const elixir = require('laravel-elixir');
elixir.config.publicDir = 'public_html';
require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function(mix)  {
    mix.styles([
        
        //'bootstrap.min.css',

        'style.css',

        'smart-forms.css',
'smart-addons.css',

'smart-themes/purple.css',

'smart-themes/yellow.css',

'smart-themes/red.css',

'smart-themes/green.css',

'smart-themes/blue.css',

'smart-themes/black.css',

'font-awesome.min.css'
], 'public/css/all.css')
    .styles([
        
        'bootstrap.min.css',

        'style.css',

        'smart-forms.css',
'smart-addons.css',

'smart-themes/purple.css',

'smart-themes/yellow.css',

'smart-themes/red.css',

'smart-themes/green.css',

'smart-themes/blue.css',

'smart-themes/black.css',

'font-awesome.min.css'
], 'public/css/allwithbootstrap.css')
       .scripts(['jquery-1.9.1.min.js',
           'jquery.form.min.js',
           'jquery.validate.min.js',  //validatebill.min.js(?)
           'additional-methods.min.js',
           'smartforms-modal.min.js',
           'smart-form.js',
           'steps.bill.js'
       ], 'public/js/all.js')
        .scriptsIn('resources/assets/js/buttons', 'public/js/buttons.js')
        .scripts(['basicjs/jquery-1.9.1.min.js',
            'basicjs/jquery.form.min.js',
            'basicjs/jquery.validatebill.min.js',
            'basicjs/additional-methods.min.js',
            'basicjs/bootstrap.min.js',
            'basicjs/custom.js',
            'basicjs/jquery.formShowHide.min.js',
            'basicjs/jquery.maskedinput.js',
            'basicjs/jquery.maskMoney.js',
            'basicjs/jquery.placeholder.min.js',
            'basicjs/jquery-cloneya-bill.js',
            'basicjs/jquery-ui.min.js',
            'basicjs/jquery-ui-custom.min.js',
            'basicjs/jquery-ui-touch-punch.min.js',
            'basicjs/smartforms-modal.min.js',
            'basicjs/steps.bill.js',
            'smartforms/smart-form.js',
            'smartforms/autosave.js'
        ], 'public/js/basic.js')
        .version(['css/all.css', 'css/allwithbootstrap.css', 'js/all.js', 'js/buttons.js', 'js/basic.js'])
        mix.browserSync({
    browser: "google chrome",
        proxy: 'http://glowing-umbrella-new.test',
    port: 8000
})
        


    ;;
});
