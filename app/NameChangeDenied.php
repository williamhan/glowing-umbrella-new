<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NameChangeDenied extends Model
{
    //
    protected $table = 'namechange_denied';
}
