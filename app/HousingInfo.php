<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HousingInfo extends Model
{
    //
    protected $table = 'housinginfo';

    public function schoolinfo()
    {
        return $this->belongsTo('App\SchoolInfo', 'sch_id', 'sch_id');
    }
}