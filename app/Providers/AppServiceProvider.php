<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use Session;
use App\AgentApps;

use App\BillTestModel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('features.smartwrapnoecho', function ($view) {
            $view->with('appdata', AgentApps::where('agent_fullname', '=', Session::get('agent'))->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
