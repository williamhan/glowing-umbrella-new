<?php
if (! function_exists('get_rollbar_user')) {
    function get_rollbar_user() {
    if (Auth::check()) {
        $user = Auth::user();
        return array(
            'id' => $user->sysuserid, // required - value is a string
            'username' => $user->name, // required - value is a string
            'email' => $user->email, // optional - value is a string
        );
    }
    return null;
}
}