<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use XMLWriter;

class SiteMapController extends Controller
{
    public function index()
    {
        $pages = Page::all();

        return response()->view('xml.sitemap', compact('pages'))->header('Content-Type', 'text/xml');
    }
}
