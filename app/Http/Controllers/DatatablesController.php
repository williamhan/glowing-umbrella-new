<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\SchoolInfo;
use App\User;
use Yajra\Datatables\Datatables;

class DatatablesController extends Controller
{
    /**
     * Displays datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        return view('vendor.datatables.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $users = Datatables::of(User::all())->make(true);

        {{ var_dump(json_decode($users, true)); }}

        return Datatables::of(User::all())->make(true);
    }
}
