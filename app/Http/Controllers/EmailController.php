<?php

namespace App\Http\Controllers;

use Request;

use ET_Client;

class EmailController extends Controller
{
    

    public function emailtest()
    {
        $DataExtensionNameForTesting = "Welcome_Email_List";


    $myclient = new \ET_Client();
        
    //DataExtension Testing
    // Add a row to a DataExtension 
    print_r("Add a row to a DataExtension  \n");
    $postDRRow = new \ET_DataExtension_Row();
    $postDRRow->authStub = $myclient;
    $postDRRow->props = array("Email_Address" => "BilliamTest@BilliamTest.Test", "App_ID" => "123456");
    $postDRRow->Name = $DataExtensionNameForTesting;    
    $postResult = $postDRRow->post();
    // print_r('Post Status: '.($postResult->status ? 'true' : 'false')."\n");
    // print 'Code: '.$postResult->code."\n";
    // print 'Message: '.$postResult->message."\n";    
    // print 'Result Count: '.count($postResult->results)."\n";
    // print 'Results: '."\n";
    // print_r($postResult->results);
    }



    public function welcomeemail()
    {

        $DataExtensionName = "Welcome_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function pastempemail()
    {

        $DataExtensionName = "psp_pev_email_list";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function travelemail()
    {

        $DataExtensionName = "Travel_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "GH_Confirmation" => $_POST['gh_conf'], "GH_Station" => $_POST['departure_station'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function leftmessageemail()
    {

        $DataExtensionName = "Left_Message_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function confirmationemail()
    {

        $DataExtensionName = "Confirmation_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function threepapprovalemail()
    {

        $DataExtensionName = "ThirdParty_Approval_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function wareleaseemail()
    {

        $DataExtensionName = "Releases_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Release_State" => "WA", "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function vareleaseemail()
    {

        $DataExtensionName = "Releases_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Release_State" => "VA", "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function nhreleaseemail()
    {

        $DataExtensionName = "Releases_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Release_State" => "NH", "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function namechange()
    {

        $DataExtensionName = "NameChange_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }
}
