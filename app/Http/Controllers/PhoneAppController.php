<?php

namespace App\Http\Controllers;

use App\States;
use App\FrapSchools;
use App\FrapRecruiters;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class PhoneAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds
        $stateslist = States::orderBy('state_id', 'asc')->get();
        $schoollist = FrapSchools::orderBy('id', 'asc')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('phoneapp.index')
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoollist = Schools::pluck('school_name', 'id');
        return View::make('feesinfo.create')
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feesinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $FeesInfo = new FeesInfo;
            $FeesInfo->sch_id       = Input::get('sch_id');
            $FeesInfo->fee_state      = Input::get('fee_state');
            $FeesInfo->fee_type = Input::get('fee_type');
            $FeesInfo->fee_amount = Input::get('fee_amount');
            $FeesInfo->fee_is_exp = Input::get('fee_is_exp');
            $FeesInfo->fee_is_sr = Input::get('fee_is_sr');
            $FeesInfo->fee_is_track_two = Input::get('fee_is_track_two');
            $FeesInfo->fee_is_sc = Input::get('fee_is_sc');
            $FeesInfo->save();

            // redirect
            Session::flash('message', 'Successfully created transportation info!');
            return Redirect::to('feesinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $FeesInfo = FeesInfo::where('sch_id', '=', $id)
            ->get();

        // show the view and pass the nerd to it
        return View::make('feesinfo.show')
            ->with('feesinfo', $FeesInfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoollist = Schools::pluck('school_name', 'id');
        $FeesInfo = FeesInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('feesinfo.edit')
            ->with('feesinfo', $FeesInfo)
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feesinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $FeesInfo = FeesInfo::find($id);
            $FeesInfo->sch_id       = Input::get('sch_id');
            $FeesInfo->fee_state      = Input::get('fee_state');
            $FeesInfo->fee_type = Input::get('fee_type');
            $FeesInfo->fee_amount = Input::get('fee_amount');
            $FeesInfo->fee_is_exp = Input::get('fee_is_exp');
            $FeesInfo->fee_is_sr = Input::get('fee_is_sr');
            $FeesInfo->fee_is_track_two = Input::get('fee_is_track_two');
            $FeesInfo->fee_is_sc = Input::get('fee_is_sc');
            $FeesInfo->save();

            // redirect
            Session::flash('message', 'Successfully updated school!');
            return Redirect::to('feesinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $FeesInfo = FeesInfo::find($id);
        $FeesInfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('feesinfo');
    }
}
