<?php

namespace App\Http\Controllers;

use Request;

use Illuminate\Database\Eloquent\Model as App;

use Illuminate\Foundation\Auth\User as Auth;







class PageController extends Controller
{

    public function show($slug = 'home')
    {
        // assumed page skug is 'about'
        $permission = 'page_' . $slug;
        if(Auth::user()->can($permission)) {
        $page = page::whereSlug('home')->get();
        return View::make('pages.index')->with('page', $page);
    }
}

}