<?php

namespace App\Http\Controllers;

use App\Nerd;
use App\TransportationInfo;
use App\Schools;
use App\ScheduleInfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ScheduleInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds
        $schoollist = Schools::pluck('school_name', 'id');
        $scheduleinfo = ScheduleInfo::with('schoolinfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('scheduleinfo.index')
            ->with('scheduleinfo', $scheduleinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $schoollist = Schools::pluck('school_name', 'id');
        return View::make('scheduleinfo.create')
            ->with('schoollist', $schoollist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('scheduleinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $scheduleinfo = new ScheduleInfo;
            $scheduleinfo->sch_id       = Input::get('sch_id');
            $scheduleinfo->schedule_sun      = Input::get('schedule_sun');
            $scheduleinfo->schedule_mon = Input::get('schedule_mon');
            $scheduleinfo->schedule_tues = Input::get('schedule_tues');
            $scheduleinfo->schedule_wed = Input::get('schedule_wed');
            $scheduleinfo->schedule_thurs = Input::get('schedule_thurs');
            $scheduleinfo->schedule_fri = Input::get('schedule_fri');
            $scheduleinfo->schedule_sat = Input::get('schedule_sat');
            $scheduleinfo->schedule_sun_start_time      = Input::get('schedule_sun_start_time');
            $scheduleinfo->schedule_mon_start_time = Input::get('schedule_mon_start_time');
            $scheduleinfo->schedule_tues_start_time = Input::get('schedule_tues_start_time');
            $scheduleinfo->schedule_wed_start_time = Input::get('schedule_wed_start_time');
            $scheduleinfo->schedule_thurs_start_time = Input::get('schedule_thurs_start_time');
            $scheduleinfo->schedule_fri_start_time = Input::get('schedule_fri_start_time');
            $scheduleinfo->schedule_sat_start_time = Input::get('schedule_sat_start_time');
            $scheduleinfo->schedule_sun_end_time      = Input::get('schedule_sun_end_time');
            $scheduleinfo->schedule_mon_end_time = Input::get('schedule_mon_end_time');
            $scheduleinfo->schedule_tues_end_time = Input::get('schedule_tues_end_time');
            $scheduleinfo->schedule_wed_end_time = Input::get('schedule_wed_end_time');
            $scheduleinfo->schedule_thurs_end_time = Input::get('schedule_thurs_end_time');
            $scheduleinfo->schedule_fri_end_time = Input::get('schedule_fri_end_time');
            $scheduleinfo->schedule_sat_end_time = Input::get('schedule_sat_end_time');
            $scheduleinfo->schedule_is_exp = Input::get('schedule_is_exp');
            $scheduleinfo->schedule_is_sr = Input::get('schedule_is_sr');
            $scheduleinfo->schedule_is_track_two = Input::get('schedule_is_track_two');
            $scheduleinfo->schedule_is_sc = Input::get('schedule_is_sc');
            $scheduleinfo->save();

            // redirect
            Session::flash('message', 'Successfully created schedule info!');
            return Redirect::to('scheduleinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $scheduleinfo = ScheduleInfo::where('sch_id', '=', $id)
            ->get();

        // show the view and pass the nerd to it
        return View::make('scheduleinfo.show')
            ->with('scheduleinfo', $scheduleinfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $schoollist = Schools::pluck('school_name', 'id');
        $scheduleinfo = ScheduleInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('scheduleinfo.edit')
            ->with('scheduleinfo', $scheduleinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('scheduleinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $scheduleinfo = ScheduleInfo::find($id);
            $scheduleinfo->sch_id       = Input::get('sch_id');
            $scheduleinfo->schedule_sun      = Input::get('schedule_sun');
            $scheduleinfo->schedule_mon = Input::get('schedule_mon');
            $scheduleinfo->schedule_tues = Input::get('schedule_tues');
            $scheduleinfo->schedule_wed = Input::get('schedule_wed');
            $scheduleinfo->schedule_thurs = Input::get('schedule_thurs');
            $scheduleinfo->schedule_fri = Input::get('schedule_fri');
            $scheduleinfo->schedule_sat = Input::get('schedule_sat');
            $scheduleinfo->schedule_sun_start_time      = Input::get('schedule_sun_start_time');
            $scheduleinfo->schedule_mon_start_time = Input::get('schedule_mon_start_time');
            $scheduleinfo->schedule_tues_start_time = Input::get('schedule_tues_start_time');
            $scheduleinfo->schedule_wed_start_time = Input::get('schedule_wed_start_time');
            $scheduleinfo->schedule_thurs_start_time = Input::get('schedule_thurs_start_time');
            $scheduleinfo->schedule_fri_start_time = Input::get('schedule_fri_start_time');
            $scheduleinfo->schedule_sat_start_time = Input::get('schedule_sat_start_time');
            $scheduleinfo->schedule_sun_end_time      = Input::get('schedule_sun_end_time');
            $scheduleinfo->schedule_mon_end_time = Input::get('schedule_mon_end_time');
            $scheduleinfo->schedule_tues_end_time = Input::get('schedule_tues_end_time');
            $scheduleinfo->schedule_wed_end_time = Input::get('schedule_wed_end_time');
            $scheduleinfo->schedule_thurs_end_time = Input::get('schedule_thurs_end_time');
            $scheduleinfo->schedule_fri_end_time = Input::get('schedule_fri_end_time');
            $scheduleinfo->schedule_sat_end_time = Input::get('schedule_sat_end_time');
            $scheduleinfo->schedule_is_exp = Input::get('schedule_is_exp');
            $scheduleinfo->schedule_is_sr = Input::get('schedule_is_sr');
            $scheduleinfo->schedule_is_track_two = Input::get('schedule_is_track_two');
            $scheduleinfo->schedule_is_sc = Input::get('schedule_is_sc');
            $scheduleinfo->save();

            // redirect
            Session::flash('message', 'Successfully updated schedule info!');
            return Redirect::to('scheduleinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $scheduleinfo = ScheduleInfo::find($id);
        $scheduleinfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the schedule info!');
        return Redirect::to('scheduleinfo');
    }
}
