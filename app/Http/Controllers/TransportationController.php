<?php

namespace App\Http\Controllers;

use App\TransportationInfo;
use App\Schools;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class TransportationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds
        $schoollist = Schools::pluck('school_name', 'id');
        $transportationinfo = TransportationInfo::with('schoolinfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('transportationinfo.index')
            ->with('transportationinfo', $transportationinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $schoollist = Schools::pluck('school_name', 'id');
        return View::make('transportationinfo.create')
            ->with('schoollist', $schoollist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('transportationinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $transportationinfo = new TransportationInfo;
            $transportationinfo->sch_id       = Input::get('sch_id');
            $transportationinfo->shuttle_type      = Input::get('shuttle_type');
            $transportationinfo->shuttle_day_of_week = Input::get('shuttle_day_of_week');
            $transportationinfo->shuttle_start_time = Input::get('shuttle_start_time');
            $transportationinfo->shuttle_end_time = Input::get('shuttle_end_time');
            $transportationinfo->shuttle_runs_after_hours = Input::get('shuttle_runs_after_hours');
            $transportationinfo->shuttle_cutoff = Input::get('shuttle_cutoff');
            $transportationinfo->shuttle_after_cutoff = Input::get('shuttle_after_cutoff');
            $transportationinfo->save();

            // redirect
            Session::flash('message', 'Successfully created transportation info!');
            return Redirect::to('transportationinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $transportationinfo = TransportationInfo::where('sch_id', '=', $id)
            ->get();

        // show the view and pass the nerd to it
        return View::make('transportationinfo.show')
            ->with('transportationinfo', $transportationinfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $schoollist = Schools::pluck('school_name', 'id');
        $transportationinfo = TransportationInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('transportationinfo.edit')
            ->with('transportationinfo', $transportationinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('transportationinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $transportationinfo = TransportationInfo::find($id);
            $transportationinfo->sch_id       = Input::get('sch_id');
            $transportationinfo->shuttle_type      = Input::get('shuttle_type');
            $transportationinfo->shuttle_day_of_week = Input::get('shuttle_day_of_week');
            $transportationinfo->shuttle_start_time = Input::get('shuttle_start_time');
            $transportationinfo->shuttle_end_time = Input::get('shuttle_end_time');
            $transportationinfo->shuttle_runs_after_hours = Input::get('shuttle_runs_after_hours');
            $transportationinfo->shuttle_cutoff = Input::get('shuttle_cutoff');
            $transportationinfo->shuttle_after_cutoff = Input::get('shuttle_after_cutoff');
            $transportationinfo->save();

            // redirect
            Session::flash('message', 'Successfully updated school!');
            return Redirect::to('transportationinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $transportationinfo = SchoolInfo::find($id);
        $transportationinfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('transportationinfo');
    }
}
