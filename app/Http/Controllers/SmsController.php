<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Twilio\Rest\Client;

class SmsController extends Controller
{
    public function welcomesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Thank you for speaking with me today and once again welcome to the team! Please check your email for your Welcome Documentation and travel information. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function pastempsms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Looks like we need your signature on some forms before we can continue. Please check your email for these and get them back to me asap. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function travelsms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Thank you for speaking with me today. Please check your email for your Travel Information. Remember you need to call me once you have your bus ticket printed to confirm your start date. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function leftmessagesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Good afternoon. I am trying to reach you about the application you submitted for the truck driving job with C.R. England. Please call me back as soon as you can. Thanks! - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function confirmationsms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Good afternoon. I just wanted to follow up and make sure that you are good to go for school this upcoming Monday. Will you please give me a call and confirm that you will be attending. Thanks! - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function lastchancesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Are you still planning on starting Monday?  If you are no longer interested please let me know so that I can cancel your reservations so we don't get charged. Your response is necessary to keep your application active. Please call immediately. Thanks! - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function wareleasesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function vareleasesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function nhreleasesms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => "Looks like we need some release forms signed before we can run your MVR. Please check your email for these and get them back to me asap to continue with the hiring process. - " . $agentname . " - " . $agentphone . " ",
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }

    public function diysms()
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $cleanphone = preg_replace('/\D+/', '', $_POST['mobilephone']);

        $agentname = $_POST['agent'];
        $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

        $message = $_POST['Sms'];

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                $cleanphone,
                [
                    "body" => $message,
                    "messagingServiceSid" => $twilioMsid
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

        } catch (TwilioException $e) {


        }
    }
}
