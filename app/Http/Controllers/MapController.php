<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

class MapController extends Controller
{
    public function map()
    {

        // Draw a map
        Mapper::map(40.59726025535419, 80.02503488467874, ['marker' => true]);



        return view('testinggm.mapper');

    }
}
