<?php

namespace App\Http\Controllers;

use App\User;
use Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    public function index()
    {
        // get all the nerds
        $userinfo = User::all()->sortBy('name');

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('userinfo.index')
            ->with('userinfo', $userinfo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        return View::make('userinfo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'email'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('userinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $userinfo = new User;
            $userinfo->name       = Input::get('name');
            $userinfo->firstname      = Input::get('firstname');
            $userinfo->lastname = Input::get('lastname');
            $userinfo->email = Input::get('email');
            $userinfo->parentuseridname = Input::get('parentuseridname');
            $userinfo->deskphone = Input::get('deskphone');
            $userinfo->title = Input::get('title');
            $userinfo->user_ref_id = Input::get('user_ref_id');
            $userinfo->save();

            // redirect
            Session::flash('message', 'Successfully created user info!');
            return Redirect::to('userinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $userinfo = User::find($id);

        // show the view and pass the nerd to it
        return View::make('userinfo.show')
            ->with('userinfo', $userinfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $userinfo = User::find($id);

        // show the edit form and pass the nerd
        return View::make('userinfo.edit')
            ->with('userinfo', $userinfo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'email'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('userinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $userinfo = User::find($id);
            $userinfo->name       = Input::get('name');
            $userinfo->firstname      = Input::get('firstname');
            $userinfo->lastname = Input::get('lastname');
            $userinfo->email = Input::get('email');
            $userinfo->parentuseridname = Input::get('parentuseridname');
            $userinfo->deskphone = Input::get('deskphone');
            $userinfo->title = Input::get('title');
            $userinfo->user_ref_id = Input::get('user_ref_id');
            $userinfo->save();

            // redirect
            Session::flash('message', 'Successfully updated user!');
            return Redirect::to('userinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $userinfo = User::find($id);
        $userinfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the user!');
        return Redirect::to('userinfo');
    }
}
