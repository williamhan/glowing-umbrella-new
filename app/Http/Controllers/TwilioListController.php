<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TwilioListController extends Controller
{
    public $client;
	//  $account_sid = env('TWILIO_ACCOUNT_SID');
    //  $auth_token = env('TWILIO_AUTH_TOKEN');
    //  $twilioMsid = env('TWILIO_MSID');

    public function __construct()
	{
        $account_sid = env('TWILIO_ACCOUNT_SID');
     $auth_token = env('TWILIO_AUTH_TOKEN');
     $twilioMsid = env('TWILIO_MSID');
        
        $this->client = new Client($this->account_sid, $this->auth_token);
    }
    
    public function getibsms($datesentstart, $datesentend)
	{
		
		//$params = array('direction' => 'inbound', 'dateSentAfter' => '2017-08-01');
		$smsmessages = array();
		$smsmessages = $this->client
			->messages
			//->read($params);
    		 ->read(
         		array(
         			"direction" => "inbound",
         			"dateSentAfter" => date($datesentstart),
         			"dateSentBefore" => date($datesentend)
    		 	)
    		);



        return $smsmessages;
	}
}
