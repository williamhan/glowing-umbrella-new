<?php

namespace App\Http\Controllers;

use App\Nerd;
use App\TransportationInfo;
use App\Schools;
use App\ScheduleInfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SchoolModalController extends Controller
{
    public function index()
    {
        // get all the nerds
        $schoolinfo = SchoolInfo::with('scheduleinfo', 'transportationinfo', 'feesinfo', 'docsinfo', 'housinginfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('modals.schoolmodals.slc')
            ->with('schoolinfo', $schoolinfo);
    }
}
