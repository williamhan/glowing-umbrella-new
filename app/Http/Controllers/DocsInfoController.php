<?php

namespace App\Http\Controllers;
use App\Nerd;
use App\TransportationInfo;
use App\Schools;
use App\DocsInfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class DocsInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds
        $schoollist = Schools::pluck('school_name', 'id');
        $docsinfo = DocsInfo::with('schoolinfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('docsinfo.index')
            ->with('docsinfo', $docsinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $schoollist = Schools::pluck('school_name', 'id');
        return View::make('docsinfo.create')
            ->with('schoollist', $schoollist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('docsinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $docsinfo = new DocsInfo;
            $docsinfo->sch_id       = Input::get('sch_id');
            $docsinfo->is_alt      = Input::get('is_alt');
            $docsinfo->doc_name = Input::get('doc_name');
            $docsinfo->doc_is_required = Input::get('doc_is_required');
            $docsinfo->doc_has_alt = Input::get('doc_has_alt');
            $docsinfo->alt_doc_id_one = Input::get('alt_doc_id_one');
            $docsinfo->alt_doc_id_two = Input::get('alt_doc_id_two');
            $docsinfo->alt_doc_id_three = Input::get('alt_doc_id_three');
            $docsinfo->doc_temp_ok = Input::get('doc_temp_ok');
            $docsinfo->doc_laminated_ok = Input::get('doc_laminated_ok');
            $docsinfo->doc_receipt_ok = Input::get('doc_receipt_ok');
            $docsinfo->doc_is_exp = Input::get('doc_is_exp');
            $docsinfo->doc_is_sr = Input::get('doc_is_sr');
            $docsinfo->doc_is_track_two = Input::get('doc_is_track_two');
            $docsinfo->doc_is_sc = Input::get('doc_is_sc');
            $docsinfo->save();

            // redirect
            Session::flash('message', 'Successfully created docs info!');
            return Redirect::to('docsinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $docsinfo = DocsInfo::where('sch_id', '=', $id)
            ->get();

        // show the view and pass the nerd to it
        return View::make('docsinfo.show')
            ->with('docsinfo', $docsinfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $schoollist = Schools::pluck('school_name', 'id');
        $docsinfo = DocsInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('docsinfo.edit')
            ->with('docsinfo', $docsinfo)
            ->with('schoollist', $schoollist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('docsinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $docsinfo = DocsInfo::find($id);
            $docsinfo->sch_id       = Input::get('sch_id');
            $docsinfo->is_alt      = Input::get('is_alt');
            $docsinfo->doc_name = Input::get('doc_name');
            $docsinfo->doc_is_required = Input::get('doc_is_required');
            $docsinfo->doc_has_alt = Input::get('doc_has_alt');
            $docsinfo->alt_doc_id_one = Input::get('alt_doc_id_one');
            $docsinfo->alt_doc_id_two = Input::get('alt_doc_id_two');
            $docsinfo->alt_doc_id_three = Input::get('alt_doc_id_three');
            $docsinfo->doc_temp_ok = Input::get('doc_temp_ok');
            $docsinfo->doc_laminated_ok = Input::get('doc_laminated_ok');
            $docsinfo->doc_receipt_ok = Input::get('doc_receipt_ok');
            $docsinfo->doc_is_exp = Input::get('doc_is_exp');
            $docsinfo->doc_is_sr = Input::get('doc_is_sr');
            $docsinfo->doc_is_track_two = Input::get('doc_is_track_two');
            $docsinfo->doc_is_sc = Input::get('doc_is_sc');
            $docsinfo->save();

            // redirect
            Session::flash('message', 'Successfully updated docs!');
            return Redirect::to('docsinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $docsinfo = DocsInfo::find($id);
        $docsinfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('docsinfo');
    }
}
