<?php

namespace App\Http\Controllers;
use App\HousingInfo;
use App\Nerd;
use App\SchoolInfo;
use App\States;
use App\TransportationInfo;
use App\Schools;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class HousingInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoollist = Schools::pluck('school_name', 'id');
        $housinginfo = HousingInfo::with('schoolinfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('housinginfo.index')
            ->with('housinginfo', $housinginfo)
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoollist = Schools::pluck('school_name', 'id');
        return View::make('housinginfo.create')
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('housinginfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $housinginfo = new HousingInfo;
            $housinginfo->sch_id       = Input::get('sch_id');
            $housinginfo->housing_provided      = Input::get('housing_provided');
            $housinginfo->housing_id = Input::get('housing_id');
            $housinginfo->housing_type = Input::get('housing_type');
            $housinginfo->housing_name = Input::get('housing_name');
            $housinginfo->housing_address = Input::get('housing_address');
            $housinginfo->housing_city = Input::get('housing_city');
            $housinginfo->housing_state = Input::get('housing_state');
            $housinginfo->housing_zip = Input::get('housing_zip');
            $housinginfo->housing_phone = Input::get('housing_phone');
            $housinginfo->housing_occupancy = Input::get('housing_occupancy');
            $housinginfo->housing_has_shuttle = Input::get('housing_has_shuttle');
            $housinginfo->housing_shuttle_id = Input::get('housing_shuttle_id');
            $housinginfo->housing_is_exp = Input::get('housing_is_exp');
            $housinginfo->housing_is_sr = Input::get('housing_is_sr');
            $housinginfo->housing_is_track_two = Input::get('housing_is_track_two');
            $housinginfo->housing_is_sc = Input::get('housing_is_sc');
            $housinginfo->save();

            // redirect
            Session::flash('message', 'Successfully created transportation info!');
            return Redirect::to('housinginfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $housinginfo = HousingInfo::where('sch_id', '=', $id)
            ->get();

        // show the view and pass the nerd to it
        return View::make('housinginfo.show')
            ->with('housinginfo', $housinginfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoollist = Schools::pluck('school_name', 'id');
        $housinginfo = HousingInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('housinginfo.edit')
            ->with('housinginfo', $housinginfo)
            ->with('stateslist', $stateslist)
            ->with('schoollist', $schoollist);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_id'       => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('housinginfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $housinginfo = HousingInfo::find($id);
            $housinginfo->sch_id       = Input::get('sch_id');
            $housinginfo->housing_provided      = Input::get('housing_provided');
            $housinginfo->housing_id = Input::get('housing_id');
            $housinginfo->housing_type = Input::get('housing_type');
            $housinginfo->housing_name = Input::get('housing_name');
            $housinginfo->housing_address = Input::get('housing_address');
            $housinginfo->housing_city = Input::get('housing_city');
            $housinginfo->housing_state = Input::get('housing_state');
            $housinginfo->housing_zip = Input::get('housing_zip');
            $housinginfo->housing_phone = Input::get('housing_phone');
            $housinginfo->housing_occupancy = Input::get('housing_occupancy');
            $housinginfo->housing_has_shuttle = Input::get('housing_has_shuttle');
            $housinginfo->housing_shuttle_id = Input::get('housing_shuttle_id');
            $housinginfo->housing_is_exp = Input::get('housing_is_exp');
            $housinginfo->housing_is_sr = Input::get('housing_is_sr');
            $housinginfo->housing_is_track_two = Input::get('housing_is_track_two');
            $housinginfo->housing_is_sc = Input::get('housing_is_sc');
            $housinginfo->save();

            // redirect
            Session::flash('message', 'Successfully updated school!');
            return Redirect::to('housinginfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $housinginfo = SchoolInfo::find($id);
        $housinginfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('housinginfo');
    }
}
