<?php

namespace App\Http\Controllers\HRight;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WSSoapClient;
use SoapClient;
use SoapVar;
use SoapHeader;
use SoapFault;
use SoapParam;
use Soap;
use Illuminate\Support\Facades\View;
use App\HireRight;

class EhfController extends Controller
{
    public function index()
    {



        $passmvrexp='17+Bm!2.)J_$j(949';
        $createdmvrexp = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrexp = mt_rand();
        $passdigestmvrexp = base64_encode(pack('H*', sha1(pack('H*', $noncemvrexp) . pack('a*', $createdmvrexp) . pack('a*', $passmvrexp))));




        $hirerightclientmvrexp = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));


        $authmvrexp = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrexp .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrexp .= '				<wsse:Username>cre_apilogin</wsse:Username>';
        $authmvrexp .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrexp.'</wsse:Password>';
        $authmvrexp .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrexp)).'</wsse:Nonce>';
        $authmvrexp .= '				<wsu:Created>' . $createdmvrexp . '</wsu:Created>';
        $authmvrexp .= '			</wsse:UsernameToken>';
        $authmvrexp .= '		</wsse:Security>';
        $auth_blockmvrexp = new SoapVar( $authmvrexp, XSD_ANYXML, NULL, NULL, NULL, NULL );


        $headermvrexp = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrexp );
        $hirerightclientmvrexp->__setSoapHeaders( $headermvrexp );
        $hirerightclientmvrexp->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed



        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $HRObject = $app->make('stdClass');

        $tmpObj = $app->make('stdClass');

        $srvcObj = $app->make('stdClass');

        $flxfieldObj = $app->make('stdClass');

        $payloadObj = $app->make('stdClass');

        $bgcheckObj = $app->make('stdClass');

        $phoneoneObj = $app->make('stdClass');

        $phonetwoObj = $app->make('stdClass');

        $addresslineObj = $app->make('stdClass');


        $servicedata = '<ns1:Service id="MVR Express"/>';



        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

        $licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

        $srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

        $flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

        $addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

        $phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('cdliserror@drivecretest.com', XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
        $personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

        $bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

        $bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

        $tmpObj->AccountId = new SoapVar("WS_CRENG", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->PackageId = new SoapVar("307774", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);


        try
        {
            $retmvrexp = $hirerightclientmvrexp->__SoapCall('Create', array($CreateParms));




            try
            {

                $passmvrstd='17+Bm!2.)J_$j(949';
                $createdmvrstd = gmdate('Y-m-d\TH:i:s\Z');
                $noncemvrstd = mt_rand();
                $passdigestmvrstd = base64_encode(pack('H*', sha1(pack('H*', $noncemvrstd) . pack('a*', $createdmvrstd) . pack('a*', $passmvrstd))));

                $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));

                $authmvrstd = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
                $authmvrstd .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
                $authmvrstd .= '				<wsse:Username>cre_apilogin</wsse:Username>';
                $authmvrstd .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrstd.'</wsse:Password>';
                $authmvrstd .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrstd)).'</wsse:Nonce>';
                $authmvrstd .= '				<wsu:Created>' . $createdmvrstd . '</wsu:Created>';
                $authmvrstd .= '			</wsse:UsernameToken>';
                $authmvrstd .= '		</wsse:Security>';
                $auth_blockmvrstd = new SoapVar( $authmvrstd, XSD_ANYXML, NULL, NULL, NULL, NULL );

                $headermvrstd = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrstd );
                $hirerightclientmvrstd->__setSoapHeaders( $headermvrstd );
                $hirerightclientmvrstd->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed




                $urlObj->AccountId = new SoapVar("WS_CRENG", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->RegId = new SoapVar($retmvrexp->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ObjId = new SoapVar($retmvrexp->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

                $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

                $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

                $url = $hirerightclientmvrstd->__SoapCall('GenerateWebLink', array($WebLinkParms));
            }
            catch (Exception $e)
            {

            }
        }
        catch (Exception $e)
        {
        }

        $resultid = $retmvrexp->Result->Id;
        $regid = $retmvrexp->Result->RegId;

        $runrecord = new HireRight;
        $runrecord->app_id = $_POST['app_id'];
        $runrecord->app_id_guid = $_POST['app_id_guid'];
        $runrecord->agent = $_POST['agent'];
        $runrecord->agent_guid = $_POST['agent_id'];
        $runrecord->object_id = $resultid;
        $runrecord->reg_id = $regid;
        $runrecord->service_type = 'ehf';
        $runrecord->save();




        $urlret = $url->URL;

        return $urlret;


    }

    public function review()
    {

        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $resultid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
            ['service_type', '=', 'ehf'],
        ])
            ->latest()
            ->select('object_id')
            ->get();

        $requestid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
            ['service_type', '=', 'ehf'],
        ])
            ->latest()
            ->select('reg_id')
            ->get();


        try
        {

            $pass='17+Bm!2.)J_$j(949';
            $created = gmdate('Y-m-d\TH:i:s\Z');
            $nonce = mt_rand();
            $passdigest = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $created) . pack('a*', $pass))));

            $hirerightclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));

            $auth = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
            $auth .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
            $auth .= '				<wsse:Username>cre_apilogin</wsse:Username>';
            $auth .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigest.'</wsse:Password>';
            $auth .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $nonce)).'</wsse:Nonce>';
            $auth .= '				<wsu:Created>' . $created . '</wsu:Created>';
            $auth .= '			</wsse:UsernameToken>';
            $auth .= '		</wsse:Security>';
            $auth_block = new SoapVar( $auth, XSD_ANYXML, NULL, NULL, NULL, NULL );

            $header = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_block );
            $hirerightclient->__setSoapHeaders( $header );
            $hirerightclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed




            $urlObj->AccountId = new SoapVar("WS_CRENG", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->RegId = new SoapVar($requestid, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->ObjId = new SoapVar($resultid, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
            $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

            $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

            $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

            $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
        }
        catch (Exception $e)
        {

        }

        $urlret = $url->URL;

        return $urlret;
    }
}
