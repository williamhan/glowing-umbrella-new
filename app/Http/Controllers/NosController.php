<?php

namespace App\Http\Controllers;

use Request;

use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Wsdl2PhpGenerator;

use Carbon\Carbon;
use App\Greyhound;
use App\Users;
use App\AgentApps;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\View;


class NosController extends Controller
{
	
	public function vdump($data)
	{
  		echo "<pre>"; var_dump($data); echo "</pre>";
	}

	public function prdump($data)
	{
  		echo "<pre>"; print_r($data); echo "</pre>";
	}

	public function cmp($a, $b) {
		if ($a->dateCreated == $b->dateCreated) {
		  return 0;
		}
	  
		return ($a->dateCreated < $b->dateCreated) ? -1 : 1;
	  }
	
	
	public function retreiveobsms($obmobilephone)
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $obcleanphone = preg_replace('/\D+/', '', $obmobilephone);

        // $agentname = $_POST['agent'];
        // $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

		$client = new Client($accountSid, $authToken);
		
		$obsmsmessages = array();
		$obsmsmessages = $client
			->messages
			//->read($params);
    		 ->read(
         		array(
         			"to" => $obcleanphone
    		 	)
			);
			
			// foreach ($smsmessages as $message) {
			// 	echo $message->body;
			// }



        return $obsmsmessages;

        
	}
	
	public function retreiveibsms($ibmobilephone)
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioMsid = env('TWILIO_MSID');

        $ibcleanphone = preg_replace('/\D+/', '', $ibmobilephone);

        // $agentname = $_POST['agent'];
        // $agentphone = preg_replace('/\D+/', '', $_POST['agent_phone']);

		$client = new Client($accountSid, $authToken);
		
		$ibsmsmessages = array();
		$ibsmsmessages = $client
			->messages
			//->read($params);
    		 ->read(
         		array(
         			"from" => $ibcleanphone
    		 	)
			);
			
			// foreach ($smsmessages as $message) {
			// 	echo $message->body;
			// }



        return $ibsmsmessages;

        
	}
	



	public function scribeapplist()
    {
		
		$agentname = Request::only(['agent']);

		
		//----- URL For Direct Write to NOS -----//
		
		$url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/4530?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";

		//$url = "https://699d1fphyyjd.runscope.net/";

		$jsonData = array(
			'agent' => $agentname
			);
		
		$jsonDataEncoded = json_encode($agentname);
		
		$curl = curl_init();
		
		$opts = array(
				            CURLOPT_URL             => $url,
				            CURLOPT_RETURNTRANSFER  => true,
				            CURLOPT_CUSTOMREQUEST   => 'POST',
				            CURLOPT_POST            => 1,
				            CURLOPT_POSTFIELDS      => $jsonDataEncoded
				        );
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
		curl_setopt_array($curl, $opts);
		$result = curl_exec($curl);
		curl_close($curl);
		
		$response = json_decode($result, true);
		$appdata = $response;
		return $appdata;
		//return view('example',compact('maindata', 'allsmsdata', 'appdata'));
	}
	
	
	
	
	public function index()
	{
		
		//Request::flashOnly('appdata');

		$data = Request::only(['appid']);

		//$agentnamedata = Request::only(['agent']);
		// 		var_dump($data);
		
		if (!empty($data))
		{
			$appid = $data;
		}
		
		elseif (empty($data))
		{
			$appid = "2193661";
		}
		//$		appid = $_POST['appid'];
		// 		-----OLD ENDPOINT -----
				        //$		url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/1678?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
		
		// 		-----NEW ENDPOINT -----
				        $url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/5036?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
		
		//$		url = "https://699d1fphyyjd.runscope.net/";
		
		$jsonData = array(
				'appid' => $appid
				);
		
		
		
		$jsonDataEncoded = json_encode($appid);
		
		
		// 		echo "jsonData \n";
		// 		var_dump($jsonData);
		// 		echo "jsonDataEncoded \n";
		// 		var_dump($jsonDataEncoded);
		
		
		// 		Initialize curl
				        $curl = curl_init();
		
		// 		Configure curl options
				        $opts = array(
				            CURLOPT_URL             => $url,
				            CURLOPT_RETURNTRANSFER  => true,
				            CURLOPT_CUSTOMREQUEST   => 'POST',
				            CURLOPT_POST            => 1,
				            CURLOPT_POSTFIELDS      => $jsonDataEncoded
				        );
		
		// 		Set curl options
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
		curl_setopt_array($curl, $opts);
		
		// 		Get the results
		$result = curl_exec($curl);
		
		// 		Close resource
		curl_close($curl);
		
		$response = json_decode($result, true);
		
		//Mapper::map($response["appInfo"][0]["address1_latitude"], $response["appInfo"][0]["address1_longitude"], ['marker' => true]);
		
		// 		$stations = Greyhound::all();
		
		// 		foreach ($stations as $station) {
			// 			$content = $station['station_name'];
			
			// 			Mapper::informationWindow($station['lat'], $station['lng'], $content);
			//

			$maindata = $response["appInfo"][0];
			$obsmsdata = $this->retreiveobsms($response["appInfo"][0]['mobilephone']);
			$ibsmsdata = $this->retreiveibsms($response["appInfo"][0]['mobilephone']);

			//$appdata = $this->scribeapplist();

			//$appdata = Request::old();

			$allsmsdata = array_merge($obsmsdata, $ibsmsdata);


			uasort($allsmsdata, array($this, 'cmp'));


			// $sorted = uasort($allsmsdata, function($a, $b) {
			// 	$ad = date_format($a->dateCreated, "Y-m-d H:i:s");
			// 	$bd = date_format($b->dateCreated, "Y-m-d H:i:s");
			  
			// 	if ($ad == $bd) {
			// 	  return 0;
			// 	}
			  
			// 	return $ad < $bd ? -1 : 1;
			//   });

			//$sortedsmsdata = asort($allsmsdata);

			//$collection = collect($allsmsdata);

			//``$sorted = $collection->sortBy('direction');

			//$applistcollection = collect($appdata['user'][0]['application']);

			//$applistsorted = $applistcollection->sortBy('modifiedon');

			$todaydate = Carbon::today()->subDays(30);



			//return $this->retreivesms($response["appInfo"][0]['mobilephone']);

			//return $this->vdump($sortedsmsdata);
			
		//return view('example',compact('maindata', 'allsmsdata'));

		return View::make('example')
						            ->with('maindata', $maindata)
						            ->with('allsmsdata', $allsmsdata)
									//->with('appdata', $appdata)
									->with('todaydate', $todaydate);
		}




		
		
		public function writeapp($agent_fullname, $cre_appid, $cre_applicantidname, $createdon, $modifiedon, $statecode, $systemuserid)
		    {
		$agentapp = new AgentApps;
		
		
		$agentapp->agent_fullname = $agent_fullname;
		$agentapp->cre_appid = $cre_appid;
		$agentapp->cre_applicantidname = $cre_applicantidname;
		$agentapp->createdon = $createdon;
		$agentapp->modifiedon = $modifiedon;
		$agentapp->statecode = $statecode;
		$agentapp->systemuserid = $systemuserid;
		
		
		$agentapp->save();
	}
		
		
		
		
		
		
		
		public function indexnoappidwithmodel()
	{
		$appdata = \App\BillTestModel::scribeapplist();

		//$sessiondata = \Session::all();

		$todaydate = Carbon::today()->subDays(30);

		$deletedRows = AgentApps::where('systemuserid', '=', \Session::get('sysuserid'))->delete();

		// $denialdelete = AgentApps::where('appid', '=', $data['app_id'])->first();
		// $denialdelete->delete();

		foreach ($appdata['user'][0]['application'] as $app) {
			
            if ($app['statecode'] == 0) {
                           
            
                $this->writeapp(\Session::get('agent'), $app['cre_appid'], $app['cre_applicantidname'], date('Y-m-d H:i:s', strtotime($app['createdon'])), date('Y-m-d H:i:s', strtotime($app['modifiedon'])), $app['statecode'], $app['systemuserid']);
            }
		}

		//var_dump($appdata);

		return View::make('example')
						            // ->with('maindata', $maindata)
						            // ->with('allsmsdata', $allsmsdata)
									//->with('appdata', $appdata)
									->with('todaydate', $todaydate);
    }




		



		
		
		
		
		
		
		public function colorchange()
		{
			// 			$headerdata = Request::only(['header']);
			// 			$themedata = Request::only(['theme']);
			// 			$userdata = Request::only(['username']);
			
			$userfind = Users::where('sysuserid', '=', $_POST['username'])->first();
			$userfind->header_color = $_POST['header'];
			$userfind->theme_color = $_POST['theme'];
			$userfind->save();
			
			$agent = $_POST['userfname'];
			
			return redirect()->route('mainpage');
		}
		
		public function map()
		{
			
			// 			Draw a map
			Mapper::map(40.59726025535419, 80.02503488467874, ['marker' => true]);
			
			
			
			return view('testinggm.mapper');
			
		}
		
		// 		public function testfunction()
			    // 		{
			// 			$user = Users::where([
					    // 			['email', '=', $_POST['username']],
					    // 			['password', '=', $_POST['password']],
					    // 			])
					    // 			->get();
			
			// 			if ($user['role_id'] = 2)
					    // 			{
				// 				return view('example');
				//
			//}
			// 			else if ($user['role_id'] = 3)
					    // 			{
				// 				return redirect()->route('managers');
				//
			//}
			//
		//}
		
		
		
		
	}
	