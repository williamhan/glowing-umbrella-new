<?php

namespace App\Http\Controllers;
use App\Nerd;
use App\TransportationInfo;
use App\Schools;
use App\States;
use App\SchoolInfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class NerdInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds

        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoolinfo = SchoolInfo::with('transportationinfo')->get();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('schoolinfo.index')
            ->with('stateslist', $stateslist)
            ->with('schoolinfo', $schoolinfo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        $stateslist = States::pluck('state_name', 'state_abbr');
        return View::make('schoolinfo.create')
            ->with('stateslist', $stateslist);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_name'       => 'required',
            'sch_type' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('schoolinfo/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $schoolinfo = new SchoolInfo;
            $schoolinfo->sch_name       = Input::get('sch_name');
            $schoolinfo->sch_type      = Input::get('sch_type');
            $schoolinfo->sch_address = Input::get('sch_address');
            $schoolinfo->sch_city = Input::get('sch_city');
            $schoolinfo->sch_state = Input::get('sch_state');
            $schoolinfo->sch_zip = Input::get('sch_zip');
            $schoolinfo->sch_phone = Input::get('sch_phone');
            $schoolinfo->sch_fax = Input::get('sch_fax');
            $schoolinfo->sch_duration_min = Input::get('sch_duration_min');
            $schoolinfo->sch_duration_max = Input::get('sch_duration_max');
            $schoolinfo->sch_transportation_id = Input::get('sch_transportation_id');
            $schoolinfo->sch_schedule_id = Input::get('sch_schedule_id');
            $schoolinfo->sch_docs_id = Input::get('sch_docs_id');
            $schoolinfo->sch_fees_id = Input::get('sch_fees_id');
            $schoolinfo->sch_housing_id = Input::get('sch_housing_id');
            $schoolinfo->save();

            // redirect
            Session::flash('message', 'Successfully created school!');
            return Redirect::to('schoolinfo');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoolinfo = SchoolInfo::find($id);

        // show the view and pass the nerd to it
        return View::make('schoolinfo.show')
            ->with('stateslist', $stateslist)
            ->with('schoolinfo', $schoolinfo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $stateslist = States::pluck('state_name', 'state_abbr');
        $schoolinfo = SchoolInfo::find($id);

        // show the edit form and pass the nerd
        return View::make('schoolinfo.edit')
            ->with('stateslist', $stateslist)
            ->with('schoolinfo', $schoolinfo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'sch_name'       => 'required',
            'sch_type' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('schoolinfo/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $schoolinfo = SchoolInfo::find($id);
            $schoolinfo->sch_name       = Input::get('sch_name');
            $schoolinfo->sch_type      = Input::get('sch_type');
            $schoolinfo->sch_address = Input::get('sch_address');
            $schoolinfo->sch_city = Input::get('sch_city');
            $schoolinfo->sch_state = Input::get('sch_state');
            $schoolinfo->sch_zip = Input::get('sch_zip');
            $schoolinfo->sch_phone = Input::get('sch_phone');
            $schoolinfo->sch_fax = Input::get('sch_fax');
            $schoolinfo->sch_duration_min = Input::get('sch_duration_min');
            $schoolinfo->sch_duration_max = Input::get('sch_duration_max');
            $schoolinfo->sch_transportation_id = Input::get('sch_transportation_id');
            $schoolinfo->sch_schedule_id = Input::get('sch_schedule_id');
            $schoolinfo->sch_docs_id = Input::get('sch_docs_id');
            $schoolinfo->sch_fees_id = Input::get('sch_fees_id');
            $schoolinfo->sch_housing_id = Input::get('sch_housing_id');
            $schoolinfo->save();

            // redirect
            Session::flash('message', 'Successfully updated school!');
            return Redirect::to('schoolinfo');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $schoolinfo = SchoolInfo::find($id);
        $schoolinfo->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('schoolinfo');
    }
}
