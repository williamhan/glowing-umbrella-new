<?php

namespace App\Http\Controllers;

use Request;

use ET_Client;

class VeteransController extends Controller
{
    public function dd214email()
    {

        $DataExtensionName = "DD214_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }

    public function commandersletteremail()
    {

        $DataExtensionName = "Commanders_Letter_Email_List";

        date_default_timezone_set('America/Denver');

        $myclient = new \ET_Client();
        $dataextensionrow = new \ET_DataExtension_Row();
        $dataextensionrow->authStub = $myclient;
        $dataextensionrow->Name = $DataExtensionName;
        $dataextensionrow->props = array("Email_Address" => $_POST['email'], "App_ID" => $_POST['app_id'], "First_Name" => $_POST['firstname'], "Last_Name" => $_POST['lastname'],
            "App_Type" => $_POST['app_type'], "Booking_Date" => $_POST['booking_date'], "Booking_School" => $_POST['school'], "Recruiter" => $_POST['recruiter'], "Sent_Date" => date('Y-m-d H:i:s'));
        $results = $dataextensionrow->post();
        print_r($results);
    }
}
