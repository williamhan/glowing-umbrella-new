<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schools;
use App\SchoolInfo;
use DB;

class SchoolInfoController extends Controller
{
    public function index()
    {
        $school = Schools::all();

        foreach ($school as $sch)
        {
            $schinfo = new SchoolInfo;
            $schinfo->sch_name = $sch['school_name'];
            $schinfo->sch_id = $sch['id'];
            $schinfo->save();
        }


        $schoolinfo = SchoolInfo::all();



        return $schoolinfo;
    }

    public function grid()
    {
        $schools['all'] = SchoolInfo::where('sch_id', '>', 0)
            ->get();
        $schools['3p'] = SchoolInfo::where('sch_type', '=', '3P')
            ->get();

        return view('table', compact('schools'));
    }
}
