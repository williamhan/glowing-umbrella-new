<?php

namespace App\Http\Controllers;
use App\Nerd;
use App\Schools;
use App\FeesInfo;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class NerdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get all the nerds

        $schools = Schools::all();

        // load the view and pass the nerds
//        return View::make('nerds.index')
//            ->with('nerds', $nerds);
        return View::make('schools.index')
            ->with('schools', $schools);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the create form (app/views/nerds/create.blade.php)
        return view('schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'school_name'       => 'required',
            'gh_station'      => 'required',
            'sch_type' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('schools/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $school = new Schools;
            $school->school_name       = Input::get('school_name');
            $school->gh_station      = Input::get('gh_station');
            $school->sch_type = Input::get('sch_type');
            $school->save();

            // redirect
            Session::flash('message', 'Successfully created school!');
            return Redirect::to('schools');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // get the nerd
        $schools = Schools::find($id);

        // show the view and pass the nerd to it
        return View::make('schools.show')
            ->with('school', $schools);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // get the nerd
        $schools = Schools::find($id);

        // show the edit form and pass the nerd
        return View::make('schools.edit')
            ->with('school', $schools);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'school_name'       => 'required',
            'gh_station'      => 'required',
            'sch_type' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('schools/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $school = Schools::find($id);
            $school->school_name       = Input::get('school_name');
            $school->gh_station      = Input::get('gh_station');
            $school->sch_type = Input::get('sch_type');
            $school->save();

            // redirect
            Session::flash('message', 'Successfully updated school!');
            return Redirect::to('schools');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $school = Schools::find($id);
        $school->delete();

        // redirect
        Session::flash('message', 'Successfully deleted the school!');
        return Redirect::to('schools');
    }
}
