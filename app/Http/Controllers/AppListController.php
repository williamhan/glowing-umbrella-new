<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppListController extends Controller
{
    public function scribeapplist($agentname)
    {
		
        $url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/4530?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
		
		$jsonDataEncoded = json_encode($agentname);
		
		$curl = curl_init();
		
		$opts = array(
				            CURLOPT_URL             => $url,
				            CURLOPT_RETURNTRANSFER  => true,
				            CURLOPT_CUSTOMREQUEST   => 'POST',
				            CURLOPT_POST            => 1,
				            CURLOPT_POSTFIELDS      => $jsonDataEncoded
				        );
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
		curl_setopt_array($curl, $opts);
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		$response = json_decode($result, true);
		
		return $response;
	}
}
