<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WSSoapClient;
use SoapClient;
use SoapVar;
use SoapHeader;
use SoapFault;
use SoapParam;
use Soap;
use Illuminate\Support\Facades\View;
use App\HireRight;


class HireRightController extends Controller
{


    public function authblock()
    {
        $pass='wsapi_pswd';
        $created = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();
        $passdigest = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $created) . pack('a*', $pass))));

        $auth = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $auth .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $auth .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $auth .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigest.'</wsse:Password>';
        $auth .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $nonce)).'</wsse:Nonce>';
        $auth .= '				<wsu:Created>' . $created . '</wsu:Created>';
        $auth .= '			</wsse:UsernameToken>';
        $auth .= '		</wsse:Security>';
        $auth_block = new SoapVar( $auth, XSD_ANYXML, NULL, NULL, NULL, NULL );

        return $auth_block;
    }

    public function nameObj()
    {
        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        return $nameObj;
    }

    public function addressObj()
    {
        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar(new SoapVar($_POST['address1_line1'], XSD_STRING, null, null, 'AddressLine'), SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        return $addressObj;
    }

    public function contactoneObj()
    {
        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], XSD_STRING, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('email', XSD_STRING, null, null, 'InternetEmailAddress');

        return $contactoneObj;
    }

    public function contacttwoObj()
    {
        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($_POST['mobilephone'], XSD_STRING, null, null, 'Telephone');

        return $contacttwoObj;
    }

    public function flexfieldObj()
    {
        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $flxfieldObj = new SoapVar($flexfielddata, XSD_ANYXML, null, null, 'FlexField');

        return $flxfieldObj;
    }

    public function demographicObj()
    {
        $demographicObj = array();
        $demographicObj[] = new SoapVar('<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>', XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        return $demographicObj;
    }

    public function persondata()
    {
        $persondata = array();
        $persondata[] = new SoapVar($this->nameObj(), SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($this->addressObj(), SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($this->contactoneObj(), SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($this->contacttwoObj(), SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($this->demographicObj(), SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        return $persondata;
    }

    public function personaldataObj()
    {
        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($this->persondata(), SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar('<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>', XSD_ANYXML);
        $personaldataObj[] = new SoapVar('    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>', XSD_ANYXML);

        return $personaldataObj;
    }

    public function bgcheckObj()
    {
        $bgcheckObj = array();
        $bgcheckObj[] = new SoapVar($this->personaldataObj(), SOAP_ENC_OBJECT, null, null, 'BackgroundSearchPackage');
        $bgcheckObj[] = new SoapVar('UserArea', XSD_STRING, null, null, 'UserArea');

        return $bgcheckObj;
    }

    public function payloadObj()
    {
        $payloadObj = new SoapVar($this->bgcheckObj(), SOAP_ENC_OBJECT, null, null, 'BackgroundCheck');

        return $payloadObj;
    }

    public function tmpObj($servicetype)
    {
        $tmpObj = array();
        $tmpObj[] = new SoapVar("WS_API", XSD_STRING, null, null, 'AccountId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('TDAC2', XSD_STRING, null, null, 'CompanyLogin', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('123', XSD_STRING, null, null, 'UserRefId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientApplicantId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientRequestId', "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj[] = new SoapVar("309496", XSD_STRING, null, null, 'PackageId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($servicetype, SOAP_ENC_OBJECT, null, null, 'Services', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($this->flexfieldObj(), SOAP_ENC_OBJECT, null, null, 'FlexFields', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($this->payloadObj(), SOAP_ENC_OBJECT, null, null, 'Payload', "urn:enterprise.soap.hireright.com/objs");

        return $tmpObj;
    }

    public function urlObj($object_id, $reg_id)
    {
        $urlObj = array();
        $urlObj[] = new SoapVar("WS_API", XSD_STRING, null, null, 'AccountId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('TDAC2', XSD_STRING, null, null, 'CompanyLogin', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('123', XSD_STRING, null, null, 'UserRefId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar($reg_id, XSD_STRING, null, null, 'RegId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientApplicantId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientRequestId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar($object_id, XSD_STRING, null, null, 'ObjId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, 'LandingPage', "urn:enterprise.soap.hireright.com/objs");

        return $urlObj;
    }

    public function urlObjLookup()
    {


        $resultid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
        ])
            ->latest()
            ->select('object_id')
            ->get();

        $requestid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
        ])
            ->latest()
            ->select('reg_id')
            ->get();



        $urlObj = array();
        $urlObj[] = new SoapVar("WS_API", XSD_STRING, null, null, 'AccountId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('TDAC2', XSD_STRING, null, null, 'CompanyLogin', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('123', XSD_STRING, null, null, 'UserRefId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar($requestid, XSD_STRING, null, null, 'RegId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientApplicantId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, 'ClientRequestId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar($resultid, XSD_STRING, null, null, 'ObjId', "urn:enterprise.soap.hireright.com/objs");
        $urlObj[] = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, 'LandingPage', "urn:enterprise.soap.hireright.com/objs");

        return $urlObj;
    }


    public function createClient($clientname, $action, $params)
    {
//        $hirerightclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE,
//            'location' => "https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2" )); 
        //$header = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock() ); 
        //$hirerightclient->__setSoapHeaders( new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock() ) ); 
        //$hirerightclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed
        $runclient = $clientname->__SoapCall($action, array($params));

        //__SoapCall($action, array($params));

        return $runclient;
    }




    public function mvr()
    {
        $hrclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hrclient->__setSoapHeaders( new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock()))
            ->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

//        $webclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $webclient->__setSoapHeaders( new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock())); 
//        $webclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed



        $mvrexpservice = new SoapVar('<ns1:Service id="MVR Express"/>', XSD_ANYXML, null, null, 'Service');
        $mvrstdservice = new SoapVar('<ns1:Service id="MVR Standard"/>', XSD_ANYXML, null, null, 'Service');
        $dacservice = new SoapVar('<ns1:Service id="Drug/Alcohol History Database"/>', XSD_ANYXML, null, null, 'Service');
        $cdlisservice = new SoapVar('<ns1:Service id="CDLIS+"/>', XSD_ANYXML, null, null, 'Service');

        $CreateParms = new SoapVar(new SoapVar(new SoapVar($this->tmpObj($mvrexpservice), SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, 'HRObject', "urn:enterprise.soap.hireright.com/objs"), SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);


        //$hrclient->__SoapCall('Create', array($CreateParms));

        if($mvr = $hrclient->__SoapCall('Create', array($CreateParms)))
        {

            $webclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
            $webclient->__setSoapHeaders( new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock()));
            $webclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

            $runrecord = new HireRight;
            $runrecord->app_id = $_POST['app_id'];
            $runrecord->app_id_guid = $_POST['app_id_guid'];
            $runrecord->agent = $_POST['agent'];
            $runrecord->agent_guid = $_POST['agent_id'];
            $runrecord->object_id = $mvr->Result->Id;
            $runrecord->reg_id = $mvr->Result->RegId;
            $runrecord->save();
            $WebLinkParms = new SoapVar(new SoapVar(new SoapVar($this->urlObj($mvr->Result->Id, $mvr->Result->RegId), SOAP_ENC_OBJECT, "SingleSignOnWL", null, 'HRWebLink', "urn:enterprise.soap.hireright.com/objs"), SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);
            $url = $webclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
            //$url = $this->createClient('GenerateWebLink', $WebLinkParms);
            return $url->URL;
        }
        else
        {
            $webclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
            $webclient->__setSoapHeaders( new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $this->authblock()));
            $webclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

            $WebLinkParms = new SoapVar(new SoapVar(new SoapVar($this->urlObjLookup(), SOAP_ENC_OBJECT, "SingleSignOnWL", null, 'HRWebLink', "urn:enterprise.soap.hireright.com/objs"), SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);
            $url = $webclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
            return $url->URL;
        }
    }


    public function index()
    {



        $passmvrexp='wsapi_pswd';
        $createdmvrexp = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrexp = mt_rand();
        $passdigestmvrexp = base64_encode(pack('H*', sha1(pack('H*', $noncemvrexp) . pack('a*', $createdmvrexp) . pack('a*', $passmvrexp))));

        $passmvrstd='wsapi_pswd';
        $createdmvrstd = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrstd = mt_rand();
        $passdigestmvrstd = base64_encode(pack('H*', sha1(pack('H*', $noncemvrstd) . pack('a*', $createdmvrstd) . pack('a*', $passmvrstd))));

        $passmvrws='wsapi_pswd';
        $createdmvrws = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrws = mt_rand();
        $passdigestmvrws = base64_encode(pack('H*', sha1(pack('H*', $noncemvrws) . pack('a*', $createdmvrws) . pack('a*', $passmvrws))));

        $passmvrdac='wsapi_pswd';
        $createdmvrdac = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrdac = mt_rand();
        $passdigestmvrdac = base64_encode(pack('H*', sha1(pack('H*', $noncemvrdac) . pack('a*', $createdmvrdac) . pack('a*', $passmvrdac))));

        $passmvrcdlis='wsapi_pswd';
        $createdmvrcdlis = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrcdlis = mt_rand();
        $passdigestmvrcdlis = base64_encode(pack('H*', sha1(pack('H*', $noncemvrcdlis) . pack('a*', $createdmvrcdlis) . pack('a*', $passmvrcdlis))));

        $passmvrehf='wsapi_pswd';
        $createdmvrehf = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrehf = mt_rand();
        $passdigestmvrehf = base64_encode(pack('H*', sha1(pack('H*', $noncemvrehf) . pack('a*', $createdmvrehf) . pack('a*', $passmvrehf))));



        $hirerightclientmvrexp = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrws = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrdac = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrcdlis = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrehf = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));


        $authmvrexp = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrexp .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrexp .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrexp .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrexp.'</wsse:Password>';
        $authmvrexp .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrexp)).'</wsse:Nonce>';
        $authmvrexp .= '				<wsu:Created>' . $createdmvrexp . '</wsu:Created>';
        $authmvrexp .= '			</wsse:UsernameToken>';
        $authmvrexp .= '		</wsse:Security>';
        $auth_blockmvrexp = new SoapVar( $authmvrexp, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrstd = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrstd .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrstd .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrstd .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrstd.'</wsse:Password>';
        $authmvrstd .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrstd)).'</wsse:Nonce>';
        $authmvrstd .= '				<wsu:Created>' . $createdmvrstd . '</wsu:Created>';
        $authmvrstd .= '			</wsse:UsernameToken>';
        $authmvrstd .= '		</wsse:Security>';
        $auth_blockmvrstd = new SoapVar( $authmvrstd, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrws = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrws .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrws .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrws .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrws.'</wsse:Password>';
        $authmvrws .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrws)).'</wsse:Nonce>';
        $authmvrws .= '				<wsu:Created>' . $createdmvrws . '</wsu:Created>';
        $authmvrws .= '			</wsse:UsernameToken>';
        $authmvrws .= '		</wsse:Security>';
        $auth_blockmvrws = new SoapVar( $authmvrws, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrcdlis = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrcdlis .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrcdlis .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrcdlis .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrcdlis.'</wsse:Password>';
        $authmvrcdlis .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrcdlis)).'</wsse:Nonce>';
        $authmvrcdlis .= '				<wsu:Created>' . $createdmvrcdlis . '</wsu:Created>';
        $authmvrcdlis .= '			</wsse:UsernameToken>';
        $authmvrcdlis .= '		</wsse:Security>';
        $auth_blockmvrcdlis = new SoapVar( $authmvrcdlis, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrdac = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrdac .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrdac .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrdac .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrdac.'</wsse:Password>';
        $authmvrdac .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrdac)).'</wsse:Nonce>';
        $authmvrdac .= '				<wsu:Created>' . $createdmvrdac . '</wsu:Created>';
        $authmvrdac .= '			</wsse:UsernameToken>';
        $authmvrdac .= '		</wsse:Security>';
        $auth_blockmvrdac = new SoapVar( $authmvrdac, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrehf = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrehf .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrehf .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrehf .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrehf.'</wsse:Password>';
        $authmvrehf .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrehf)).'</wsse:Nonce>';
        $authmvrehf .= '				<wsu:Created>' . $createdmvrehf . '</wsu:Created>';
        $authmvrehf .= '			</wsse:UsernameToken>';
        $authmvrehf .= '		</wsse:Security>';
        $auth_blockmvrehf = new SoapVar( $authmvrehf, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $headermvrexp = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrexp );
        $hirerightclientmvrexp->__setSoapHeaders( $headermvrexp );
        $hirerightclientmvrexp->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrstd = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrstd );
        $hirerightclientmvrstd->__setSoapHeaders( $headermvrstd );
        $hirerightclientmvrstd->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrws = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrws );
        $hirerightclientmvrws->__setSoapHeaders( $headermvrws );
        $hirerightclientmvrws->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrcdlis = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrcdlis );
        $hirerightclientmvrcdlis->__setSoapHeaders( $headermvrcdlis );
        $hirerightclientmvrcdlis->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrdac = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrdac );
        $hirerightclientmvrdac->__setSoapHeaders( $headermvrdac );
        $hirerightclientmvrdac->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrehf = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrehf );
        $hirerightclientmvrehf->__setSoapHeaders( $headermvrehf );
        $hirerightclientmvrehf->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $HRObject = $app->make('stdClass');

        $tmpObj = $app->make('stdClass');

        $srvcObj = $app->make('stdClass');

        $flxfieldObj = $app->make('stdClass');

        $payloadObj = $app->make('stdClass');

        $bgcheckObj = $app->make('stdClass');

        $phoneoneObj = $app->make('stdClass');

        $phonetwoObj = $app->make('stdClass');

        $addresslineObj = $app->make('stdClass');


        $servicedata = '<ns1:Service id="MVR Express"/>';

//        if ($servicetype == 'mvrexp')
//        {
//            $servicedata = '<ns1:Service id="MVR Express"/>';
//        }
//        elseif ($servicetype == 'mvrstd')
//        {
//            $servicedata = '<ns1:Service id="MVR Standard"/>';
//        }
//        elseif ($servicetype == 'dac')
//        {
//            $servicedata = '<ns1:Service id="Drug/Alcohol History Database"/>';
//        }
//        elseif ($servicetype == 'cdlis')
//        {
//            $servicedata = '<ns1:Service id="CDLIS+"/>';
//        }
//        else
//        {
//            $servicedata = '<ns1:Service id="MVR Express"/>';
//        }

        //$servicedata = '<ns1:Service id="MVR Express"/>';

        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

        $licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

        $srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

        $flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

        $addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

        $phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('email', XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
        $personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

        $bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

        $bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

        $tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

//        $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->RegId = new SoapVar($ret->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ObjId = new SoapVar($ret->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//
//        $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");
//
//        $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

        try
        {
            $retmvrexp = $hirerightclientmvrexp->__SoapCall('Create', array($CreateParms));

            $retmvrstd = $hirerightclientmvrstd->__SoapCall('Create', array($CreateParms));

            $retmvrws = $hirerightclientmvrws->__SoapCall('Create', array($CreateParms));

            $retmvrdac = $hirerightclientmvrdac->__SoapCall('Create', array($CreateParms));

            $retmvrcdlis = $hirerightclientmvrcdlis->__SoapCall('Create', array($CreateParms));

            $retmvrehf = $hirerightclientmvrehf->__SoapCall('Create', array($CreateParms));



            try
            {

                $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->RegId = new SoapVar($retmvrexp->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ObjId = new SoapVar($retmvrexp->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

                $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

                $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

                $url = $hirerightclientmvrstd->__SoapCall('GenerateWebLink', array($WebLinkParms));
            }
            catch (Exception $e)
            {

            }
        }
        catch (Exception $e)
        {
        }

        $resultid = $retmvrexp->Result->Id;
        $regid = $retmvrexp->Result->RegId;

        $runrecord = new HireRight;
        $runrecord->app_id = $_POST['app_id'];
        $runrecord->app_id_guid = $_POST['app_id_guid'];
        $runrecord->agent = $_POST['agent'];
        $runrecord->agent_guid = $_POST['agent_id'];
        $runrecord->object_id = $resultid;
        $runrecord->reg_id = $regid;
        $runrecord->save();



//        try {
//            $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
//        } catch (Exception $e) {
//
//        }

        $urlret = $url->URL;

        return $urlret;


    }

    public function mvrdev()
    {
        $passmvrexp='wsapi_pswd';
        $createdmvrexp = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrexp = mt_rand();
        $passdigestmvrexp = base64_encode(pack('H*', sha1(pack('H*', $noncemvrexp) . pack('a*', $createdmvrexp) . pack('a*', $passmvrexp))));

        $passmvrws='wsapi_pswd';
        $createdmvrws = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrws = mt_rand();
        $passdigestmvrws = base64_encode(pack('H*', sha1(pack('H*', $noncemvrws) . pack('a*', $createdmvrws) . pack('a*', $passmvrws))));

        $passmvrdac='wsapi_pswd';
        $createdmvrdac = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrdac = mt_rand();
        $passdigestmvrdac = base64_encode(pack('H*', sha1(pack('H*', $noncemvrdac) . pack('a*', $createdmvrdac) . pack('a*', $passmvrdac))));

        $passmvrcdlis='wsapi_pswd';
        $createdmvrcdlis = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrcdlis = mt_rand();
        $passdigestmvrcdlis = base64_encode(pack('H*', sha1(pack('H*', $noncemvrcdlis) . pack('a*', $createdmvrcdlis) . pack('a*', $passmvrcdlis))));

        $passmvrehf='wsapi_pswd';
        $createdmvrehf = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrehf = mt_rand();
        $passdigestmvrehf = base64_encode(pack('H*', sha1(pack('H*', $noncemvrehf) . pack('a*', $createdmvrehf) . pack('a*', $passmvrehf))));



        $hirerightclientmvrexp = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrws = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrdac = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrcdlis = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrehf = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));


        $authmvrexp = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrexp .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrexp .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrexp .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrexp.'</wsse:Password>';
        $authmvrexp .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrexp)).'</wsse:Nonce>';
        $authmvrexp .= '				<wsu:Created>' . $createdmvrexp . '</wsu:Created>';
        $authmvrexp .= '			</wsse:UsernameToken>';
        $authmvrexp .= '		</wsse:Security>';
        $auth_blockmvrexp = new SoapVar( $authmvrexp, XSD_ANYXML, NULL, NULL, NULL, NULL );


        $authmvrws = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrws .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrws .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrws .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrws.'</wsse:Password>';
        $authmvrws .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrws)).'</wsse:Nonce>';
        $authmvrws .= '				<wsu:Created>' . $createdmvrws . '</wsu:Created>';
        $authmvrws .= '			</wsse:UsernameToken>';
        $authmvrws .= '		</wsse:Security>';
        $auth_blockmvrws = new SoapVar( $authmvrws, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrcdlis = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrcdlis .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrcdlis .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrcdlis .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrcdlis.'</wsse:Password>';
        $authmvrcdlis .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrcdlis)).'</wsse:Nonce>';
        $authmvrcdlis .= '				<wsu:Created>' . $createdmvrcdlis . '</wsu:Created>';
        $authmvrcdlis .= '			</wsse:UsernameToken>';
        $authmvrcdlis .= '		</wsse:Security>';
        $auth_blockmvrcdlis = new SoapVar( $authmvrcdlis, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrdac = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrdac .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrdac .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrdac .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrdac.'</wsse:Password>';
        $authmvrdac .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrdac)).'</wsse:Nonce>';
        $authmvrdac .= '				<wsu:Created>' . $createdmvrdac . '</wsu:Created>';
        $authmvrdac .= '			</wsse:UsernameToken>';
        $authmvrdac .= '		</wsse:Security>';
        $auth_blockmvrdac = new SoapVar( $authmvrdac, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $authmvrehf = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrehf .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrehf .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrehf .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrehf.'</wsse:Password>';
        $authmvrehf .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrehf)).'</wsse:Nonce>';
        $authmvrehf .= '				<wsu:Created>' . $createdmvrehf . '</wsu:Created>';
        $authmvrehf .= '			</wsse:UsernameToken>';
        $authmvrehf .= '		</wsse:Security>';
        $auth_blockmvrehf = new SoapVar( $authmvrehf, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $headermvrexp = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrexp );
        $hirerightclientmvrexp->__setSoapHeaders( $headermvrexp );
        $hirerightclientmvrexp->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


        $headermvrws = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrws );
        $hirerightclientmvrws->__setSoapHeaders( $headermvrws );
        $hirerightclientmvrws->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrcdlis = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrcdlis );
        $hirerightclientmvrcdlis->__setSoapHeaders( $headermvrcdlis );
        $hirerightclientmvrcdlis->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrdac = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrdac );
        $hirerightclientmvrdac->__setSoapHeaders( $headermvrdac );
        $hirerightclientmvrdac->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $headermvrehf = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrehf );
        $hirerightclientmvrehf->__setSoapHeaders( $headermvrehf );
        $hirerightclientmvrehf->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $HRObject = $app->make('stdClass');

        $tmpObj = $app->make('stdClass');

        $srvcObj = $app->make('stdClass');

        $flxfieldObj = $app->make('stdClass');

        $payloadObj = $app->make('stdClass');

        $bgcheckObj = $app->make('stdClass');

        $phoneoneObj = $app->make('stdClass');

        $phonetwoObj = $app->make('stdClass');

        $addresslineObj = $app->make('stdClass');


        $servicedata = '<ns1:Service id="MVR Express"/>';


        //$servicedata = '<ns1:Service id="MVR Express"/>';

        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

        $licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

        $srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

        $flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

        $addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

        $phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('email', XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
        $personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

        $bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

        $bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

        $tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

        $mvrexpservice = new SoapVar('<ns1:Service id="MVR Express"/>', XSD_ANYXML, null, null, 'Service');



        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);
        //$CreateParms = new SoapVar(new SoapVar(new SoapVar($this->tmpObj($mvrexpservice), XSD_ANYXML, "hr_objs:BackgroundCheck", null, 'HRObject', "urn:enterprise.soap.hireright.com/objs"), SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);


        try
        {
            $retmvrexp = $hirerightclientmvrexp->__SoapCall('Create', array($CreateParms));

//            $retmvrstd = $hirerightclientmvrstd->__SoapCall('Create', array($CreateParms));
//
//            $retmvrws = $hirerightclientmvrws->__SoapCall('Create', array($CreateParms));
//
//            $retmvrdac = $hirerightclientmvrdac->__SoapCall('Create', array($CreateParms));
//
//            $retmvrcdlis = $hirerightclientmvrcdlis->__SoapCall('Create', array($CreateParms));
//
//            $retmvrehf = $hirerightclientmvrehf->__SoapCall('Create', array($CreateParms));



            try
            {

                $passmvrstd='wsapi_pswd';
                $createdmvrstd = gmdate('Y-m-d\TH:i:s\Z');
                $noncemvrstd = mt_rand();
                $passdigestmvrstd = base64_encode(pack('H*', sha1(pack('H*', $noncemvrstd) . pack('a*', $createdmvrstd) . pack('a*', $passmvrstd))));

                $authmvrstd = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
                $authmvrstd .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
                $authmvrstd .= '				<wsse:Username>wsapi_api</wsse:Username>';
                $authmvrstd .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrstd.'</wsse:Password>';
                $authmvrstd .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrstd)).'</wsse:Nonce>';
                $authmvrstd .= '				<wsu:Created>' . $createdmvrstd . '</wsu:Created>';
                $authmvrstd .= '			</wsse:UsernameToken>';
                $authmvrstd .= '		</wsse:Security>';
                $auth_blockmvrstd = new SoapVar( $authmvrstd, XSD_ANYXML, NULL, NULL, NULL, NULL );

                $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));

                $headermvrstd = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrstd );
                $hirerightclientmvrstd->__setSoapHeaders( $headermvrstd );
                $hirerightclientmvrstd->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed





                $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->RegId = new SoapVar($retmvrexp->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ObjId = new SoapVar($retmvrexp->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

                $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

                $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

                $url = $hirerightclientmvrstd->__SoapCall('GenerateWebLink', array($WebLinkParms));
            }
            catch (Exception $e)
            {

            }
        }
        catch (Exception $e)
        {
        }

        $resultid = $retmvrexp->Result->Id;
        $regid = $retmvrexp->Result->RegId;

        $runrecord = new HireRight;
        $runrecord->app_id = $_POST['app_id'];
        $runrecord->app_id_guid = $_POST['app_id_guid'];
        $runrecord->agent = $_POST['agent'];
        $runrecord->agent_guid = $_POST['agent_id'];
        $runrecord->object_id = $resultid;
        $runrecord->reg_id = $regid;
        $runrecord->save();



//        try {
//            $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
//        } catch (Exception $e) {
//
//        }

        $urlret = $url->URL;

        return $urlret;
    }

    public function widescreendev()
    {
//        $passmvrexp='wsapi_pswd';
//        $createdmvrexp = gmdate('Y-m-d\TH:i:s\Z');
//        $noncemvrexp = mt_rand();
//        $passdigestmvrexp = base64_encode(pack('H*', sha1(pack('H*', $noncemvrexp) . pack('a*', $createdmvrexp) . pack('a*', $passmvrexp))));

        $passmvrws='wsapi_pswd';
        $createdmvrws = gmdate('Y-m-d\TH:i:s\Z');
        $noncemvrws = mt_rand();
        $passdigestmvrws = base64_encode(pack('H*', sha1(pack('H*', $noncemvrws) . pack('a*', $createdmvrws) . pack('a*', $passmvrws))));

//        $passmvrdac='wsapi_pswd';
//        $createdmvrdac = gmdate('Y-m-d\TH:i:s\Z');
//        $noncemvrdac = mt_rand();
//        $passdigestmvrdac = base64_encode(pack('H*', sha1(pack('H*', $noncemvrdac) . pack('a*', $createdmvrdac) . pack('a*', $passmvrdac))));
//
//        $passmvrcdlis='wsapi_pswd';
//        $createdmvrcdlis = gmdate('Y-m-d\TH:i:s\Z');
//        $noncemvrcdlis = mt_rand();
//        $passdigestmvrcdlis = base64_encode(pack('H*', sha1(pack('H*', $noncemvrcdlis) . pack('a*', $createdmvrcdlis) . pack('a*', $passmvrcdlis))));
//
//        $passmvrehf='wsapi_pswd';
//        $createdmvrehf = gmdate('Y-m-d\TH:i:s\Z');
//        $noncemvrehf = mt_rand();
//        $passdigestmvrehf = base64_encode(pack('H*', sha1(pack('H*', $noncemvrehf) . pack('a*', $createdmvrehf) . pack('a*', $passmvrehf))));



//        $hirerightclientmvrexp = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $hirerightclientmvrws = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $hirerightclientmvrdac = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $hirerightclientmvrcdlis = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
//        $hirerightclientmvrehf = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));


//        $authmvrexp = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
//        $authmvrexp .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
//        $authmvrexp .= '				<wsse:Username>wsapi_api</wsse:Username>';
//        $authmvrexp .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrexp.'</wsse:Password>';
//        $authmvrexp .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrexp)).'</wsse:Nonce>';
//        $authmvrexp .= '				<wsu:Created>' . $createdmvrexp . '</wsu:Created>';
//        $authmvrexp .= '			</wsse:UsernameToken>';
//        $authmvrexp .= '		</wsse:Security>';
//        $auth_blockmvrexp = new SoapVar( $authmvrexp, XSD_ANYXML, NULL, NULL, NULL, NULL );


        $authmvrws = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $authmvrws .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $authmvrws .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $authmvrws .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrws.'</wsse:Password>';
        $authmvrws .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrws)).'</wsse:Nonce>';
        $authmvrws .= '				<wsu:Created>' . $createdmvrws . '</wsu:Created>';
        $authmvrws .= '			</wsse:UsernameToken>';
        $authmvrws .= '		</wsse:Security>';
        $auth_blockmvrws = new SoapVar( $authmvrws, XSD_ANYXML, NULL, NULL, NULL, NULL );

//        $authmvrcdlis = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
//        $authmvrcdlis .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
//        $authmvrcdlis .= '				<wsse:Username>wsapi_api</wsse:Username>';
//        $authmvrcdlis .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrcdlis.'</wsse:Password>';
//        $authmvrcdlis .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrcdlis)).'</wsse:Nonce>';
//        $authmvrcdlis .= '				<wsu:Created>' . $createdmvrcdlis . '</wsu:Created>';
//        $authmvrcdlis .= '			</wsse:UsernameToken>';
//        $authmvrcdlis .= '		</wsse:Security>';
//        $auth_blockmvrcdlis = new SoapVar( $authmvrcdlis, XSD_ANYXML, NULL, NULL, NULL, NULL );

//        $authmvrdac = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
//        $authmvrdac .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
//        $authmvrdac .= '				<wsse:Username>wsapi_api</wsse:Username>';
//        $authmvrdac .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrdac.'</wsse:Password>';
//        $authmvrdac .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrdac)).'</wsse:Nonce>';
//        $authmvrdac .= '				<wsu:Created>' . $createdmvrdac . '</wsu:Created>';
//        $authmvrdac .= '			</wsse:UsernameToken>';
//        $authmvrdac .= '		</wsse:Security>';
//        $auth_blockmvrdac = new SoapVar( $authmvrdac, XSD_ANYXML, NULL, NULL, NULL, NULL );

//        $authmvrehf = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
//        $authmvrehf .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
//        $authmvrehf .= '				<wsse:Username>wsapi_api</wsse:Username>';
//        $authmvrehf .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrehf.'</wsse:Password>';
//        $authmvrehf .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrehf)).'</wsse:Nonce>';
//        $authmvrehf .= '				<wsu:Created>' . $createdmvrehf . '</wsu:Created>';
//        $authmvrehf .= '			</wsse:UsernameToken>';
//        $authmvrehf .= '		</wsse:Security>';
//        $auth_blockmvrehf = new SoapVar( $authmvrehf, XSD_ANYXML, NULL, NULL, NULL, NULL );

//        $headermvrexp = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrexp );
//        $hirerightclientmvrexp->__setSoapHeaders( $headermvrexp );
//        $hirerightclientmvrexp->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


        $headermvrws = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrws );
        $hirerightclientmvrws->__setSoapHeaders( $headermvrws );
        $hirerightclientmvrws->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

//        $headermvrcdlis = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrcdlis );
//        $hirerightclientmvrcdlis->__setSoapHeaders( $headermvrcdlis );
//        $hirerightclientmvrcdlis->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed
//
//        $headermvrdac = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrdac );
//        $hirerightclientmvrdac->__setSoapHeaders( $headermvrdac );
//        $hirerightclientmvrdac->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed
//
//        $headermvrehf = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrehf );
//        $hirerightclientmvrehf->__setSoapHeaders( $headermvrehf );
//        $hirerightclientmvrehf->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed


        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $HRObject = $app->make('stdClass');

        $tmpObj = $app->make('stdClass');

        $srvcObj = $app->make('stdClass');

        $flxfieldObj = $app->make('stdClass');

        $payloadObj = $app->make('stdClass');

        $bgcheckObj = $app->make('stdClass');

        $phoneoneObj = $app->make('stdClass');

        $phonetwoObj = $app->make('stdClass');

        $addresslineObj = $app->make('stdClass');


        $servicedata = '<ns1:Service id="MVR Express"/>';


        //$servicedata = '<ns1:Service id="MVR Express"/>';

        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

        $licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

        $srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

        $flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

        $addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

        $phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('email', XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
        $personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

        $bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

        $bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

        $tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

        $mvrexpservice = new SoapVar('<ns1:Service id="MVR Express"/>', XSD_ANYXML, null, null, 'Service');



        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);
        //$CreateParms = new SoapVar(new SoapVar(new SoapVar($this->tmpObj($mvrexpservice), XSD_ANYXML, "hr_objs:BackgroundCheck", null, 'HRObject', "urn:enterprise.soap.hireright.com/objs"), SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);


        try
        {
            $retmvrws = $hirerightclientmvrws->__SoapCall('Create', array($CreateParms));

//            $retmvrstd = $hirerightclientmvrstd->__SoapCall('Create', array($CreateParms));
//
//            $retmvrws = $hirerightclientmvrws->__SoapCall('Create', array($CreateParms));
//
//            $retmvrdac = $hirerightclientmvrdac->__SoapCall('Create', array($CreateParms));
//
//            $retmvrcdlis = $hirerightclientmvrcdlis->__SoapCall('Create', array($CreateParms));
//
//            $retmvrehf = $hirerightclientmvrehf->__SoapCall('Create', array($CreateParms));



            try
            {

                $passmvrstd='wsapi_pswd';
                $createdmvrstd = gmdate('Y-m-d\TH:i:s\Z');
                $noncemvrstd = mt_rand();
                $passdigestmvrstd = base64_encode(pack('H*', sha1(pack('H*', $noncemvrstd) . pack('a*', $createdmvrstd) . pack('a*', $passmvrstd))));

                $authmvrstd = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
                $authmvrstd .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
                $authmvrstd .= '				<wsse:Username>wsapi_api</wsse:Username>';
                $authmvrstd .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigestmvrstd.'</wsse:Password>';
                $authmvrstd .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $noncemvrstd)).'</wsse:Nonce>';
                $authmvrstd .= '				<wsu:Created>' . $createdmvrstd . '</wsu:Created>';
                $authmvrstd .= '			</wsse:UsernameToken>';
                $authmvrstd .= '		</wsse:Security>';
                $auth_blockmvrstd = new SoapVar( $authmvrstd, XSD_ANYXML, NULL, NULL, NULL, NULL );

                $hirerightclientmvrstd = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));

                $headermvrstd = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_blockmvrstd );
                $hirerightclientmvrstd->__setSoapHeaders( $headermvrstd );
                $hirerightclientmvrstd->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed





                $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->RegId = new SoapVar($retmvrws->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ObjId = new SoapVar($retmvrws->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

                $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

                $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

                $url = $hirerightclientmvrstd->__SoapCall('GenerateWebLink', array($WebLinkParms));
            }
            catch (Exception $e)
            {

            }
        }
        catch (Exception $e)
        {
        }

        $resultid = $retmvrws->Result->Id;
        $regid = $retmvrws->Result->RegId;

        $runrecord = new HireRight;
        $runrecord->app_id = $_POST['app_id'];
        $runrecord->app_id_guid = $_POST['app_id_guid'];
        $runrecord->agent = $_POST['agent'];
        $runrecord->agent_guid = $_POST['agent_id'];
        $runrecord->object_id = $resultid;
        $runrecord->reg_id = $regid;
        $runrecord->save();



//        try {
//            $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
//        } catch (Exception $e) {
//
//        }

        $urlret = $url->URL;

        return $urlret;
    }


    public function dac()
    {
        $pass='wsapi_pswd';
        $created = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();
        $passdigest = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $created) . pack('a*', $pass))));

        $hirerightclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $auth = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $auth .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $auth .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $auth .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigest.'</wsse:Password>';
        $auth .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $nonce)).'</wsse:Nonce>';
        $auth .= '				<wsu:Created>' . $created . '</wsu:Created>';
        $auth .= '			</wsse:UsernameToken>';
        $auth .= '		</wsse:Security>';
        $auth_block = new SoapVar( $auth, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $header = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_block );
        $hirerightclient->__setSoapHeaders( $header );
        $hirerightclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');



        $HRObject = $app->make('stdClass');

        $tmpObj = $app->make('stdClass');

        $srvcObj = $app->make('stdClass');

        $flxfieldObj = $app->make('stdClass');

        $payloadObj = $app->make('stdClass');

        $bgcheckObj = $app->make('stdClass');

        $phoneoneObj = $app->make('stdClass');

        $phonetwoObj = $app->make('stdClass');

        $addresslineObj = $app->make('stdClass');

        $servicedata = '<ns1:Service id="Drug/Alcohol History Database"/>';

        $flexfielddata = '<ns1:FlexField name="Sub_Account">R199</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="Customer_Reference">940</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';

        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">'.$_POST['ssn'].'</GovernmentId>';

        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';

        $licensedata = '    <Screenings>																					
								<Screening type="license" qualifier="mvPersonal">
									<CountryCode>US</CountryCode>
									<Region>'.$_POST['dl_state'].'</Region>									
									<SearchLicense>
										<License>
											<LicenseNumber>'.$_POST['dl_number'].'</LicenseNumber>
											<LicensingAgency/>
										</License>
									</SearchLicense>
									<AdditionalItems qualifier="SearchDepth">
										<Text>STD</Text>
									</AdditionalItems>
								</Screening>			
							</Screenings>';

        $srvcObj->Service = new SoapVar($servicedata, XSD_ANYXML);

        $flxfieldObj->FlexField = new SoapVar($flexfielddata, XSD_ANYXML);

        $addresslineObj->AddressLine = new SoapVar($_POST['address1_line1'], XSD_STRING);

        $phoneoneObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $phonetwoObj->FormattedNumber = new SoapVar($_POST['mobilephone'], XSD_STRING);

        $nameObj = array();
        $nameObj[] = new SoapVar($_POST['firstname'], XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar($_POST['middlename'], XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar($_POST['lastname'], XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar($_POST['address1_postalcode'], XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar($_POST['address1_stateorprovince'], XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar($_POST['address1_city'], XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($_POST['mobilephone'], SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar('email', XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar($_POST['DOB'], XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);
        $personaldataObj[] = new SoapVar($licensedata, XSD_ANYXML);

        $bgcheckObj->BackgroundSearchPackage = new SoapVar($personaldataObj, SOAP_ENC_OBJECT);

        $bgcheckObj->UserArea = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', null, 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj->BackgroundCheck = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT);

        $tmpObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $tmpObj->PackageId = new SoapVar("309496", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Services = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->FlexFields = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $tmpObj->Payload = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRObject->HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, null, "urn:enterprise.soap.hireright.com/objs");

        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

//        $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->RegId = new SoapVar($ret->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->ObjId = new SoapVar($ret->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//        $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
//
//        $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");
//
//        $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

        try
        {
            $ret = $hirerightclient->__SoapCall('Create', array($CreateParms));

            try
            {

                $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->RegId = new SoapVar($ret->Result->RegId, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->ObjId = new SoapVar($ret->Result->Id, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
                $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

                $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

                $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

                $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
            }
            catch (Exception $e)
            {

            }
        }
        catch (Exception $e)
        {
        }

        $resultid = $ret->Result->Id;
        $regid = $ret->Result->RegId;

        $runrecord = new HireRight;
        $runrecord->app_id = $_POST['app_id'];
        $runrecord->app_id_guid = $_POST['app_id_guid'];
        $runrecord->agent = $_POST['agent'];
        $runrecord->agent_guid = $_POST['agent_id'];
        $runrecord->object_id = $resultid;
        $runrecord->reg_id = $regid;
        $runrecord->save();



//        try {
//            $url = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
//        } catch (Exception $e) {
//
//        }

        $urlret = $url->URL;

        return $urlret;


    }




    public function reviewreports()
    {


        $resultid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
        ])
            ->latest()
            ->select('object_id')
            ->get();

        $requestid = HireRight::where([
            ['app_id', '=', $_POST['app_id']],
            ['app_id_guid', '=', $_POST['app_id_guid']],
            ['agent', '=', $_POST['agent']],
            ['agent_guid', '=', $_POST['agent_id']],
        ])
            ->latest()
            ->select('reg_id')
            ->get();


        $pass='wsapi_pswd';
        $created = gmdate('Y-m-d\TH:i:s\Z');
        $nonce = mt_rand();
        $passdigest = base64_encode(pack('H*', sha1(pack('H*', $nonce) . pack('a*', $created) . pack('a*', $pass))));


        $hirerightclient = new SoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE));
        $auth = '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        $auth .= '			<wsse:UsernameToken wsu:Id="UsernameToken-6E86F7DED0E04869BA14853730532531">';
        $auth .= '				<wsse:Username>wsapi_api</wsse:Username>';
        $auth .= '				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">'.$passdigest.'</wsse:Password>';
        $auth .= '				<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">'.base64_encode(pack('H*', $nonce)).'</wsse:Nonce>';
        $auth .= '				<wsu:Created>' . $created . '</wsu:Created>';
        $auth .= '			</wsse:UsernameToken>';
        $auth .= '		</wsse:Security>';
        $auth_block = new SoapVar( $auth, XSD_ANYXML, NULL, NULL, NULL, NULL );

        $header = new SoapHeader('http://schemas.xmlsoap.org/soap/envelope/', 'Header', $auth_block );
        $hirerightclient->__setSoapHeaders( $header );
        $hirerightclient->__setLocation("https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2"); // IONA - Explicitly setting the location forced the POST headers to be set as needed

        $app = app();
        $HRWebLink = $app->make('stdClass');
        $urlObj = $app->make('stdClass');

        $urlObj->AccountId = new SoapVar("WS_API", XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->CompanyLogin = new SoapVar('TDAC2', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->UserRefId = new SoapVar('123', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->RegId = new SoapVar($requestid, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->ClientApplicantId = new SoapVar('AppID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->ClientRequestId = new SoapVar('ReqID_'.$_POST['app_id'].'_10', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->ObjId = new SoapVar($resultid, XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");
        $urlObj->LandingPage = new SoapVar('ENHANCED REPORT', XSD_STRING, null, null, null, "urn:enterprise.soap.hireright.com/objs");

        $HRWebLink->HRWebLink = new SoapVar($urlObj, SOAP_ENC_OBJECT, "SingleSignOnWL", null, null, "urn:enterprise.soap.hireright.com/objs");

        $WebLinkParms = new SoapVar(new SoapVar($HRWebLink, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

        try {
            $ret = $hirerightclient->__SoapCall('GenerateWebLink', array($WebLinkParms));
        } catch (Exception $e) {

        }

        $soapdatarequestheaders = htmlentities(str_ireplace('><', ">\n<",$hirerightclient->__getLastRequestHeaders()));
        $soapdatarequest = htmlentities(str_ireplace('><', ">\n<",$hirerightclient->__getLastRequest()));
        $soapdataresponse = htmlentities(str_ireplace('><', ">\n<",$hirerightclient->__getLastResponse()));

        $soapdataret = $ret;

        $urlret = $ret->URL;

        //$url = url('/auth/token', $this->token);

        //echo '<a href="' . $urlret . '">Login To NOS</a>';

        //$pagereferred = 'mvrreview';


//        return redirect()->back()
//            ->with('urlret', $urlret);

//        Request::flash();
//
//        return Redirect::to('/postform');

        return $urlret;


    }


    public function runmvrexpress()
    {
        $client = new WSSoapClient("https://ows01.hireright.com/ws_gateway_stub/wsdl/HireRightAPI.wsdl", array("trace" => TRUE, 'soap_version' => SOAP_1_1));
        $client->__setLocation('https://api.hireright.com:11443/ws_gateway/HireRightAPI/v/1/2');
        $client->__setUsernameToken("wsapi_api", "wsapi_pswd");

        $servicedata = '<ns1:Service id="MVR Standard"/>';
        $flexfielddata = '<ns1:FlexField name="ReqNumber">BikramReqNo 123</ns1:FlexField>';
        $flexfielddata .= '<ns1:FlexField name="ClientStatusReceiverURL">https://www.hireright.com</ns1:FlexField>';
        $ssndata = '<GovernmentId countryCode="US" issuingAuthority="SSN">111111111</GovernmentId>';
        $supdocdata = '<SupportingDocumentation><Documentation manifestName="Consent Form" resultType="x:signed" type="release"/><Documentation manifestName="DA Release Form" resultType="x:FaxByRequestor" type="release"/></SupportingDocumentation>';
        $srvcObj = new SoapVar($servicedata, XSD_ANYXML, null, null, 'Service');
        $flxfieldObj = new SoapVar($flexfielddata, XSD_ANYXML, null, null, 'FlexField');
        $addresslineObj = new SoapVar("5151 California Avenue", XSD_STRING, null, null, 'AddressLine');
        $phoneoneObj = new SoapVar("3721236789", XSD_STRING, null, null, 'FormattedNumber');
        $phonetwoObj = new SoapVar("123456789", XSD_STRING, null, null, 'FormattedNumber');

        $nameObj = array();
        $nameObj[] = new SoapVar("Fname", XSD_STRING, null, null, 'GivenName');
        $nameObj[] = new SoapVar("Mname", XSD_STRING, null, null, 'MiddleName');
        $nameObj[] = new SoapVar("Lname", XSD_STRING, null, null, 'FamilyName');

        $addressObj = array();
        $addressObj[] = new SoapVar("US", XSD_STRING, null, null, 'CountryCode');
        $addressObj[] = new SoapVar("90210", XSD_STRING, null, null, 'PostalCode');
        $addressObj[] = new SoapVar("OH", XSD_STRING, null, null, 'Region');
        $addressObj[] = new SoapVar("Municipality", XSD_STRING, null, null, 'Municipality');
        $addressObj[] = new SoapVar($addresslineObj, SOAP_ENC_OBJECT, null, null, 'DeliveryAddress');

        $contactoneObj = array();
        $contactoneObj[] = new SoapVar("Home", XSD_STRING, null, null, 'Location');
        $contactoneObj[] = new SoapVar($phoneoneObj, SOAP_ENC_OBJECT, null, null, 'Telephone');
        $contactoneObj[] = new SoapVar("test@hireright.com", XSD_STRING, null, null, 'InternetEmailAddress');

        $contacttwoObj = array();
        $contacttwoObj[] = new SoapVar("Work", XSD_STRING, null, null, 'Location');
        $contacttwoObj[] = new SoapVar($phonetwoObj, SOAP_ENC_OBJECT, null, null, 'Telephone');

        $demographicObj = array();
        $demographicObj[] = new SoapVar($ssndata, XSD_ANYXML);
        $demographicObj[] = new SoapVar("1938-12-31", XSD_STRING, null, null, 'DateOfBirth');
        $demographicObj[] = new SoapVar("Race", XSD_STRING, null, null, 'Race');
        $demographicObj[] = new SoapVar("GenderCode", XSD_STRING, null, null, 'GenderCode');
        $demographicObj[] = new SoapVar("Language", XSD_STRING, null, null, 'Language');
        $demographicObj[] = new SoapVar("EyeColor", XSD_STRING, null, null, 'EyeColor');
        $demographicObj[] = new SoapVar("HairColor", XSD_STRING, null, null, 'HairColor');
        $demographicObj[] = new SoapVar("Height", XSD_STRING, null, null, 'Height');
        $demographicObj[] = new SoapVar("Weight", XSD_STRING, null, null, 'Weight');
        $demographicObj[] = new SoapVar("BirthPlace", XSD_STRING, null, null, 'BirthPlace');
        $demographicObj[] = new SoapVar("Other", XSD_STRING, null, null, 'Other');

        $persondata = array();
        $persondata[] = new SoapVar($nameObj, SOAP_ENC_OBJECT, null, null, 'PersonName');
        $persondata[] = new SoapVar($addressObj, SOAP_ENC_OBJECT, null, null, 'PostalAddress');
        $persondata[] = new SoapVar($contactoneObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($contacttwoObj, SOAP_ENC_OBJECT, null, null, 'ContactMethod');
        $persondata[] = new SoapVar($demographicObj, SOAP_ENC_OBJECT, null, null, 'DemographicDetail');

        $personaldataObj = array();
        $personaldataObj[] = new SoapVar($persondata, SOAP_ENC_OBJECT, null, null, 'PersonalData');
        $personaldataObj[] = new SoapVar($supdocdata, XSD_ANYXML);

        $bgcheckObj = array();
        $bgcheckObj[] = new SoapVar($personaldataObj, SOAP_ENC_OBJECT, null, null, 'BackgroundSearchPackage');
        $bgcheckObj[] = new SoapVar('UserArea', XSD_STRING, null, 'urn:enterprise.soap.hireright.com/objs', 'UserArea', 'urn:enterprise.soap.hireright.com/objs');

        $payloadObj = new SoapVar($bgcheckObj, SOAP_ENC_OBJECT, null, null, 'BackgroundCheck');

// The final parameter in this call forces the namespace of the data element to reference hireright namespace - until I put this in, the receiving service did not recognize this element
        $tmpObj = array();
        $tmpObj[] = new SoapVar("WS_API", XSD_STRING, null, null, 'AccountId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('TDAC2', XSD_STRING, null, null, 'CompanyLogin', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('123', XSD_STRING, null, null, 'UserRefId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('AppID_170109_10', XSD_STRING, null, null, 'ClientApplicantId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar('ReqID_170109_10', XSD_STRING, null, null, 'ClientRequestId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar("309496", XSD_STRING, null, null, 'PackageId', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($srvcObj, SOAP_ENC_OBJECT, null, null, 'Services', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($flxfieldObj, SOAP_ENC_OBJECT, null, null, 'FlexFields', "urn:enterprise.soap.hireright.com/objs");
        $tmpObj[] = new SoapVar($payloadObj, SOAP_ENC_OBJECT, null, null, 'Payload', "urn:enterprise.soap.hireright.com/objs");

        $HRObject = new SoapVar($tmpObj, SOAP_ENC_OBJECT, "hr_objs:BackgroundCheck", null, 'HRObject', "urn:enterprise.soap.hireright.com/objs");

        $CreateParms = new SoapVar(new SoapVar($HRObject, SOAP_ENC_OBJECT), SOAP_ENC_OBJECT);

        try {
            $ret = $client->__SoapCall('Create', array($CreateParms));
        } catch (Exception $e) {

            echo 'Request Headers:' . $client->__getLastRequestHeaders() . "\n\n";

            echo "REQUEST:\n" . str_ireplace('><', ">\n<", $client->__getLastRequest()) . "\n";
            echo "Response:\n" . str_ireplace('><', ">\n<", $client->__getLastResponse()) . "\n";

            echo 'Exception: ' . print_r($e, TRUE);
        }

        echo 'Request Headers:' . $client->__getLastRequestHeaders() . "\n\n";

        echo "REQUEST:\n" . str_ireplace('><', ">\n<", $client->__getLastRequest()) . "\n";
        echo "Response:\n" . str_ireplace('><', ">\n<", $client->__getLastResponse()) . "\n";
        var_dump($ret);

        $soapdatarequest = str_ireplace('><', ">\n<", $client->__getLastRequest());
        $soapdataresponse = str_ireplace('><', ">\n<", $client->__getLastResponse());

        return View::make('ghtest')
            ->with('request', $soapdatarequest)
            ->with('response', $soapdataresponse);
        //return view ('ghtest', compact('soapdata'));

        echo "jsonData \n";

    }
}
