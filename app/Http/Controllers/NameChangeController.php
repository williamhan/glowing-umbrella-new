<?php

namespace App\Http\Controllers;

use App\NameChange;
use App\NameChangeApproved;
use App\NameChangeDenied;
use App\NameChangePs;
use App\NameChangePsApproved;
use App\NameChangePsDenied;
use App\Users;
use Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use ET_Client;

class NameChangeController extends Controller
{
	public function namechange()
		    {
		$namechange = new NameChange;
		
		
		$namechange->appid = $_POST['app_id'];
		$namechange->manager = $_POST['manager'];
		$namechange->status = 'Requested';
		$namechange->link = 'http://drivercrm/Driver/main.aspx?etc=10009&id=%7b' . $_POST['app_id_guid'] . '%7d&pagetype=entityrecord' ;
		$namechange->to = $_POST['agent'];
		$namechange->to_id = $_POST['agent_id'];
		$namechange->from = $_POST['recruiter'];
		$namechange->agent_email = $_POST['agent_email'];
		$namechange->is_permit_spec = 'No';
		
		
		$namechange->save();
	}
	
	public function namechangeps()
		    {
		$namechangeps = new NameChange;
		
		$namechangeps->appid = $_POST['app_id'];
		$namechangeps->manager = $_POST['manager'];
		$namechangeps->status = 'Requested';
		$namechangeps->link = 'http://drivercrm/Driver/main.aspx?etc=10009&id=%7b' . $_POST['app_id_guid'] . '%7d&pagetype=entityrecord' ;
		$namechangeps->to = $_POST['agent'];
		$namechangeps->to_id = $_POST['agent_id'];
		$namechangeps->from = $_POST['permit_spec_name'];
		$namechangeps->agent_email = $_POST['agent_email'];
		$namechangeps->is_permit_spec = 'Yes';
		
		
		$namechangeps->save();
	}
	
	public function approval($nc_appid)
		    {
		
		$approvalfind = NameChange::where('appid', '=', $nc_appid)->firstOrFail();
		
		$namechangeapproval = new NameChangeApproved;
		
		$namechangeapproval->appid = $approvalfind->appid;
		$namechangeapproval->manager = $approvalfind->manager;
		$namechangeapproval->status = 'Approved';
		$namechangeapproval->link = $approvalfind->link;
		$namechangeapproval->to = $approvalfind->to;
		$namechangeapproval->to_id = $approvalfind->to_id;
		$namechangeapproval->from = $approvalfind->from;
		$namechangeapproval->is_permit_spec = $approvalfind->is_permit_spec;
		
		$namechangeapproval->save();
		
		$approvaldelete = NameChange::where('appid', '=', $nc_appid)->firstOrFail();
		
		$approvaldelete->delete();
		
		
	}
	
	public function scribe($appdata)
		    {
		
		if ($appdata['is_permit_spec'] == 'No')
				        {
			//$			url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/2306?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
			$url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/4187?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
		}
		elseif ($appdata['is_permit_spec'] == 'Yes')
				        {
			//$			url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/2325?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
			$url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/4188?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";
		}
		
		$jsonDataEncoded = json_encode($appdata);
		
		$curl = curl_init();
		
		$opts = array(
				            CURLOPT_URL             => $url,
				            CURLOPT_RETURNTRANSFER  => true,
				            CURLOPT_CUSTOMREQUEST   => 'POST',
				            CURLOPT_POST            => 1,
				            CURLOPT_POSTFIELDS      => $jsonDataEncoded
				        );
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
		curl_setopt_array($curl, $opts);
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		$response = json_decode($result, true);
		
		return $response;
	}
	
	
	
	public function buildagain($guid)
		    {
		$manager = $guid;
		
		if ($manager == "All") {
			$names[$manager] = NameChange::where('status', '=', 'Requested')
						            ->get();
			
			return View::make('managers')
						            ->with('manager', $manager)
						            ->with('names', $names);
		}
		
		else {
			$names[$manager] = NameChange::where([
						            ['status', '=', 'Requested'],
						            ['manager', 'LIKE', $manager.'%'],
						        ])
						            ->get();
			
			return View::make('managers')
						            ->with('manager', $manager)
						            ->with('names', $names);
		}
		
		
	}
	
	public function build($guid)
		    {
		$manager = $guid;
		
		$names['all'] = NameChange::where('status', '=', 'Requested')
				            ->get();
		$names['jacob'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Jacob%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['jessica'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Jessica%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['debbie'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Debbie%'],
				            ['is_permit_spec', '=', 'Yes'],
				        ])
				            ->get();
		$names['lisa'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Lisa%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['nick'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Nick%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['sepeti'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Sepeti%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		
		return View::make('managers')
				            ->with('manager', $manager)
				            ->with('names', $names);
		// 		return view ('managers', compact('names', 'manager'));
	}
	
	public function deny()
		    {
		$data = Request::only(['app_id']);
		
		
		
		$approvalfind = NameChange::where('appid', '=', $_POST['app_id'])->first();
		$approvalfind->status = 'Denied';
		$approvalfind->save();
		
		$DataExtensionName = "NameChange_Email_List";
		
		date_default_timezone_set('America/Denver');
		
		$myclient = new \ET_Client();
		$dataextensionrow = new \ET_DataExtension_Row();
		$dataextensionrow->authStub = $myclient;
		$dataextensionrow->Name = $DataExtensionName;
		$dataextensionrow->props = array("Email_Address" => $approvalfind->agent_email, "App_ID" => $approvalfind->appid, "Recruiter" => $approvalfind->to, "Sent_Date" => date('Y-m-d H:i:s'));
		$results = $dataextensionrow->post();
		print_r($results);
		
		
		
		$sendSendDefinition = new \ET_Email_SendDefinition();
		$sendSendDefinition->authStub = $myclient;
		$sendSendDefinition->props = array("CustomerKey"=>"NC_Deny");
		$sendResponse = $sendSendDefinition->send();
		print_r($sendResponse);
		
		$namechangedenial = new NameChangeDenied;
		
		$namechangedenial->appid = $approvalfind->appid;
		$namechangedenial->manager = $approvalfind->manager;
		$namechangedenial->status = 'Denied';
		$namechangedenial->link = $approvalfind->link;
		$namechangedenial->to = $approvalfind->to;
		$namechangedenial->to_id = $approvalfind->to_id;
		$namechangedenial->from = $approvalfind->from;
		$namechangedenial->is_permit_spec = $approvalfind->is_permit_spec;
		
		
		$namechangedenial->save();
		
		$denialdelete = NameChange::where('appid', '=', $data['app_id'])->first();
		$denialdelete->delete();
		//p		rint_r($response["appInfo"][0]);
		$names['all'] = NameChange::where('status', '=', 'Requested')
				            ->get();
		$names['jacob'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Jacob%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['jessica'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Jessica%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['jessicaps'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Jessica%'],
				            ['is_permit_spec', '=', 'Yes'],
				        ])
				            ->get();
		$names['lisa'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Lisa%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['nick'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Nick%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		$names['sepeti'] = NameChange::where([
				            ['status', '=', 'Requested'],
				            ['manager', 'LIKE', 'Sepeti%'],
				            ['is_permit_spec', '=', 'No'],
				        ])
				            ->get();
		
		
		return view ('managers', compact('names'));
	}
	
	public function scribenamechange($guid)
		    {
		$manager = $guid;
		$data = Request::all();
		
		$scribedata = $this->scribe($data);
		
		$appid = $scribedata["ncinfo"][0]['app_id'];
		
		$this->approval($appid);
		
		//$		this->buildagain($guid);
		
		if ($manager == "All") {
			$names[$manager] = NameChange::where('status', '=', 'Requested')
						            ->get();
			
			return View::make('managers')
						            ->with('manager', $manager)
						            ->with('names', $names);
		}
		
		else {
			$names[$manager] = NameChange::where([
						            ['status', '=', 'Requested'],
						            ['manager', 'LIKE', $manager.'%'],
						        ])
						            ->get();
			
			return View::make('managers')
						            ->with('manager', $manager)
						            ->with('names', $names);
		}
		
		
	}
	
	
}
