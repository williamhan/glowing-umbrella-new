<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schools extends Model
{
    protected $table = 'schools';
    public $incrementing = false;
    public $timestamps = false;

    public function schoolinfo()
    {
        return $this->hasOne('App\SchoolInfo', 'sch_id', 'id');
    }
}
