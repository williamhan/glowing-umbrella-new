<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrapSchools extends Model
{
    // frap_schools
    protected $table = 'frap_schools';
    public $incrementing = false;
    public $timestamps = false;

    public function states()
    {
        return $this->belongsTo('App\States', 'schoolstate', 'state_abbr');
    }
}
