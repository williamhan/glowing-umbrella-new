<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Request;
use Session;

use Carbon\Carbon;
use App\Greyhound;
use App\Users;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\View;

class BillTestModel extends Model
{
    public static function scribeapplist()
    {
		
        //$agentname = Request::only(['agent']);
        
        //$agentname = Users::where('name', '=', 'Bam Anderson')->get(['name']);

		//$agentname = Session::get('agent');
		
		$agentname = Session::get('sysuserid');

		
		//----- URL For Direct Write to NOS -----//
		
		$url = "https://endpoint.scribesoft.com/v1/orgs/6701/requests/5037?accesstoken=1b110848-40b6-43bb-9c45-568bdcf001e1";

		//$url = "https://699d1fphyyjd.runscope.net/";

		$jsonData = array(
			'agent' => $agentname
			);
		
		$jsonDataEncoded = json_encode($jsonData);
		
		$curl = curl_init();
		
		$opts = array(
				            CURLOPT_URL             => $url,
				            CURLOPT_RETURNTRANSFER  => true,
				            CURLOPT_CUSTOMREQUEST   => 'POST',
				            CURLOPT_POST            => 1,
				            CURLOPT_POSTFIELDS      => $jsonDataEncoded
				        );
		
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=utf-8"));
		curl_setopt_array($curl, $opts);
		$result = curl_exec($curl);
		curl_close($curl);
		
		$response = json_decode($result, true);
		$appdata = $response;
		return $appdata;
		//return view('example',compact('maindata', 'allsmsdata', 'appdata'));
	}
}
