<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Mail;

class LoginToken extends Model
{
	
	
	/**
	* Fillable fields for the model.
						     *
						     * @var array
						     */
						    protected $fillable = ['user_id', 'token'];
	
	
	public static function generateFor(User $user)
						    {
		return static::create([
												            'user_id' => $user->id,
												            'token' => str_random(50)
												        ]);
	}
	
	public function getRouteKeyName()
						    {
		return 'token';
	}
	
	public function send()
						    {
		$url = url('/auth/token', $this->token);
		
		echo '<a href="' . $url . '">Login To NOS</a>';
		
		
		
	}
	
	public function user()
						    {
		return $this->belongsTo(User::class);
	}
	
}
