<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleInfo extends Model
{
    //
    protected $table = 'scheduleinfo';

    public function schoolinfo()
    {
        return $this->belongsTo('App\SchoolInfo', 'sch_id', 'sch_id');
    }
}
