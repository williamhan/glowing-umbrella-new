<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NameChangePending extends Model
{
    //
    protected $table = 'namechange_pending';
}
