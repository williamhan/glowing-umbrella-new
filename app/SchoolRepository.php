<?php
use Illuminate\Database\Eloquent\Model;

use Mgallegos\LaravelJqgrid\Repositories\EloquentRepositoryAbstract;

class SchoolRepository extends EloquentRepositoryAbstract {

    public function __construct(Model $Model)
    {
        $this->Database = $Model;

        $this->visibleColumns = array('id', 'sch_id', 'sch_name', 'sch_type', 'sch_address', 'sch_city', 'sch_state', 'sch_zip', 'sch_phone', 'sch_fax', 'sch_duration_min', 'sch_duration_max', 'sch_transportation_id', 'sch_schedule_id', 'sch_docs_id', 'sch_fees_id', 'sch_housing_id');

        $this->orderBy = array(array('sch_id', 'asc'));
    }
}