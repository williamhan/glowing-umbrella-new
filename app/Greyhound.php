<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Greyhound extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'greyhound';
    public $incrementing = false;
    public $timestamps = false;
}
