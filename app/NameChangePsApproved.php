<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NameChangePsApproved extends Model
{
    //
    protected $table = 'namechange_ps_approved';
}
