<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocsInfo extends Model
{
    //
    protected $table = 'docsinfo';

    public function schoolinfo()
    {
        return $this->belongsTo('App\SchoolInfo', 'sch_id', 'sch_id');
    }
}