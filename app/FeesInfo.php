<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeesInfo extends Model
{
    //
    protected $table = 'feesinfo';

    public function schoolinfo()
    {
        return $this->belongsTo('App\SchoolInfo', 'sch_id', 'sch_id');
    }
}