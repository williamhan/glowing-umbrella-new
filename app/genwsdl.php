<?php

use \Wsdl2PhpGenerator;


$generator = new Wsdl2PhpGenerator\Generator();
$generator->generate(
    new Wsdl2PhpGenerator\Config(array(
        'inputFile' => 'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL',
        'outputDir' => 'app/SoapCC'
    ))
);