<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportationInfo extends Model
{
    //
    protected $table = 'transportationinfo';
    public $incrementing = false;
    public $timestamps = false;

    public function schoolinfo()
    {
        return $this->belongsTo('App\SchoolInfo', 'sch_id', 'sch_id');
    }
}