<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentApps extends Model
{
    protected $table = 'agent_apps';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'agent_fullname', 'cre_appid', 'cre_applicantidname', 'createdon', 'modifiedon', 'statecode', 'systemuserid'
    ];
}
