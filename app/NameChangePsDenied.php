<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NameChangePsDenied extends Model
{
    //
    protected $table = 'namechange_ps_denied';
}
