<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driverapp extends Model
{
	//
	protected $table = 'driverapp';
	
	public static function getGUID()
	{
		if (function_exists('com_create_guid')) {
			return com_create_guid();
		}
		else {
			mt_srand((double)microtime() * 10000);
			//o			ptional for php 4.2.0 and up.
			        $charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);
			// 			"-"
			        $uuid =             //c			hr(123) . // 			"{"
			            substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
			//.			chr(125);
			// 			"}"
			        return $uuid;
			
		}
	}
}
