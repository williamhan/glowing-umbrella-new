<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NameChangeApproved extends Model
{
    //
    protected $table = 'namechange_approved';
}
