<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolInfo extends Model
{
    //
    protected $table = 'schoolinfo';
    public $incrementing = false;
    public $timestamps = false;

    public function feesinfo()
    {
        return $this->hasMany('App\FeesInfo', 'sch_id', 'sch_id');
    }

    public function docsinfo()
    {
        return $this->hasMany('App\DocsInfo', 'sch_id', 'sch_id');
    }

    public function housinginfo()
    {
        return $this->hasOne('App\HousingInfo', 'sch_id', 'sch_id');
    }

    public function scheduleinfo()
    {
        return $this->hasMany('App\ScheduleInfo', 'sch_id', 'sch_id');
    }

    public function transportationinfo()
    {
        return $this->hasOne('App\TransportationInfo', 'id', 'sch_transportation_id');
    }

    public function school()
    {
        return $this->belongsTo('App\Schools', 'id', 'sch_id');
    }
}