<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    protected $table = 'tbl_state';
    public $incrementing = false;
    public $timestamps = false;

    public function frapschools()
    {
        return $this->hasMany('App\FrapSchools', 'state_abbr', 'schoolstate');
    }
}
